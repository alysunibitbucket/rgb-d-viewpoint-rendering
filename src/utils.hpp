/*
 * utils.hpp
 *
 *  Created on: 7 Aug 2013
 *      Author: aly
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <opencv2/opencv.hpp>
#include <Eigen/Dense>
#include "vtkSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkTransformPolyDataFilter.h"
#include <vtkTransform.h>
#include <vtkMatrix4x4.h>
#include "render_views.hpp"

#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkWorldPointPicker.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>

#include <vtk/vtkPolyDataMapper.h>
#include <vtkPLYReader.h>
#include <vtkPLYWriter.h>
#include <vtk/vtkSmartPointer.h>
#include <vtkSelection.h>
#include <vtkCellArray.h>
#include <vtkTransformFilter.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointPicker.h>
#include <vtkCleanPolyData.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkCubeSource.h>
#include <opencv2/opencv.hpp>
#include <vtkExtractEdges.h>
#include <vtkArrowSource.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkPlaneSource.h>
#include <vtkPlane.h>


struct point3d{
	float x;
	float y;
	float z;
	point3d(float x = 0, float y = 0, float z = 0):x(x),y(y),z(z){};
};

void calculate_center_of_mass(vtkSmartPointer<vtkPolyData>& poly, double* com);
cv::Mat get_visualizable_depth_img(cv::Mat& depth_img);
int round_to_nearest_int(double d);
cv::Mat load_accv12_depth(std::string filename);
Eigen::Matrix3f get_rotation_matrix(Eigen::Vector3f point_on_sphere);

double get_yaw(vtkMatrix4x4& R);
double get_pitch(vtkMatrix4x4& R);
double get_roll(vtkMatrix4x4& R);

double get_yaw(Eigen::Matrix3f& R);
double get_pitch(Eigen::Matrix3f& rotation_mat);
double get_roll(Eigen::Matrix3f& rotation_mat);

double get_yaw(Eigen::Matrix3Xf& R);
double get_pitch(Eigen::Matrix3Xf& rotation_mat);
double get_roll(Eigen::Matrix3Xf& rotation_mat);

vtkSmartPointer<vtkTransformPolyDataFilter> rotate_mesh(vtkSmartPointer<vtkPolyData> poly_data, double rotation_angle, double x, double y, double z);
void generate_image_at_specific_position(std::string& mesh_path, std::string& rgb_out, std::string& depth_out, std::string& pose_out,
		double x, double y, double z, double yaw, double pitch, double roll,
		bool center_object_in_output = false);



#endif /* UTILS_HPP_ */
