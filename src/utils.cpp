/*
 * utils.cpp
 *
 *  Created on: 7 Aug 2013
 *      Author: aly
 */

#include "utils.hpp"
#include <vtkCellArray.h>
#include <vtkTriangle.h>
#include <opencv2/opencv.hpp>
#include <math.h>

void calculate_center_of_mass(vtkSmartPointer<vtkPolyData>& polydata, double* CoM){

	vtkIdType npts_com = 0, *ptIds_com = NULL;
	vtkSmartPointer<vtkCellArray> cells_com = polydata->GetPolys();
//	cout << " number of cells is " << cells_com->GetNumberOfCells() << endl;

	double center[3], p1_com[3], p2_com[3], p3_com[3], area_com, totalArea_com = 0;
	double comx = 0, comy = 0, comz = 0;
	for (cells_com->InitTraversal(); cells_com->GetNextCell(npts_com, ptIds_com);) {
		polydata->GetPoint(ptIds_com[0], p1_com);
		polydata->GetPoint(ptIds_com[1], p2_com);
		polydata->GetPoint(ptIds_com[2], p3_com);
		vtkTriangle::TriangleCenter(p1_com, p2_com, p3_com, center);
		area_com = vtkTriangle::TriangleArea(p1_com, p2_com, p3_com);
		comx += center[0] * area_com;
		comy += center[1] * area_com;
		comz += center[2] * area_com;

		totalArea_com += area_com;
	}

//	std::cout << " total area si " << totalArea_com << std::endl;
	CoM[0] = comx / totalArea_com;
	CoM[1] = comy / totalArea_com;
	CoM[2] = comz / totalArea_com;
//	cout << " com is " << CoM[0] << ", " << CoM[1] << ", " << CoM[2] << endl;
}


cv::Mat load_accv12_depth(std::string filename) {

//	pcl::visualization::PCLVisualizer vis("Visualizer");

	std::ifstream l_file(filename.c_str(), std::ofstream::in | std::ofstream::binary);

	if (l_file.fail() == true) {
		printf("cv_load_depth: could not open file for writing!\n");
		std::cerr << "failed to load file " << filename << endl;
		exit(-1);
	}
	int l_row;
	int l_col;

	l_file.read((char*) &l_row, sizeof(l_row));
	l_file.read((char*) &l_col, sizeof(l_col));

	IplImage * lp_image = cvCreateImage(cvSize(l_col, l_row), IPL_DEPTH_16U, 1);

	for (int l_r = 0; l_r < l_row; ++l_r) {
		for (int l_c = 0; l_c < l_col; ++l_c) {
			l_file.read((char*) &CV_IMAGE_ELEM(lp_image,unsigned short,l_r,l_c) ,sizeof(unsigned short));
		}
	}
	l_file.close();

	cv::Mat m = cv::Mat(lp_image);
	return m;
}


int round_to_nearest_int(double d)
{
  return floor(d + 0.5);
}
cv::Mat get_visualizable_depth_img(cv::Mat& depth_img) {
	using namespace cv;
	double min;
	double max;

	minMaxIdx(depth_img, &min, &max);
	min = DBL_MAX;
	for (int x = 0; x < depth_img.cols; x++) {
		for (int y = 0; y < depth_img.rows; y++) {
			if (depth_img.at<int16_t>(y, x) != 0 && depth_img.at<int16_t>(y, x) < min) {
				min = depth_img.at<int16_t>(y, x);
			}
		}
	}

	cv::Mat m = depth_img.clone();
	m -= min;

	cv::Mat adjMap;
	cv::convertScaleAbs(m, adjMap, 255.0 / (max - min));
	cout << " min is " << min << " max is " << max << endl;
	return adjMap;
}

/*
 * input takes point on tesselated sphere - must be normalized
 */
Eigen::Matrix3f get_rotation_matrix(Eigen::Vector3f end_point) {

	Eigen::Vector3f start_point(0.f, 0.f, 1.f);

	end_point.normalize();
	//opposite direction
	end_point.operator *=(-1);
	 
	Eigen::Vector3f x = start_point.cross(end_point);
	
	if(x.norm() != 0){
		//otherwise the cross product is (0,0,0) and normalization gives NAN
		x.normalize();	
	}
	else{
		x[0] = 0;
		x[1] = 1;
		x[2] = 0;
	}
	

	double theta = acos(start_point.dot(end_point));

	Eigen::MatrixXf A(3, 3);
	A(0, 0) = 0.f;
	A(0, 1) = -x(2);
	A(0, 2) = x(1);

	A(1, 0) = x(2);
	A(1, 1) = 0.f;
	A(1, 2) = -x(0);

	A(2, 0) = -x(1);
	A(2, 1) = x(0);
	A(2, 2) = 0.f;

	Eigen::MatrixXf R(3, 3);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			R(i, j) = 0;
		}
	}
	R = Eigen::Matrix3f::Identity() + A * sin(theta) + (1 - cos(theta)) * A * A;
	
	return R;
}
double get_yaw(Eigen::Matrix3f& R){
	using namespace std;
	double yaw = atan2(R(1,0), R(0,0));
	return yaw;
}
double get_pitch(Eigen::Matrix3f& rotation_mat){
	using namespace std;
	return atan2(-rotation_mat(2,0), sqrt(pow(rotation_mat(2,1),2) + pow(rotation_mat(2,2),2)));
}
double get_roll(Eigen::Matrix3f& rotation_mat){
	using namespace std;
	return atan2(rotation_mat(2,1),rotation_mat(2,2));
}


double get_yaw(Eigen::Matrix3Xf& R){
	using namespace std;
	double yaw = atan2(R(1,0), R(0,0));
	return yaw;
}
double get_pitch(Eigen::Matrix3Xf& rotation_mat){
	using namespace std;
	return atan2(-rotation_mat(2,0), sqrt(pow(rotation_mat(2,1),2) + pow(rotation_mat(2,2),2)));
}
double get_roll(Eigen::Matrix3Xf& rotation_mat){
	using namespace std;
	return atan2(rotation_mat(2,1),rotation_mat(2,2));
}


double get_yaw(vtkMatrix4x4& R){
	using namespace std;
	
	double yaw = atan2(R.GetElement(1,0), R.GetElement(0,0));
	return yaw;
}
double get_pitch(vtkMatrix4x4& R){
	using namespace std;
	return atan2(-R.GetElement(2,0), sqrt(pow(R.GetElement(2,1),2) + pow(R.GetElement(2,2),2)));
}
double get_roll(vtkMatrix4x4& R){
	using namespace std;
	return atan2(R.GetElement(2,1),R.GetElement(2,2));
}





vtkSmartPointer<vtkTransformPolyDataFilter> rotate_mesh(vtkSmartPointer<vtkPolyData> poly_data, double rotation_angle, double x, double y, double z) {
	vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
	transform->RotateWXYZ(rotation_angle, x, y, z);
	
	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();

	transformFilter->SetTransform(transform);
	transformFilter->SetInput(poly_data);
	transformFilter->Update();

	return transformFilter;
}


void generate_image_at_specific_position(std::string& mesh_path, std::string& rgb_out, std::string& depth_out, std::string& pose_out,
		double x, double y, double z, double yaw, double pitch, double roll,
		bool center_object_in_output) {
	RenderViews rv_demo;
	//kinect values for near and far?
	rv_demo.set_z_near(200);
	rv_demo.set_z_far(10000000);

	rv_demo.setTesselationLevel(2);
	rv_demo.setUseUpperHemisphereOnly(true);
	rv_demo.setUseVertices(true);
	rv_demo.set_save_images_and_poses(true);

	vtkSmartPointer<vtkPLYReader> fileReader_demo = vtkSmartPointer<vtkPLYReader>::New();
	fileReader_demo->SetFileName(mesh_path.c_str());
	fileReader_demo->Update();
	//No transformation needed
	vtkSmartPointer<vtkPolyData> poly;
	poly = fileReader_demo->GetOutput();

	rv_demo.setModelFromPolyData(poly);

	//larger scale ape 60
	rv_demo.generate_specific_view(x, y, z, yaw, pitch, roll, rgb_out, depth_out, pose_out, center_object_in_output);
}





















