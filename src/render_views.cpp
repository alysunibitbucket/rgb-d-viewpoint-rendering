/*
 * render_views_tesselated_sphere.cpp
 *
 *  Created on: Dec 23, 2011
 *      Author: aitor
 */
#include "render_views.hpp"


void RenderViews::get_centre_of_mass(double* CoM) {
	calculate_center_of_mass(polydata_, CoM);
}
vtkSmartPointer<vtkPolyData> RenderViews::get_tesselated_sphere() {
	/***
	 * create subdivided icosahedron 
	 ***/
	vtkSmartPointer<vtkPlatonicSolidSource> ico = vtkSmartPointer<vtkPlatonicSolidSource>::New();
	ico->SetSolidTypeToIcosahedron();	
	ico->Update();

	//tesselate cells from icosahedron
	vtkSmartPointer<vtkLoopSubdivisionFilter> subdivide = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
	subdivide->SetNumberOfSubdivisions(tesselation_level_);
	subdivide->SetInputConnection(ico->GetOutputPort());

	// Get camera positions
	vtkSmartPointer<vtkPolyData> sphere = subdivide->GetOutput();
	sphere->Update();
	

//	cout << " created tesselated sphere with " << sphere->GetNumberOfCells() << " cells " << endl;

	return sphere;
}

/*
 * returns camera positions around the input sphere which is probably a unit sphere - must be multiplied by desiered radius for scaling
 */
std::vector<Eigen::Vector3f> RenderViews::get_cam_positions(vtkSmartPointer<vtkPolyData> tesselated_sphere) {
//	cout << "Getting camera positions use_upper_hemisphere = " << use_upper_hemisphere_only_ << " and use_vertices = " << use_vertices_ << endl;
	std::vector<Eigen::Vector3f> cam_positions;
	vtkIdType npts_com = 0, *ptIds_com = NULL;
	double center[3], p1_com[3], p2_com[3], p3_com[3];

	std::cout << " use vertices is " << use_vertices_ << std::endl;

	if (!use_vertices_) {
		// not using vertices but the centre of the faces instead
		vtkSmartPointer<vtkCellArray> cells_sphere = tesselated_sphere->GetPolys();
		

		for (cells_sphere->InitTraversal(); cells_sphere->GetNextCell(npts_com, ptIds_com);) {
			tesselated_sphere->GetPoint(ptIds_com[0], p1_com);
			tesselated_sphere->GetPoint(ptIds_com[1], p2_com);
			tesselated_sphere->GetPoint(ptIds_com[2], p3_com);
			vtkTriangle::TriangleCenter(p1_com, p2_com, p3_com, center);

			if (use_upper_hemisphere_only_) {
				//Upper hemispher is in the negative z region only as the object starts in a funny location
//				if (center[1] <= 0) {
				if (center[1] >= 0) {
//				if(center[2] <= 0){
					Eigen::Vector3f position = Eigen::Vector3f(float(center[0]), float(center[1]), float(center[2]));
					cam_positions.push_back(position);
				}
			} else {
//				if (center[1] >= 0) {
				if (center[1] <= 0) {
//				if(center[2] >= 0){
					Eigen::Vector3f position = Eigen::Vector3f(float(center[0]), float(center[1]), float(center[2]));
					cam_positions.push_back(position);
				}
			}
		}

	} else {
		std::cout << "****************** using the vertices *************" << std::endl;
		for (int i = 0; i < tesselated_sphere->GetNumberOfPoints(); i++) {
			double cam_pos[3];
			tesselated_sphere->GetPoint(i, cam_pos);

			if (use_upper_hemisphere_only_) {
//				if (cam_pos[1] <= 0) {
				if (cam_pos[1] >= 0) {
//				if(cam_pos[2] <= 0){
					Eigen::Vector3f position = Eigen::Vector3f(float(cam_pos[0]), float(cam_pos[1]), float(cam_pos[2]));
					cam_positions.push_back(position);
				}
			} else {
//				if (cam_pos[1] >= 0) {
				if (cam_pos[1] <= 0) {
//				if(cam_pos[2] >= 0){
					Eigen::Vector3f position = Eigen::Vector3f(float(cam_pos[0]), float(cam_pos[1]), float(cam_pos[2]));
					cam_positions.push_back(position);
				}
			}
		}
	}

	cout << " numbr of points are " << tesselated_sphere->GetNumberOfPoints() << endl;

	cout << " generated " << cam_positions.size() << " camera positions " << endl;
	return cam_positions;
}

#include <opencv2/opencv_modules.hpp>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

/*
 * yaw = z
 * pitch =y 
 * roll = x
 */
struct world_coord {
	double x;
	double y;
	double z;
	world_coord(double x = 0, double y = 0, double z = 0)
			: x(x), y(y), z(z) {
	}
	;
};

bool operator<(const world_coord& a, const world_coord& b) {
	return a.z < b.z;
}

int _main(int argc, char **argv) {
	vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();

	fileReader->SetFileName("benchvise_mesh.ply");
	fileReader->Update();
	vtkSmartPointer<vtkPolyData> polydata_ = fileReader->GetOutput();

	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();

	actor_view->SetMapper(mapper);
	actor_view->Modified();

	renderer->SetActiveCamera(cam);
	renderer->AddActor(actor_view);
	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();

	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	//white
	render_win->Modified();

	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();

	for (int i = 0; i < 100; i++) {

		mapper->SetInput(polydata_);
		mapper->Update();

		cam->SetViewAngle(58);
		cam->SetClippingRange(0.00001, 10000);
		//Invert view Up
		cam->SetViewUp(0, -1, 0);
		cam->SetPosition(0, 0, -500);
		cam->SetFocalPoint(0, 0, 1);
		cam->Modified();

//		render_win->Start();
		render_win->Render();
//		render_win->Finalize();

		/*
		 * Save Images
		 */

		windowToImageFilter->SetInput(render_win);
		windowToImageFilter->SetMagnification(1);
		windowToImageFilter->SetInputBufferTypeToRGB();

		vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
		std::stringstream ss;
		ss << "tmp/test" << i << ".png";
		writer->SetFileName(ss.str().c_str());
		writer->SetInputConnection(windowToImageFilter->GetOutputPort());
		writer->Write();

		std::cout << i << std::endl;
	}
}

vtkSmartPointer<vtkMatrix4x4> RenderViews::generate_specific_view(double x, double y, double z, double yaw, double pitch, double roll, vtkSmartPointer<vtkRenderWindow>& render_win,
		vtkSmartPointer<vtkCamera>& cam, vtkSmartPointer<vtkPolyDataMapper>& mapper, vtkSmartPointer<vtkWindowToImageFilter>& windowToImageFilter, std::string& rgb_out, std::string& depth_out,
		bool center_object_in_output_image) {

	/*
	 * Rotate
	 */

	vtkSmartPointer<vtkTransform> rotation = vtkSmartPointer<vtkTransform>::New();
	rotation->RotateZ(yaw);
	rotation->RotateY(pitch);
	rotation->RotateX(roll);

	vtkSmartPointer<vtkMatrix4x4> rotation_matrix = rotation->GetMatrix();
	//		cout << "rotation mat " << endl;
	//		for(int i = 0; i < 4; i++){
	//			for(int j =0 ; j < 4; j++){
	//				cout << rotation_matrix->Element[i][j] << ",";
	//			}
	//			cout << endl;
	//		}

	vtkSmartPointer<vtkTransformFilter> rotation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	rotation_filter->SetTransform(rotation);
	rotation_filter->SetInput(polydata_);
	rotation_filter->Update();

	/*
	 * Translate
	 */
	double CoM[3];
	vtkSmartPointer<vtkPolyData> data = rotation_filter->GetPolyDataOutput();
	calculate_center_of_mass(data, CoM);

	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	//First to CoM
	translation->Translate(-CoM[0], -CoM[1], -CoM[2]);
	translation->Translate(x, y, z);

	vtkSmartPointer<vtkTransformFilter> translation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_filter->SetTransform(translation);
	translation_filter->SetInput(rotation_filter->GetPolyDataOutput());
	translation_filter->Update();

	mapper->SetInputConnection(translation_filter->GetOutputPort());
	mapper->Update();

	Eigen::Vector3f cam_pos_3f(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z));
	cam_pos_3f = cam_pos_3f.normalized();
	Eigen::Vector3f view_up = Eigen::Vector3f::UnitY();

	//If the view up is parallel to ray cam_pos - focalPoint then the transformation
	//is singular and no points are rendered...
	//make sure it is perpendicular
	if (fabs(cam_pos_3f.dot(view_up)) == 1) {
		//parallel, create
		view_up = cam_pos_3f.cross(Eigen::Vector3f::UnitX());
	}

	cam->SetViewAngle(view_angle_);
	cam->SetClippingRange(z_near, z_far);
	//Invert view Up

	cam->SetViewUp(0, -1, 0);
	cam->SetPosition(0, 0, 0);
	if (center_object_in_output_image) {
		cam->SetFocalPoint(x, y, z);
	} else {
		cam->SetFocalPoint(0, 0, 1);
	}
	//use the one below to generate a training image as it focuses on the model, so it is centered in the image -- todo add a flag for this
	cam->Modified();

	render_win->Render();

	/*
	 * Save Images
	 */

	windowToImageFilter->SetInput(render_win);
	windowToImageFilter->SetMagnification(1);
	windowToImageFilter->SetInputBufferTypeToRGB();

	vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
	writer->SetFileName(rgb_out.c_str());
	writer->SetInputConnection(windowToImageFilter->GetOutputPort());
	writer->Write();

	cv::Mat depth_img = cv::Mat(rows_, cols_, CV_16UC1, cv::Scalar(0));

	float * depth = new float[cols_ * rows_];
	render_win->GetZbufferData(0, 0, cols_ - 1, rows_ - 1, &(depth[0]));

	for (int y = 0; y < depth_img.rows; y++) {
		for (int x = 0; x < depth_img.cols; x++) {
			float z_buff = depth[y * cols_ + x];
			//because we flip the image around x

			if (z_buff != 1.0) {
				//formula from here: https://en.wikipedia.org/wiki/Z-buffering
				double depth = -1 * ((z_far * z_near) / (z_buff * (z_far - z_near) - z_far));
				depth_img.at<int16_t>(y, x) = (int16_t) depth;

			} else {
				depth_img.at<int16_t>(y, x) = 0;
			}
		}
	}
	//must flip image as it's producing one from camera perspective i.e. upside down
	cv::flip(depth_img, depth_img, 0);
	cv::imwrite(depth_out, depth_img);

	return rotation_matrix;

}

vtkSmartPointer<vtkMatrix4x4> RenderViews::generate_specific_view(double x, double y, double z, double yaw, double pitch, double roll, 
		std::string& rgb_out, std::string& depth_out, std::string& pose_file,
		bool center_object_in_output_image) {
	/*
	 * Initialization
	 */
	double CoM[3];
	double bounds[6];

	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

	render_win->AddRenderer(renderer);
	render_win->SetSize(cols_, rows_);
	//white
	renderer->SetBackground(1.0, 1.0, 1.0);

	vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	rwi->SetRenderWindow(render_win);

	/*
	 * Rotate
	 */
	vtkSmartPointer<vtkTransform> rotation = vtkSmartPointer<vtkTransform>::New();
	rotation->RotateZ(yaw);
	rotation->RotateY(pitch);
	rotation->RotateX(roll);

	vtkSmartPointer<vtkMatrix4x4> rotation_matrix = rotation->GetMatrix();

	vtkSmartPointer<vtkTransformFilter> rotation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	rotation_filter->SetTransform(rotation);
	rotation_filter->SetInput(polydata_);
	rotation_filter->Update();

	/*
	 * Translate
	 */
	vtkSmartPointer<vtkPolyData> data = rotation_filter->GetPolyDataOutput();
	calculate_center_of_mass(data, CoM);

	vtkSmartPointer<vtkTransform> translate_to_center = vtkSmartPointer<vtkTransform>::New();
	translate_to_center->Translate(-CoM[0], -CoM[1], -CoM[2]);
	
	vtkSmartPointer<vtkTransformFilter> translation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_filter->SetTransform(translate_to_center);
	//	translation_filter->SetInput(rotation_filter->GetPolyDataOutput());
	translation_filter->SetInput(polydata_);
	translation_filter->Update();
	
	vtkSmartPointer<vtkTransform> transformation = vtkSmartPointer<vtkTransform>::New();
	//First to CoM
	transformation->Translate(x, y, z);
	transformation->Concatenate(rotation);

	std::cout <<" now matrix is "<< std::endl;
	rotation_matrix = transformation->GetMatrix();
	cout << "rotation mat " << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << rotation_matrix->Element[i][j] << ",";
		}
		cout << endl;
	}
	
	
	std::cout << "com is (" << -CoM[0] << ", " <<  -CoM[1] << ", " <<  -CoM[2] << ") and pos is (" << x << ", " << y << ", " << z << ") " << std::endl;  
	
	vtkSmartPointer<vtkTransformFilter> transformation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	transformation_filter->SetTransform(transformation);
//	translation_filter->SetInput(rotation_filter->GetPolyDataOutput());
//	transformation_filter->SetInput(polydata_);
	transformation_filter->SetInputConnection(translation_filter->GetOutputPort());
	transformation_filter->Update();

	/*
	 * Display
	 */

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(transformation_filter->GetOutputPort());
	mapper->Update();

	Eigen::Vector3f cam_pos_3f(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z));
	cam_pos_3f = cam_pos_3f.normalized();
	Eigen::Vector3f view_up = Eigen::Vector3f::UnitY();
	

	//If the view up is parallel to ray cam_pos - focalPoint then the transformation
	//is singular and no points are rendered...
	//make sure it is perpendicular
	if (fabs(cam_pos_3f.dot(view_up)) == 1) {
		//parallel, create
		view_up = cam_pos_3f.cross(Eigen::Vector3f::UnitX());
	}

	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	cam->SetViewAngle(view_angle_);
	cam->SetClippingRange(z_near, z_far);
	//Invert view Up

//	cam->SetViewUp(0, -1, 0);
//	cam->SetViewUp(0, 1, 0);
	cam->SetViewUp(0.265043, -0.963289, 0.0427419); 
	cam->SetPosition(0, 0, 0);
	if (center_object_in_output_image) {
		cam->SetFocalPoint(x, y, z);
	} else {
		cam->SetFocalPoint(0, 0, 1);
	}
	//use the one below to generate a training image as it focuses on the model, so it is centered in the image -- todo add a flag for this
	cam->Modified();

	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	actor_view->SetMapper(mapper);
//	renderer->SetActiveCamera(cam);
	renderer->AddActor(actor_view);
	renderer->Modified();

	render_win->Render();
	rwi->Start();
	/*
	 * Save Images
	 */

	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImageFilter->SetInput(render_win);
	windowToImageFilter->SetMagnification(1);
	windowToImageFilter->SetInputBufferTypeToRGB();

	vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
	writer->SetFileName(rgb_out.c_str());
	writer->SetInputConnection(windowToImageFilter->GetOutputPort());
	writer->Write();

	cv::Mat depth_img = cv::Mat(rows_, cols_, CV_16UC1, cv::Scalar(0));

	float * depth = new float[cols_ * rows_];
	render_win->GetZbufferData(0, 0, cols_ - 1, rows_ - 1, &(depth[0]));

	for (int y = 0; y < depth_img.rows; y++) {
		for (int x = 0; x < depth_img.cols; x++) {
			float z_buff = depth[y * cols_ + x];
			//because we flip the image around x

			if (z_buff != 1.0) {
				//formula from here: https://en.wikipedia.org/wiki/Z-buffering
				double depth = -1 * ((z_far * z_near) / (z_buff * (z_far - z_near) - z_far));
				depth_img.at<int16_t>(y, x) = (int16_t) depth;

			} else {
				depth_img.at<int16_t>(y, x) = 0;
			}
		}
	}
	//must flip image as it's producing one from camera perspective i.e. upside down
	cv::flip(depth_img, depth_img, 0);
	cv::imwrite(depth_out, depth_img);

	ofstream fout;
	fout.open(pose_file.c_str());

	fout << yaw << ", " << pitch << ", " << roll << endl;
	fout.flush();
	fout.close();
	/*
	 * Interact
	 */
//			rwi->Start();	
//		cout << "finished rendering " << endl;
	render_win->Finalize();
	return rotation_matrix;
}

std::pair<Eigen::Vector3f, Eigen::Vector3f> RenderViews::get_3d_position(Eigen::Vector3f& pos, double additional_yaw) {
	pos.normalize();
	//no - in y as -ve y is positive
	Eigen::Vector3f translation(-pos[0] * radius_sphere_, pos[1] * radius_sphere_, -pos[2] * radius_sphere_);

	//Euler Angels
	Eigen::Matrix3f R = get_rotation_matrix(pos);
	
	double pitch = -get_pitch(R);
	double roll = -get_roll(R);
	double yaw = -get_yaw(R);

	pitch = RAD2DEG(pitch);
	roll = RAD2DEG(roll);
	yaw = RAD2DEG(yaw);
	yaw += additional_yaw;
	//To bring the model to the upright position
//	roll -= 90;

	Eigen::Vector3f rot(yaw, pitch, roll);
	return std::pair<Eigen::Vector3f, Eigen::Vector3f>(translation, rot);
}

std::vector<std::pair<Eigen::Vector3f, vtkSmartPointer<vtkMatrix4x4> > > RenderViews::generate_around_icosahedron_hemisphere_camera_matrix(std::string& mesh_path, double additional_yaw) {
	vtkSmartPointer<vtkPolyData> sphere = get_tesselated_sphere();
	std::vector<Eigen::Vector3f> cam_positions = get_cam_positions(sphere);

	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	cam->SetFocalPoint(0, 0, 0);

//	Eigen::Vector3f cam_pos_3f = cam_positions[0];
//	Eigen::Vector3f perp = cam_pos_3f.cross(Eigen::Vector3f::UnitY());
//	cam->SetViewUp(perp[0], perp[1], perp[2]);
//	double first_cam_pos[3];
//
//	first_cam_pos[0] = cam_positions[0][0] * radius_sphere_;
//	first_cam_pos[1] = cam_positions[0][1] * radius_sphere_;
//	first_cam_pos[2] = cam_positions[0][2] * radius_sphere_;
//
//	cam->SetPosition(first_cam_pos);
//	cam->SetViewAngle(view_angle_);
//	cam->Modified();

	std::vector<std::pair<Eigen::Vector3f, vtkSmartPointer<vtkMatrix4x4> > > poses;

	for (int i = 0; i < cam_positions.size(); i++) {
		//create temporal virtual camera
		vtkSmartPointer<vtkCamera> cam_tmp = vtkSmartPointer<vtkCamera>::New();
		cam_tmp->SetViewAngle(view_angle_);

		double cam_pos[3];
		cam_pos[0] = cam_positions[i][0];
		cam_pos[1] = cam_positions[i][1];
		cam_pos[2] = cam_positions[i][2];

		Eigen::Vector3f cam_pos_3f(static_cast<float>(cam_pos[0]), static_cast<float>(cam_pos[1]), static_cast<float>(cam_pos[2]));
		cam_pos_3f = cam_pos_3f.normalized();
		Eigen::Vector3f test = Eigen::Vector3f::UnitY();

		//If the view up is parallel to ray cam_pos - focalPoint then the transformation
		//is singular and no points are rendered...
		//make sure it is perpendicular
		if (fabs(cam_pos_3f.dot(test)) == 1) {
			//parallel, create
			test = cam_pos_3f.cross(Eigen::Vector3f::UnitX());
		}

		cam_tmp->SetViewUp(test[0], test[1], test[2]);

		for (int k = 0; k < 3; k++) {
			cam_pos[k] = cam_pos[k] * radius_sphere_;
		}

		cam_tmp->SetPosition(cam_pos);
		cam_tmp->SetFocalPoint(0, 0, 0);
		cam_tmp->Modified();


		vtkSmartPointer<vtkMatrix4x4> cam_tmp_view_trans_inverted = vtkSmartPointer<vtkMatrix4x4>::New();
		vtkMatrix4x4::Invert(cam_tmp->GetViewTransformMatrix(), cam_tmp_view_trans_inverted);
		
//		vtkSmartPointer<vtkTransform> trans_rot_pose = vtkSmartPointer<vtkTransform>::New();
//		trans_rot_pose->Identity();
////		trans_rot_pose->Concatenate(roll90);
////		trans_rot_pose->Concatenate(cam_tmp->GetViewTransformMatrix());
//		trans_rot_pose->Concatenate(cam_tmp_view_trans_inverted);
////		trans_rot_pose
		
		vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
//		translation->Translate(first_cam_pos[0] * -1, first_cam_pos[1] * -1, first_cam_pos[2] * -1);

//		vtkSmartPointer<vtkMatrix4x4> matrixRotModel = trans_rot_pose->GetMatrix();

		Eigen::Vector3f pos(cam_pos[0], cam_pos[1], cam_pos[2]);
		poses.push_back(std::pair<Eigen::Vector3f, vtkSmartPointer<vtkMatrix4x4> >(pos, cam_tmp_view_trans_inverted));
	}

	std::cout << "[";
	for (std::vector<std::pair<Eigen::Vector3f, vtkSmartPointer<vtkMatrix4x4> > >::iterator it = poses.begin(); it != poses.end(); it++) {
		std::cout << "[" << it->first[0] << ", " << it->first[1] << ", " << it->first[2] << "];" << std::endl;
	}
	std::cout << "];" << std::endl << std::endl;

	std::cout << "[";
	for (std::vector<std::pair<Eigen::Vector3f, vtkSmartPointer<vtkMatrix4x4> > >::iterator it = poses.begin(); it != poses.end(); it++) {
		double yaw = RAD2DEG(get_yaw(*(it->second)));
		double pitch = RAD2DEG(get_pitch(*(it->second))); 
		double roll = RAD2DEG(get_roll(*(it->second)));
		
		double x = it->first[0];
		double y = it->first[1];
		double z = it->first[2];
		
		std::cout << "[" << yaw << ", "<< pitch << ", "<< roll << "];" << std::endl;
		
	}
	std::cout << "];" << std::endl;

	
	
	return poses;

}

void RenderViews::generate_pcl_method() {
	//center object
	double CoM[3];
	vtkIdType npts_com = 0, *ptIds_com = NULL;
	vtkSmartPointer<vtkCellArray> cells_com = polydata_->GetPolys();

	double center[3], p1_com[3], p2_com[3], p3_com[3], area_com, totalArea_com = 0;
	double comx = 0, comy = 0, comz = 0;
	for (cells_com->InitTraversal(); cells_com->GetNextCell(npts_com, ptIds_com);) {
		polydata_->GetPoint(ptIds_com[0], p1_com);
		polydata_->GetPoint(ptIds_com[1], p2_com);
		polydata_->GetPoint(ptIds_com[2], p3_com);
		vtkTriangle::TriangleCenter(p1_com, p2_com, p3_com, center);
		area_com = vtkTriangle::TriangleArea(p1_com, p2_com, p3_com);
		comx += center[0] * area_com;
		comy += center[1] * area_com;
		comz += center[2] * area_com;
		totalArea_com += area_com;
	}

	CoM[0] = comx / totalArea_com;
	CoM[1] = comy / totalArea_com;
	CoM[2] = comz / totalArea_com;
	cout << " com is " << CoM[0] << ", " << CoM[1] << ", " << CoM[2] << endl;
	vtkSmartPointer<vtkTransform> trans_center = vtkSmartPointer<vtkTransform>::New();
	trans_center->Translate(-CoM[0], -CoM[1], -CoM[2]);
	vtkSmartPointer<vtkMatrix4x4> matrixCenter = trans_center->GetMatrix();

	vtkSmartPointer<vtkTransformFilter> trans_filter_center = vtkSmartPointer<vtkTransformFilter>::New();
	trans_filter_center->SetTransform(trans_center);
	trans_filter_center->SetInput(polydata_);
	trans_filter_center->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(trans_filter_center->GetOutputPort());
	mapper->Update();

	//scale so it fits in the unit sphere!
	double bb[6];
	mapper->GetBounds(bb);

	double ms = (std::max)((std::fabs)(bb[0] - bb[1]), (std::max)((std::fabs)(bb[2] - bb[3]), (std::fabs)(bb[4] - bb[5])));
	double max_side = radius_sphere_ / 2.0;
	double scale_factor = max_side / ms;

	vtkSmartPointer<vtkTransform> trans_scale = vtkSmartPointer<vtkTransform>::New();
	trans_scale->Scale(scale_factor, scale_factor, scale_factor);
	vtkSmartPointer<vtkMatrix4x4> matrixScale = trans_scale->GetMatrix();

	vtkSmartPointer<vtkTransformFilter> trans_filter_scale = vtkSmartPointer<vtkTransformFilter>::New();
	trans_filter_scale->SetTransform(trans_scale);
	trans_filter_scale->SetInputConnection(trans_filter_center->GetOutputPort());
	trans_filter_scale->Update();

	mapper->SetInputConnection(trans_filter_scale->GetOutputPort());
	mapper->Update();

	//////////////////////////////
	// * Compute area of the mesh
	//////////////////////////////
	vtkSmartPointer<vtkCellArray> cells = mapper->GetInput()->GetPolys();
	vtkIdType npts = 0, *ptIds = NULL;

	double p1[3], p2[3], p3[3], area, totalArea = 0;
	for (cells->InitTraversal(); cells->GetNextCell(npts, ptIds);) {
		polydata_->GetPoint(ptIds[0], p1);
		polydata_->GetPoint(ptIds[1], p2);
		polydata_->GetPoint(ptIds[2], p3);
		area = vtkTriangle::TriangleArea(p1, p2, p3);
		totalArea += area;
	}

	//create icosahedron
	vtkSmartPointer<vtkPlatonicSolidSource> ico = vtkSmartPointer<vtkPlatonicSolidSource>::New();
	ico->SetSolidTypeToIcosahedron();
	ico->Update();

	//tesselate cells from icosahedron
	vtkSmartPointer<vtkLoopSubdivisionFilter> subdivide = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
	subdivide->SetNumberOfSubdivisions(tesselation_level_);
	subdivide->SetInputConnection(ico->GetOutputPort());

	// Get camera positions
	vtkPolyData *sphere = subdivide->GetOutput();
	sphere->Update();

	std::vector<Eigen::Vector3f> cam_positions;
	if (!use_vertices_) {
		vtkSmartPointer<vtkCellArray> cells_sphere = sphere->GetPolys();
		cam_positions.resize(sphere->GetNumberOfPolys());

		size_t i = 0;
		for (cells_sphere->InitTraversal(); cells_sphere->GetNextCell(npts_com, ptIds_com);) {
			sphere->GetPoint(ptIds_com[0], p1_com);
			sphere->GetPoint(ptIds_com[1], p2_com);
			sphere->GetPoint(ptIds_com[2], p3_com);
			vtkTriangle::TriangleCenter(p1_com, p2_com, p3_com, center);
			cam_positions[i] = Eigen::Vector3f(float(center[0]), float(center[1]), float(center[2]));
			i++;
		}

	} else {
		cam_positions.resize(sphere->GetNumberOfPoints());
		for (int i = 0; i < sphere->GetNumberOfPoints(); i++) {
			double cam_pos[3];
			sphere->GetPoint(i, cam_pos);
			cam_positions[i] = Eigen::Vector3f(float(cam_pos[0]), float(cam_pos[1]), float(cam_pos[2]));
		}
	}

	double camera_radius = radius_sphere_;
	double cam_pos[3];
	double first_cam_pos[3];

	first_cam_pos[0] = cam_positions[0][0] * radius_sphere_;
	first_cam_pos[1] = cam_positions[0][1] * radius_sphere_;
	first_cam_pos[2] = cam_positions[0][2] * radius_sphere_;

	//create renderer and window
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	renderer->SetBackground(1.0, 0, 0);

	//create picker
	vtkSmartPointer<vtkWorldPointPicker> worldPicker = vtkSmartPointer<vtkWorldPointPicker>::New();

	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	cam->SetFocalPoint(0, 0, 0);

	Eigen::Vector3f cam_pos_3f = cam_positions[0];
	Eigen::Vector3f perp = cam_pos_3f.cross(Eigen::Vector3f::UnitY());
	cam->SetViewUp(perp[0], perp[1], perp[2]);

	cam->SetPosition(first_cam_pos);
	cam->SetViewAngle(view_angle_);
	cam->Modified();

	//For each camera position, traposesnsform the object and render view
	for (size_t i = 0; i < cam_positions.size(); i++) {
		cam_pos[0] = cam_positions[i][0];
		cam_pos[1] = cam_positions[i][1];
		cam_pos[2] = cam_positions[i][2];

		//create temporal virtual camera
		vtkSmartPointer<vtkCamera> cam_tmp = vtkSmartPointer<vtkCamera>::New();
		cam_tmp->SetViewAngle(view_angle_);

		Eigen::Vector3f cam_pos_3f(static_cast<float>(cam_pos[0]), static_cast<float>(cam_pos[1]), static_cast<float>(cam_pos[2]));
		cam_pos_3f = cam_pos_3f.normalized();
		Eigen::Vector3f test = Eigen::Vector3f::UnitY();

		//If the view up is parallel to ray cam_pos - focalPoint then the transformation
		//is singular and no points are rendered...
		//make sure it is perpendicular
		if (fabs(cam_pos_3f.dot(test)) == 1) {
			//parallel, create
			test = cam_pos_3f.cross(Eigen::Vector3f::UnitX());
		}

		cam_tmp->SetViewUp(test[0], test[1], test[2]);

		for (int k = 0; k < 3; k++) {
			cam_pos[k] = cam_pos[k] * camera_radius;
		}

		cam_tmp->SetPosition(cam_pos);
		cam_tmp->SetFocalPoint(0, 0, 0);
		cam_tmp->Modified();

		//rotate model so it looks the same as if we would look from the new position
		vtkSmartPointer<vtkMatrix4x4> view_trans_inverted = vtkSmartPointer<vtkMatrix4x4>::New();
		vtkMatrix4x4::Invert(cam->GetViewTransformMatrix(), view_trans_inverted);
		vtkSmartPointer<vtkTransform> trans_rot_pose = vtkSmartPointer<vtkTransform>::New();
		trans_rot_pose->Identity();
		trans_rot_pose->Concatenate(view_trans_inverted);
		trans_rot_pose->Concatenate(cam_tmp->GetViewTransformMatrix());
		vtkSmartPointer<vtkTransformFilter> trans_rot_pose_filter = vtkSmartPointer<vtkTransformFilter>::New();
		trans_rot_pose_filter->SetTransform(trans_rot_pose);
		trans_rot_pose_filter->SetInputConnection(trans_filter_scale->GetOutputPort());

		//translate model so we can place camera at (0,0,0)
		vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
		translation->Translate(first_cam_pos[0] * -1, first_cam_pos[1] * -1, first_cam_pos[2] * -1);
		vtkSmartPointer<vtkTransformFilter> translation_filter = vtkSmartPointer<vtkTransformFilter>::New();
		translation_filter->SetTransform(translation);
		translation_filter->SetInputConnection(trans_rot_pose_filter->GetOutputPort());

		//modify camera
		cam_tmp->SetPosition(0, 0, 0);
		cam_tmp->SetFocalPoint(first_cam_pos[0] * -1, first_cam_pos[1] * -1, first_cam_pos[2] * -1);
		cam_tmp->Modified();

		//notice transformations for final pose
		vtkSmartPointer<vtkMatrix4x4> matrixRotModel = trans_rot_pose->GetMatrix();
		vtkSmartPointer<vtkMatrix4x4> matrixTranslation = translation->GetMatrix();

		mapper->SetInputConnection(translation_filter->GetOutputPort());
		mapper->Update();

		//render view
		vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
		actor_view->SetMapper(mapper);
		renderer->SetActiveCamera(cam_tmp);
		renderer->AddActor(actor_view);
		renderer->Modified();
		//renderer->ResetCameraClippingRange ();
		render_win->Render();

		//back to real scale transform
		vtkSmartPointer<vtkTransform> backToRealScale = vtkSmartPointer<vtkTransform>::New();
		backToRealScale->PostMultiply();
		backToRealScale->Identity();
		backToRealScale->Concatenate(matrixScale);
		backToRealScale->Concatenate(matrixTranslation);
		backToRealScale->Inverse();
		backToRealScale->Modified();
		backToRealScale->Concatenate(matrixTranslation);
		backToRealScale->Modified();

		Eigen::Matrix4f backToRealScale_eigen;
		backToRealScale_eigen.setIdentity();

		for (int x = 0; x < 4; x++)
			for (int y = 0; y < 4; y++)
				backToRealScale_eigen(x, y) = float(backToRealScale->GetMatrix()->GetElement(x, y));

	}
}

void RenderViews::generate_around_icosahedron_hemisphere_euler_angles(std::string& mesh_path, double additional_yaw) {
	vtkSmartPointer<vtkPolyData> sphere = get_tesselated_sphere();
	
	std::vector<Eigen::Vector3f> cam_positions = get_cam_positions(sphere);
//	std::cout << "[";
	std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f> > positions3d;
	for (std::vector<Eigen::Vector3f>::iterator it = cam_positions.begin(); it != cam_positions.end(); it++) {
		it->normalize();
//		std::cout << "[" << (*it)[0] << ", " << (*it)[1] << ", " << (*it)[2] << "];" << std::endl;
		positions3d.push_back(get_3d_position(*it, additional_yaw));
	}
//	std::cout << "]" << std::endl;

//	std::cout << std::endl << "[";
//	for (std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f> >::iterator it = positions3d.begin(); it != positions3d.end(); it++) {
//		std::cout << "[" << (*it).second[0] << ", " << (*it).second[1] << ", " << (*it).second[2] << "];" << std::endl;
//	}
//	std::cout << "]" << std::endl;
//	std::cout << "there are "<< positions3d.size() << " images "<< std::endl;
//	exit(-1);

	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();

	actor_view->SetMapper(mapper);
	actor_view->Modified();

	renderer->SetActiveCamera(cam);
	renderer->AddActor(actor_view);
	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();

	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	//white
	render_win->Modified();

	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();

	for (int i = 0; i < (int) positions3d.size(); i++) {
		vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();

		std::pair<Eigen::Vector3f, Eigen::Vector3f>& pos = positions3d.at(i);

		double x = pos.first[0];
		double y = pos.first[1];
		double z = pos.first[2];

		double yaw = pos.second[0];
		double pitch = pos.second[1];
		double roll = pos.second[2];

		fileReader->SetFileName(mesh_path.c_str());
		fileReader->Update();

		polydata_ = fileReader->GetOutput();

		std::stringstream rgb_file;
		std::stringstream depth_file;
		std::stringstream pose_file;

		rgb_file << image_prefix << "yaw_" << yaw << "_" << i << "_rgb.png";
		depth_file << image_prefix << "yaw_" << yaw << "_" << i << "_depth.png";
		pose_file << image_prefix << "yaw_" << yaw << "_" << i << "_pose.txt";

		std::string rgb_out = rgb_file.str();
		std::string depth_out = depth_file.str();
		bool center_object_in_output_image = true;

		vtkSmartPointer<vtkMatrix4x4> transformation_mat = generate_specific_view(0, 0, radius_sphere_, yaw, pitch, roll, render_win, cam, mapper, windowToImageFilter, rgb_out, depth_out,
				center_object_in_output_image);

		transformation_mat->Element[0][3] = x;
		transformation_mat->Element[1][3] = y;
		transformation_mat->Element[2][3] = z;
		transformation_mat->Element[3][3] = 1.f;

		ofstream fout;
		fout.open(pose_file.str().c_str());

		fout << yaw << ", " << pitch << ", " << roll << endl;

		fout << "transformation_mat" << endl;
		for (int k = 0; k < 4; k++) {
			for (int j = 0; j < 4; j++) {
				fout << transformation_mat->Element[k][j] << ",";
			}
			fout << endl;
		}

		fout.flush();
		fout.close();
	}
}

void RenderViews::generate_around_icosahedron_hemisphere(std::string& mesh_path, double additional_yaw) {

	vtkSmartPointer<vtkPolyData> sphere = get_tesselated_sphere();
	std::vector<Eigen::Vector3f> cam_positions = get_cam_positions(sphere);
	std::cout << "[";
	for (std::vector<Eigen::Vector3f>::iterator it = cam_positions.begin(); it != cam_positions.end(); it++) {
		it->normalize();
	}

	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();

	actor_view->SetMapper(mapper);
	actor_view->Modified();

	renderer->SetActiveCamera(cam);
	renderer->AddActor(actor_view);
	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();

	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	//white
	render_win->Modified();

	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();

	for (int i = 0; i < (int) cam_positions.size(); i++) {
		//so always begins at default position //TODO: actually pass this to the function
		vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();

		fileReader->SetFileName(mesh_path.c_str());
		fileReader->Update();

		polydata_ = fileReader->GetOutput();
		double* bounds = polydata_->GetBounds();
		std::cout << "z bounds are: " << bounds[4] << ", " << bounds[5] << std::endl;

		exit(-1);

		cam_positions[i].normalize();

		cout << " doing number " << i << endl;
		double x = -cam_positions[i][0] * radius_sphere_;
		double y = -cam_positions[i][1] * radius_sphere_;
		double z = -cam_positions[i][2] * radius_sphere_;

		//Flip in y to meet camera coordinate system
		cam_positions[i][1] = -cam_positions[i][1];

//		cout <<" camera position is ("<< cam_positions[i][0] << ", " << cam_positions[i][1] << ", " << cam_positions[i][2] << ")" << endl;
		Eigen::Matrix3f R = get_rotation_matrix(cam_positions[i]);
//		cout <<" got camera rot mat "<< endl;

		double pitch = get_pitch(R);
		double roll = get_roll(R);
		pitch = RAD2DEG(pitch);
		roll = RAD2DEG(roll);
		double yaw = additional_yaw;

		cout << "yaw is " << additional_yaw << " pitch: " << pitch << "  roll: " << roll << endl;
		std::stringstream rgb_file;
		std::stringstream depth_file;
		std::stringstream pose_file;

		rgb_file << image_prefix << "yaw_" << yaw << "_" << i << "_rgb.png";
		depth_file << image_prefix << "yaw_" << yaw << "_" << i << "_depth.png";
		pose_file << image_prefix << "yaw_" << yaw << "_" << i << "_pose.txt";

		std::string rgb_out = rgb_file.str();
		std::string depth_out = depth_file.str();
//		cout <<" view to generate is at  x: " << x << " y: " << y << " z: " << z << " yaw " << yaw << " pitch " << pitch << " roll " << roll << endl;
		bool center_object_in_output_image = true;

//		vtkSmartPointer<vtkMatrix4x4> transformation_mat = generate_specific_view(0, 0, radius_sphere_, yaw, pitch, roll, rgb_out, depth_out,center_object_in_output_image);
		vtkSmartPointer<vtkMatrix4x4> transformation_mat = generate_specific_view(0, 0, radius_sphere_, yaw, pitch, roll, render_win, cam, mapper, windowToImageFilter, rgb_out, depth_out,
				center_object_in_output_image);
//		cout <<" got transformation mat "<< endl;

		transformation_mat->Element[0][3] = x;
		transformation_mat->Element[1][3] = y;
		transformation_mat->Element[2][3] = z;
		transformation_mat->Element[3][3] = 1.f;

		ofstream fout;
		fout.open(pose_file.str().c_str());

		fout << yaw << ", " << pitch << ", " << roll << endl;

		fout << "transformation_mat" << endl;
		for (int k = 0; k < 4; k++) {
			for (int j = 0; j < 4; j++) {
				fout << transformation_mat->Element[k][j] << ",";
			}
			fout << endl;
		}

		fout.flush();
		fout.close();
//		polydata_ = 0;
//		fileReader = 0;
//		if(i > 68){
//			exit(-1);
//		}
//		usleep(100000);

	}
}

void RenderViews::generate_all_views(double additional_roll) {

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	/*********************************
	 * Transform object CoM to the origin
	 **********************************/
	//center object
	double CoM[3];
	get_centre_of_mass(CoM);

	//This shifts the object CoM to the origin
	vtkSmartPointer<vtkTransform> trans_center = vtkSmartPointer<vtkTransform>::New();
	trans_center->Translate(-CoM[0], -CoM[1], -CoM[2]);
	vtkSmartPointer<vtkMatrix4x4> matrixCenter = trans_center->GetMatrix();

	vtkSmartPointer<vtkTransformFilter> trans_filter_center = vtkSmartPointer<vtkTransformFilter>::New();
	trans_filter_center->SetTransform(trans_center);
	trans_filter_center->SetInput(polydata_);
	trans_filter_center->Update();
	//end - object now transformed s.t. the CoM is now at the origin

	mapper->SetInputConnection(trans_filter_center->GetOutputPort());
	mapper->Update();

	vtkSmartPointer<vtkPolyData> sphere = get_tesselated_sphere();
	std::vector<Eigen::Vector3f> cam_positions = get_cam_positions(sphere);

	double camera_radius = radius_sphere_;

	//create renderer and window
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	render_win->AddRenderer(renderer);
	render_win->SetInteractor(rwi);
	render_win->SetSize(cols_, rows_);
	//white
	renderer->SetBackground(1.0, 1.0, 1.0);

	double cam_pos[3];
	int image_id = 0;

//	for (size_t i = 0; i < cam_positions.size(); i++) {
	for (size_t i = 0; i < 1; i++) {
		cam_pos[0] = cam_positions[i][0];
		cam_pos[1] = cam_positions[i][1];
		cam_pos[2] = cam_positions[i][2];

		Eigen::Vector3f cam_pos_3f(static_cast<float>(cam_pos[0]), static_cast<float>(cam_pos[1]), static_cast<float>(cam_pos[2]));
		cam_pos_3f = cam_pos_3f.normalized();
		Eigen::Vector3f view_up = Eigen::Vector3f::UnitY();

		//If the view up is parallel to ray cam_pos - focalPoint then the transformation
		//is singular and no points are rendered...
		//make sure it is perpendicular
		if (fabs(cam_pos_3f.dot(view_up)) == 1) {
			//parallel, create
			view_up = cam_pos_3f.cross(Eigen::Vector3f::UnitX());
		}

		for (int k = 0; k < 3; k++) {
			cam_pos[k] = cam_pos[k] * camera_radius;
		}

		std::cout << " cam pos is " << cam_pos[0] << ", " << cam_pos[1] << ", " << cam_pos[2] << std::endl;

		vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
		cam->SetViewAngle(view_angle_);
		cam->SetClippingRange(z_near, z_far);
//		cam->SetViewUp(view_up[0], view_up[1], view_up[2]);
		cam->SetViewUp(0, -1, 0);
//		cam->SetPosition(cam_pos[0], cam_pos[1], cam_pos[2]);
//		cam->SetFocalPoint(0, 0, 0);
		cam->Modified();

		vtkSmartPointer<vtkTransform> trans_camera = vtkSmartPointer<vtkTransform>::New();
		trans_camera->Translate(-cam_pos[0], -cam_pos[1], -cam_pos[2]);
		vtkSmartPointer<vtkTransformFilter> filter_translate_cam_to_origin = vtkSmartPointer<vtkTransformFilter>::New();
		filter_translate_cam_to_origin->SetTransform(trans_camera);
		filter_translate_cam_to_origin->SetInputConnection(trans_filter_center->GetOutputPort());		// TODO::::: this should be the trans _centre
		filter_translate_cam_to_origin->Update();

		mapper->SetInputConnection(filter_translate_cam_to_origin->GetOutputPort());
		mapper->Update();

		cam->SetPosition(0, 0, 0);		// Camera is now at origin 
		cam->SetFocalPoint(-cam_pos[0], -cam_pos[1], -cam_pos[2]);
		cam->Modified();

//		cout << "cam pos is (" << cam_pos[0] << ", " << cam_pos[1]  << ", " << cam_pos[2] << ") " << endl;

		mapper->SetInputConnection(filter_translate_cam_to_origin->GetOutputPort());
		mapper->Update();

		//now we have a real camera position 
		//render view
		vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
		actor_view->SetMapper(mapper);
		renderer->SetActiveCamera(cam);
		renderer->AddActor(actor_view);
		renderer->Modified();

		double* orientation = cam->GetOrientation();

		//rotation in VTK is done over the z, x and then y 
		double pitch = orientation[0];		// about x
		double yaw = orientation[1];		// about y // should be roll
		double roll = orientation[2];		// about z // should be yaw

		std::cout << " orientation is " << orientation[0] << ", " << orientation[1] << ", " << orientation[2] << std::endl;

		if (roll < 0.01) {
			roll = 0;
		}
		roll += additional_roll; // for in-plane rotations

		Eigen::Matrix3f m;
		m = Eigen::AngleAxisf(DEG2RAD(roll), Eigen::Vector3f::UnitZ()) * Eigen::AngleAxisf(DEG2RAD(yaw), Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(DEG2RAD(pitch), Eigen::Vector3f::UnitX());
		// this is the object rotation matrix
		Eigen::Matrix3f object_rotation = m.transpose();

		render_win->Render();
		rwi->Start();

		if (should_save_images_and_poses()) {

			vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
			windowToImageFilter->SetInput(render_win);
			windowToImageFilter->SetMagnification(1); //set the resolution of the output image (3 times the current resolution of vtk render window)
			windowToImageFilter->SetInputBufferTypeToRGB(); //also record the alpha (transparency)   channel
			std::stringstream ss;
			ss << image_prefix << "roll_" << roll << "_" << image_id << "_rgb.png";

			vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
			writer->SetFileName(ss.str().c_str());
			writer->SetInputConnection(windowToImageFilter->GetOutputPort());
			writer->Write();

			cv::Mat depth_img = cv::Mat(rows_, cols_, CV_16UC1, cv::Scalar(0));

			float * depth = new float[cols_ * rows_];
			render_win->GetZbufferData(0, 0, cols_ - 1, rows_ - 1, &(depth[0]));

			for (int y = 0; y < depth_img.rows; y++) {
				for (int x = 0; x < depth_img.cols; x++) {
					float z_buff = depth[y * cols_ + x];
					if (z_buff != 1.0) {
						//formula from here: https://en.wikipedia.org/wiki/Z-buffering
						double depth = -1 * ((z_far * z_near) / (z_buff * (z_far - z_near) - z_far));
						depth_img.at<int16_t>(y, x) = (int16_t) depth;
					} else {
						depth_img.at<int16_t>(y, x) = 0;
					}
				}
			}

			//must flip image as it's producing one from camera perspective i.e. upside down
			cv::flip(depth_img, depth_img, 0);

			std::stringstream ds;
			ds << image_prefix << "roll_" << roll << "_" << image_id << "_depth.png";
			cv::imwrite(ds.str(), depth_img);

			std::stringstream ps;

			ps << image_prefix << "roll_" << roll << "_" << image_id << "_pose.txt";

			ofstream fout;
			fout.open(ps.str().c_str());

			fout << pitch << ", " << yaw << ", " << roll << endl;

			fout << "rot_mat" << endl;
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					fout << object_rotation(i, j) << ",";
				}
				fout << endl;
			}
			fout << "translation" << endl;
			fout << -1 * cam_pos[0] << "," << -1 * cam_pos[1] << "," << -1 * cam_pos[2] << endl;
			fout.flush();
			fout.close();

			image_id++;
		}

		mapper->SetInputConnection(trans_filter_center->GetOutputPort());
		mapper->Update();

	}
}
