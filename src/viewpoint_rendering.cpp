/**
 * Demo program for simulation library
 * A virtual camera generates simulated point clouds
 * No visual output, point clouds saved to file
 * 
 * three different demo modes:
 * 0 - static camera, 100 poses
 * 1 - circular camera flying around the scene, 16 poses
 * 2 - camera translates between 2 poses using slerp, 20 poses
 * pcl_sim_terminal_demo 2 ../../../../kmcl/models/table_models/meta_model.ply  
 */

#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include <boost/shared_ptr.hpp>
#ifdef _WIN32
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
#endif

#include "render_views.hpp"
#include <vtk/vtkPolyDataMapper.h>
#include <vtkPLYReader.h>
#include <vtkPLYWriter.h>
#include <vtk/vtkSmartPointer.h>
#include <vtkSelection.h>
#include <vtkCellArray.h>
#include <vtkTransformFilter.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointPicker.h>
#include <vtkCleanPolyData.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkCubeSource.h>
#include <opencv2/opencv.hpp>
#include <vtkExtractEdges.h>
#include <vtkArrowSource.h>

vtkSmartPointer<vtkTransformPolyDataFilter> rotate_and_save_mesh(vtkSmartPointer<vtkPolyData> poly_data, double rotation_angle, double x, double y, double z, std::string& out_name) {

	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = rotate_mesh(poly_data, rotation_angle, x, y, z);

	vtkSmartPointer<vtkPLYWriter> writer = vtkSmartPointer<vtkPLYWriter>::New();
	writer->SetInputConnection(transformFilter->GetOutputPort());
	writer->SetFileName(out_name.c_str());
	writer->Write();

	return transformFilter;
}

vtkSmartPointer<vtkTransformPolyDataFilter> apply_transform(vtkSmartPointer<vtkPolyData> poly_data, vtkSmartPointer<vtkTransform>& transform) {
	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();

	transformFilter->SetTransform(transform);
	transformFilter->SetInput(poly_data);
	transformFilter->Update();
	return transformFilter;
}

#include "vtkSphereSource.h"
#include "vtkProperty.h"

void show_mesh(std::string& name) {
	vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();
	fileReader->SetFileName(name.c_str());
	fileReader->Update();

	double bounds[6];
	fileReader->GetOutput()->GetBounds(bounds);

	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl;

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(fileReader->GetOutputPort());
	mapper->Update();

	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	//white
	renderer->SetBackground(1.0, 1.0, 1.0);

	renderWindowInteractor->SetRenderWindow(render_win);

	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	actor_view->SetMapper(mapper);
	renderer->AddActor(actor_view);
	renderer->Modified();

	render_win->Render();
	renderWindowInteractor->Start();

}
#include <vtkSimplePointsReader.h>



void overlay_synthetic_imgae(cv::Mat& synthetic_depth, cv::Mat& synthetic_rgb, cv::Mat& real_depth, cv::Mat& real_rgb) {
	cv::Mat seg_depth = cv::Mat(real_depth.rows, real_depth.cols, CV_16UC1, cv::Scalar(0));
	std::vector<std::pair<int, int> > vals;
	cv::Mat depthmask = cv::Mat(synthetic_rgb.rows, synthetic_rgb.cols, CV_8UC1, cv::Scalar(0));
	for (int y = 0; y < synthetic_rgb.rows; y++) {
		for (int x = 0; x < synthetic_rgb.cols; x++) {
			if (!(synthetic_rgb.at<cv::Vec3b>(y, x)[0] == 255 && synthetic_rgb.at<cv::Vec3b>(y, x)[1] == 255 && synthetic_rgb.at<cv::Vec3b>(y, x)[2] == 255)) {
//				real_rgb.at < cv::Vec3b > (y + 3 -21, x + 5 + 6)[0] = synthetic_rgb.at<cv::Vec3b>(y,x)[0];
//				real_rgb.at < cv::Vec3b > (y + 3 -21, x + 5 + 6)[1] = synthetic_rgb.at<cv::Vec3b>(y,x)[1];
//				real_rgb.at < cv::Vec3b > (y + 3 -21, x + 5 + 6)[2] = synthetic_rgb.at<cv::Vec3b>(y,x)[2];

				real_rgb.at<cv::Vec3b>(y + 3, x + 5)[0] = synthetic_rgb.at<cv::Vec3b>(y, x)[0];
				real_rgb.at<cv::Vec3b>(y + 3, x + 5)[1] = synthetic_rgb.at<cv::Vec3b>(y, x)[1];
				real_rgb.at<cv::Vec3b>(y + 3, x + 5)[2] = synthetic_rgb.at<cv::Vec3b>(y, x)[2];

//				real_rgb.at < cv::Vec3b > (y + 3, x + 5 )[0] = synthetic_rgb.at<cv::Vec3b>(y,x)[0];
//				real_rgb.at < cv::Vec3b > (y + 3 , x + 5 )[1] = synthetic_rgb.at<cv::Vec3b>(y,x)[1];
//				real_rgb.at < cv::Vec3b > (y + 3 , x + 5 )[2] = synthetic_rgb.at<cv::Vec3b>(y,x)[2];
				depthmask.at<uchar>(y + 3, x + 5) = 1;
			}
		}
	}
	cv::Mat vis_depth_gen = get_visualizable_depth_img(synthetic_depth);
	cv::Mat vis_depth = get_visualizable_depth_img(real_depth);

//	cv::imwrite("segmented_depth_color_100.png", real_depth);
//	cv::imwrite("synthetic_depth_color_100.png", synthetic_depth);
//	cv::imwrite("segmented_color_100.png", real_rgb);
	cv::imshow("depth_gen", vis_depth_gen);
	cv::imshow("rgb_gen", synthetic_rgb);
	cv::imshow("rgb", real_rgb);
	cv::imshow("depth", vis_depth);

	cv::waitKey(0);
}

#include <pcl-1.7/apps/include/pcl/apps/render_views_tesselated_sphere.h>

std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > get_poses(std::string& mesh_name, bool use_vertices, int tesselation_level, float scale){
	std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > poses;
	
	pcl::apps::RenderViewsTesselatedSphere rvts;

	rvts.setRadiusSphere(scale);
	rvts.setTesselationLevel(tesselation_level);
	rvts.setUseVertices(use_vertices);

	vtkSmartPointer<vtkPLYReader> fileReader_demo = vtkSmartPointer<vtkPLYReader>::New();
	fileReader_demo->SetFileName(mesh_name.c_str());
	fileReader_demo->Update();
	//No transformation needed
	vtkSmartPointer<vtkPolyData> poly;
	poly = fileReader_demo->GetOutput();

	rvts.addModelFromPolyData(poly);
	rvts.generateViews();
	rvts.getPoses(poses);

	return poses;
}


void render_views_pcl(std::string& mesh_name, std::string& image_prefix, 
		std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> >& poses, 
		double scale,double additional_yaw) {

	int i = 0; 
	for(std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> >::iterator it = poses.begin(); it != poses.end(); it++){
		double x = (*it)(0,3)*scale;
		double y = (*it)(1,3)*scale;
		double z = (*it)(2,3)*scale;
		
		if(y >= 0){
			Eigen::Matrix4f& R = (*it);
			double yaw = RAD2DEG(std::atan2(R(1,0), R(0,0))) + additional_yaw;
			double pitch = RAD2DEG(std::atan2(-R(2,0), std::sqrt(std::pow(R(2,1),2) + std::pow(R(2,2),2))));
			double roll = RAD2DEG(std::atan2(R(2,1),R(2,2)));
			
			std::stringstream rgb_file;
			std::stringstream depth_file;
			std::stringstream pose_file;
			rgb_file << image_prefix << "yaw_" << additional_yaw << "_" << i << "_rgb.png";
			depth_file << image_prefix << "yaw_" << additional_yaw << "_" << i << "_depth.png";
			pose_file << image_prefix << "yaw_" << additional_yaw << "_" << i << "_pose.txt";
			
			std::string rgb_out = rgb_file.str();
			std::string depth_out = depth_file.str();
			std::string pose_out = pose_file.str();
			
			generate_image_at_specific_position(mesh_name, rgb_out, depth_out, pose_out, x, y, z, yaw, pitch, roll, true);
			
			i++;
//			cv::Mat tmp = cv::imread(rgb_out, CV_LOAD_IMAGE_ANYCOLOR);
//			std::cout <<" (x,y,z) = (" << x << ", "<< y << ", " << z << ") (yaw, pitch, roll) = (" << yaw << ", " << pitch << ", "<< roll << ")" << std::endl;
//			cv::imshow("t", tmp);
//			cv::waitKey(0);
		}
		
	}
}


void visualize_polydata(std::string& mesh, vtkSmartPointer<vtkMatrix4x4>& rot, double* pos){
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	double com[3];

	actor_view->SetMapper(mapper);
	actor_view->Modified();

	renderer->SetActiveCamera(cam);

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();

	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	//white
	render_win->Modified();

	rwi->SetRenderWindow(render_win);

	vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();

	fileReader->SetFileName(mesh.c_str());
	fileReader->Update();

	vtkSmartPointer<vtkPolyData> polydata_ = fileReader->GetOutput();

	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	//First to CoM
	translation->Translate(-com[0], -com[1], -com[2]);
	translation->Translate(-pos[0], -pos[1], -pos[2]);
	translation->Concatenate(rot);
	
	//	translation->Translate(0, 0, -scale);
	//Then to point
	//	translation->Translate(x, y, z);

	vtkSmartPointer<vtkTransformFilter> translation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_filter->SetTransform(translation);
	translation_filter->SetInput(polydata_);
	translation_filter->Update();

	mapper->SetInputConnection(translation_filter->GetOutputPort());
	mapper->Update();

	renderer->AddActor(actor_view);
	
	renderer->Render();
	rwi->Start();
}



int icosphere_generation_main(int argc, char **argv) {
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	vtkSmartPointer<vtkActor> actor_view = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	double com[3];
	float scale = 1050;
	
	actor_view->SetMapper(mapper);
	actor_view->Modified();

	renderer->SetActiveCamera(cam);
	
	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	

	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	//white
	render_win->Modified();
	
	rwi->SetRenderWindow(render_win);

	
	vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();

	fileReader->SetFileName("benchvise_mesh.ply");
	fileReader->Update();

	vtkSmartPointer<vtkPolyData> polydata_ = fileReader->GetOutput();

	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	//First to CoM
	translation->Translate(-com[0], -com[1], -com[2]);
//	translation->Translate(0, 0, -scale);
	//Then to point
//	translation->Translate(x, y, z);

	vtkSmartPointer<vtkTransformFilter> translation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_filter->SetTransform(translation);
	translation_filter->SetInput(polydata_);
	translation_filter->Update();

	mapper->SetInputConnection(translation_filter->GetOutputPort());
	mapper->Update();
	
	renderer->AddActor(actor_view);
	
	/*
	 * Draw icosphere
	 */
	
	// Sphere 2
	
	vtkSmartPointer<vtkPlatonicSolidSource> ico = vtkSmartPointer<vtkPlatonicSolidSource>::New();
	ico->SetSolidTypeToIcosahedron();
	ico->Update();

	//tesselate cells from icosahedron
	vtkSmartPointer<vtkLoopSubdivisionFilter> subdivide = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
	subdivide->SetNumberOfSubdivisions(2);
	subdivide->SetInputConnection(ico->GetOutputPort());

	
	vtkSmartPointer<vtkPolyData> icosphere = subdivide->GetOutput();
	icosphere->Update();

	std::cout <<" icosphere has " << icosphere->GetNumberOfPoints() << std::endl;
	
	calculate_center_of_mass(icosphere, com);
	
	std::cout <<" icosphere com is " << com[0] << ", " << com[1] << ", " << com[2] << std::endl;
	vtkSmartPointer<vtkTransform> icosphere_transformation = vtkSmartPointer<vtkTransform>::New();
	//First to CoM
	icosphere_transformation->Translate(-com[0], -com[1], -com[2]);
//	icosphere_transformation->Translate(0, 0, -scale);
	//Then Scale
	icosphere_transformation->Scale(scale, scale, scale);
	//Then to point
	
	vtkSmartPointer<vtkTransformFilter> ico_transformation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	ico_transformation_filter->SetTransform(icosphere_transformation);
	ico_transformation_filter->SetInput(icosphere);
	ico_transformation_filter->Update();

	
	vtkSmartPointer<vtkExtractEdges> extractEdges = vtkSmartPointer<vtkExtractEdges>::New();
	extractEdges->SetInputConnection(ico_transformation_filter->GetOutputPort());
	extractEdges->Update();
	
	unsigned char red[3] = { 255, 0, 0 };
	unsigned char white[3] = { 255, 255, 255 };
	unsigned char grey[3] = { 211, 211, 211 };
	vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
	colors->SetNumberOfComponents(3);
	colors->SetName("Colors");

	
	int num_points = extractEdges->GetOutput()->GetNumberOfPoints();
	std::vector<Eigen::Vector3f> cam_positions;
	int index = 0;
	for(int i =0 ; i < num_points; i++){
		if(extractEdges->GetOutput()->GetPoint(i)[1] >= 0){
			Eigen::Vector3f cam_pos(extractEdges->GetOutput()->GetPoint(i)[0], extractEdges->GetOutput()->GetPoint(i)[1], extractEdges->GetOutput()->GetPoint(i)[2]);
			cam_positions.push_back(cam_pos);
			if(index == 55){
				colors->InsertNextTupleValue(red);	
			}
			else{
				colors->InsertNextTupleValue(grey);
			}
			index++;
		}
		else{
			colors->InsertNextTupleValue(white);
		}
	}
	
	
	extractEdges->GetOutput()->GetPointData()->SetScalars(colors);
	
	// Create a mapper
	vtkSmartPointer<vtkPolyDataMapper> sphere_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	sphere_mapper->SetInputConnection(extractEdges->GetOutputPort());
	sphere_mapper->Update();
	
	// Create an actor
	vtkSmartPointer<vtkActor> sphere_actor = vtkSmartPointer<vtkActor>::New();
	sphere_actor->SetMapper(sphere_mapper);
	sphere_actor->Modified();
	
	renderer->AddActor(sphere_actor);
	
	
	/*
	 * Get Points and work out vectors 
	 */
	

	Eigen::Vector3f cam_point = cam_positions.at(55);
	double cam_pos[3];
	cam_pos[0] = cam_point(0);
	cam_pos[1] = cam_point(1);
	cam_pos[2] = cam_point(2);
	std::cout <<" cam_point is ("<< cam_point(0) << ", " << cam_point(1) <<", " << cam_point(2) << ") cam_pos(double) is (" << cam_pos[0] << ", " << cam_pos[1] << ", " << cam_pos[2] << ")" << std::endl;    
	Eigen::Vector3f origin(0.f,0.f,0.f);
	
	//X 
//	Eigen::Vector3f fwd = origin - cam_point;
	//For Right Handed Coordinate system
	Eigen::Vector3f fwd = cam_point - origin ;
	double length = fwd.norm();
	std::cout <<" length is " << length << std::endl;
	fwd.normalize();
	
	
	Eigen::Vector3f gravity(0.f, 1.f, 0.f);
	//Z is gravity (0,-1,0) cross X
	Eigen::Vector3f side = gravity.cross(fwd);
	side.normalize();
	
	//Y
//	Eigen::Vector3f up = side.cross(fwd);
	Eigen::Vector3f up = fwd.cross(side);
	up.normalize();
	
	vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	 
	  // Create the direction cosine matrix
	matrix->Identity();
	for (unsigned int i = 0; i < 3; i++) {
//		matrix->SetElement(i, 0, (double) fwd(i));
//		matrix->SetElement(i, 1, (double) up(i));
//		matrix->SetElement(i, 2, (double) side(i));

//		matrix->SetElement(i, 0, (double) side(i));
//		matrix->SetElement(i, 1, (double) fwd(i));
//		matrix->SetElement(i, 2, (double) up(i));
		
		matrix->SetElement(i, 0, (double) side(i));
		matrix->SetElement(i, 1, (double) up(i));
		matrix->SetElement(i, 2, (double) fwd(i));
	}
	
	
	
	vtkSmartPointer<vtkMatrix4x4> inv_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkMatrix4x4::Invert(matrix, inv_matrix);
	
	vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
	transform->Translate(cam_pos);
	transform->Concatenate(matrix);
	
	transform->Scale(length, length, length);

	// Transform the polydata
	vtkSmartPointer<vtkArrowSource> arrowSource = vtkSmartPointer<vtkArrowSource>::New();
	vtkSmartPointer<vtkTransformPolyDataFilter> transformPD = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	transformPD->SetTransform(transform);
	transformPD->SetInputConnection(arrowSource->GetOutputPort());
	
	vtkSmartPointer<vtkPolyDataMapper> arrow_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	vtkSmartPointer<vtkActor> arrow_actor = vtkSmartPointer<vtkActor>::New();
	arrow_mapper->SetInputConnection(transformPD->GetOutputPort());
	arrow_actor->SetMapper(arrow_mapper);
	
	renderer->AddActor(arrow_actor);
	
	
//	double yaw = -RAD2DEG(get_yaw(*matrix));
//	double pitch = -RAD2DEG(get_pitch(*matrix));
//	double roll = -RAD2DEG(get_roll(*matrix));
	
	double yaw = RAD2DEG(get_yaw(*inv_matrix));
	double pitch = RAD2DEG(get_pitch(*inv_matrix));
	double roll = RAD2DEG(get_roll(*inv_matrix));
	

	
	std::cout << "R=[";
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			std::cout << matrix->GetElement(i,j) << " ";
		}
		std::cout << ";" << std::endl;
	}
	std::cout << "];" << std::endl;
	
	std::cout <<" (yaw, pitch, roll) (" << yaw << ", "<< pitch << ", "<< roll << ")" << std::endl; 
	
	std::string mesh = "benchvise_mesh.ply";	
//	visualize_polydata(mesh, matrix, cam_pos);
//	visualize_polydata(mesh, inv_matrix, cam_pos);
	
	std::string benchvise_rgb_out2 = "benchvise_color136_at_-180_0_-148.2825_2rgb.png";
	std::string benchvise_depth_out2 = "benchvise_color136_at_-180_0_-148.2825_2depth.png";
	std::string benchvise_pose_out2 = "benchvise_color136_at_-180_0_-148.2825_2depth.png";

	
	/*
	 * Calculate Translation params
	 * 	xaxis = side
	 * 	yaxis = up
	 * 	zaxis = fwd
	 *  = (-dot(xaxis,cam_pos), -dot(yaxis, cam_pos), -dot(zaxis, cam_pos))
	 */
	
	double x_trans = -(side.dot(cam_point));
	double y_trans = -(up.dot(cam_point));
	double z_trans = -(fwd.dot(cam_point));
	std::cout <<" up is ("<<up(0) << ", "<< up(1) << ", "<< up(2) << ") " << std::endl; 
//	-cam_pos[0], -cam_pos[1], -cam_pos[2]
	generate_image_at_specific_position(mesh, benchvise_rgb_out2, benchvise_depth_out2, benchvise_pose_out2, x_trans, y_trans, z_trans , yaw, pitch, roll, true);
//	
	cv::Mat m = cv::imread(benchvise_rgb_out2, CV_LOAD_IMAGE_ANYCOLOR);
	cv::imshow("r", m);
	cv::waitKey(0);
	
	
	
//	vtkSmartPointer<vtkTransform> inv_transform = vtkSmartPointer<vtkTransform>::New();
//	double _origin[3];
//	_origin[0] = origin(0);
//	_origin[1] = origin(1);
//	_origin[2] = origin(2);
//	
////	inv_transform->Translate(_origin);
////	inv_transform->Concatenate(inv_matrix);
//	inv_transform->Concatenate(matrix);
////	inv_transform->Scale(length, length, length);
//
//	// Transform the polydata
////	vtkSmartPointer<vtkArrowSource> inv_arrowSource = vtkSmartPointer<vtkArrowSource>::New();
//	vtkSmartPointer<vtkTransformPolyDataFilter> inv_transformPD = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
//	inv_transformPD->SetTransform(inv_transform);
//	inv_transformPD->SetInputConnection(translation_filter->GetOutputPort());
//
//	vtkSmartPointer<vtkPolyDataMapper> inv_arrow_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
//	vtkSmartPointer<vtkActor> inv_arrow_actor = vtkSmartPointer<vtkActor>::New();
//	inv_arrow_mapper->SetInputConnection(inv_transformPD->GetOutputPort());
//	inv_arrow_actor->SetMapper(inv_arrow_mapper);

//	renderer->AddActor(inv_arrow_actor);
	
	
	
	
	render_win->Render();
	rwi->Start();
}







int main3(int argc, char **argv) {
	std::vector<float> scales;
//	scales.push_back(650.0f);
//	scales.push_back(750.0f);
//	scales.push_back(850.0f);
//	scales.push_back(950.0f);
//	scales.push_back(1050.0f);
	scales.push_back(1150.0f);

	std::vector<double> in_plane_rotations;
//	in_plane_rotations.push_back(-45.0);
//	in_plane_rotations.push_back(-30.0);
//	in_plane_rotations.push_back(-15.0);
	in_plane_rotations.push_back(0.0);
//	in_plane_rotations.push_back(15.0);
//	in_plane_rotations.push_back(30.0);
//	in_plane_rotations.push_back(45.0);
	
	std::vector<std::string> out_names;
	std::vector<std::string> mesh_names;
	//	mesh_names.push_back("ape_mesh.ply");
	//	out_names.push_back("output/ape/ape_");

	mesh_names.push_back("benchvise_mesh.ply");
	out_names.push_back("output/benchvise/benchvise_");

	
	std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > poses = get_poses(mesh_names.at(0), true, 2, 1150);
	for(std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> >::iterator it = poses.begin(); it != poses.end(); it++){
		(*it)(0,3)/= 1150;
		(*it)(1,3)/= 1150;
		(*it)(2,3)/= 1150;
	}
	
	for (int i = 0; i < (int) mesh_names.size(); i++) {
		cout << " doing " << mesh_names.at(i) << endl;

		for (std::vector<float>::iterator it = scales.begin(); it != scales.end(); it++) {
			cout << " Rendereing for scale " << *it << endl;
			
			std::stringstream prefix;
			prefix << out_names.at(i) << *it;
			cout << "writing to " << prefix.str() << endl;

			for (std::vector<double>::iterator in_plane_rotation = in_plane_rotations.begin(); in_plane_rotation != in_plane_rotations.end(); in_plane_rotation++) {
				cout << "rendering for yaw rotation " << *in_plane_rotation << endl;
				std::string image_prefix = prefix.str();
				render_views_pcl(mesh_names.at(i), image_prefix, poses, *it, *in_plane_rotation);
			}
		}
	}
	
}




int main2(int arc, char *argv[]) {


//	std::string rgb_out = "larger_scale_ape_color60_gen.png";
//	std::string depth_out = "larger_scale_ape_color60_depth_gen.png";
//	std::string ape_mesh = "ape_mesh.ply";

	std::string benchvise_mesh = "benchvise_mesh.ply";

//	std::string rgb_out = "2ape_color100_gen.png";
//	std::string depth_out = "2ape_color100_depth_gen.png";

//	
//	RenderViews rv_demo;
//	//kinect values for near and far?
//	rv_demo.set_z_near(200);
//	rv_demo.set_z_far(10000000);
//
//	rv_demo.setTesselationLevel(2);
//	rv_demo.setUseUpperHemisphereOnly(true);
//	rv_demo.setUseVertices(true);
//	rv_demo.set_save_images_and_poses(true);
//
//	vtkSmartPointer<vtkPLYReader> fileReader_demo = vtkSmartPointer<vtkPLYReader>::New();
//	fileReader_demo->SetFileName(ape_mesh.c_str());
//	fileReader_demo->Update();
//	//No transformation needed
//	vtkSmartPointer<vtkPolyData> poly;
//	poly = fileReader_demo->GetOutput();
//
//	rv_demo.setModelFromPolyData(poly);

//	rv_demo.do_at_specific(-94.8856, 83.1328, 919.84, -105.7658, -61.1411, 20.4953, rgb_out, depth_out);

//		rv_demo.do_at_specific(85.6372, -27.5112, 1080.58, -45.2006, -39.7538 , -29.0146 , rgb_out, depth_out);

//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 466.909, -130.49, 238.723, -45, 59.7714, 151.338, true);

//Seeemed to be a 56 offset in depth. The real value was closer as the center is under the 

	std::string benchvise_rgb_out = "benchvise_color136_at_correct_pose_rgb.png";
	std::string benchvise_depth_out = "benchvise_color136_at_correct_pose_depth.png";
//	1.17692 
//	17.5759 
//	105.834 
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out, benchvise_depth_out, 0, 0, 1058.34, 0, 45, -90, true);

//	exit(-1);
//	std::string benchvise_rgb_out2 = "benchvise_color136_at_-180_0_-148.2825_rgb.png";
//	std::string benchvise_depth_out2 = "benchvise_color136_at_-180_0_-148.2825_depth.png";
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out2, benchvise_depth_out2, 0, 0,1031.56, 144,-7.0612,  19.5722, true);
//	
//	std::string benchvise_rgb_out3 = "benchvise_color136_at_-180_0_-148.2825_rgb3.png";
//	std::string benchvise_depth_out3 = "benchvise_color136_at_-180_0_-148.2825_depth3.png";
//////	
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out, benchvise_depth_out, -139.753, 61.3342 ,1018.69 , 0,0,-90, true);
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out, benchvise_depth_out, -139.753, 61.3342 ,1018.69 , 0,0,0, true);
////	
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out2, benchvise_depth_out2, -139.753, 61.3342 ,1018.69 ,90 + 30 -5.1992, 45.6255, -12.3216, true);
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out3, benchvise_depth_out3, -139.753, 61.3342 ,1018.69 ,111.6463, 47.2650, -11.0537, true);
////////	
//	cv::Mat r1 = cv::imread(benchvise_rgb_out, CV_LOAD_IMAGE_ANYCOLOR);
//	cv::Mat r2 = cv::imread(benchvise_rgb_out2, CV_LOAD_IMAGE_ANYCOLOR);
////	cv::Mat r3 = cv::imread(benchvise_rgb_out3, CV_LOAD_IMAGE_ANYCOLOR);
////	
//	cv::imshow("r1", r1);
//	cv::imshow("r2", r2);
////	cv::imshow("r3", r3);
//	cv::waitKey(0);
//	exit(-1);
//	
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out, benchvise_depth_out, -139.753, 61.3342 ,1018.69 - 57.0 , 111.6463, 47.2650, -11.0537, true);
//	generate_image_at_specific_position(benchvise_mesh, benchvise_rgb_out, benchvise_depth_out, -139.753, 61.3342 ,1018.69 - 57.0 , 115.6463, 45.2650, -15.0537, true);

//	exit(-1);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, 0,1000, 0,48.1897, 7, true);

//	-394.298
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, 394.298, 650.988, 0, 0 , 148.2826, true);
//	x: -0 y: z:  yaw -45 pitch -0 roll 148.283

//	 view to generate is at  x: -0 y: -394.298 z: -637.988 yaw -45 pitch -0 roll 31.7175
//	 view to generate is at  x: 394.298 y: -637.988 z: -0 yaw -45 pitch 31.7175 roll 90
//	 view to generate is at  x: 466.909 y: -130.49 z: 238.723 yaw -45 pitch 59.7714 roll 151.338

//	-17.1289 
//	-50.8389 
//	746.916 

//	-94.8856, 83.1328, 919.84
//	yaw is 0 pitch: -43.097  roll: -101.793
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, 0, 750, -45, -43.097, -101.793, true);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, 0, 750, -45, -39.7538, -29.0146, true);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, -713.252, 201.351, 115.022, 0, -71.9899, -119.737, true);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0,0,750, 0, -71.9899, -119.737, true);

//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, 0, 746.916, -6.9385,-4.5482, -55.7246, true);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, -642.164,-387.46, -6.9385,-4.5482, 58.7246, true);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, -17.1289, -50.8389, 746.916, -6.9385,-4.5482, -55.7246, true);
//	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, 0, 750, 90,0,0, true);
////////	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 85.6372, -27.5112, 1080.0, -45.2006, -39.7538 , -29.0146 );
//////	
////////	generate_image_at_specific_position(ape_mesh, rgb_out, depth_out, 0, -283.908, 459.372, -180, -31.7175, 180);
////////	

//	cv::Mat synthetic_depth = cv::imread(benchvise_depth_out, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
//	cv::Mat synthetic_rgb = cv::imread(benchvise_rgb_out, CV_LOAD_IMAGE_ANYCOLOR);
//
//	double depth = 0;
//	double count = 0;
//	for(int i = 0; i < synthetic_depth.rows; i++){
//		for(int j =0; j < synthetic_depth.cols; j++){
//			if(synthetic_depth.at<int16_t>(i, j) != 0){
//				depth+= synthetic_depth.at<int16_t>(i, j);
//				count++;
//			}
//		}
//	}
//	cv::Mat new_rgb = cv::Mat(synthetic_depth.rows, synthetic_depth.cols, CV_8UC3, cv::Scalar(255,255,255));
//	cv::Mat new_depth = cv::Mat(synthetic_depth.rows, synthetic_depth.cols, CV_16UC1, cv::Scalar(0));
//	for(int y =0; y < synthetic_depth.rows; y++){
//		for(int x = 0; x < synthetic_depth.cols; x++){
//			if(synthetic_depth.at<int16_t>(y,x) != 0){
//				int16_t dpt = synthetic_depth.at<int16_t>(y,x);
//				cv::Vec3b v3b = synthetic_rgb.at<cv::Vec3b>(y,x);
//				
//				int new_x = x - 53 - 5;
//				int new_y = y + 12 - 3;
//				new_rgb.at<cv::Vec3b>(new_y, new_x) = v3b;
//				new_depth.at<int16_t>(new_y, new_x) = dpt;
//			}
//		}
//	}
//	
//	std::cout <<" average depth is "<< depth/count << endl;
//	
//	cv::imwrite(benchvise_rgb_out, new_rgb);
//	cv::imwrite(benchvise_depth_out, new_depth);
//	
//	std::string tmpdepth_img_path = "/home/aly/workspace/linemod_opencv_src/depth136.dpt";
//	cv::Mat real_depth = load_accv12_depth(tmpdepth_img_path);
//	cv::Mat real_rgb = cv::imread("/home/aly/workspace/linemod_opencv_src/color136.jpg", CV_LOAD_IMAGE_ANYCOLOR);
//
////	cv::imshow("r", synthetic_rgb);
////	cv::imshow("r2", real_rgb);
////	cv::waitKey(0);
//	
//	overlay_synthetic_imgae(new_depth, new_rgb, real_depth, real_rgb);
//	exit(-1);	
//	

	std::vector<float> scales;
//	scales.push_back(650.0f);
//	scales.push_back(750.0f);
//	scales.push_back(850.0f);
//	scales.push_back(950.0f);
//	scales.push_back(1050.0f);
	scales.push_back(1150.0f);

	std::vector<double> in_plane_rotations;
	in_plane_rotations.push_back(-45.0);
	in_plane_rotations.push_back(-30.0);
	in_plane_rotations.push_back(-15.0);
	in_plane_rotations.push_back(0.0);
	in_plane_rotations.push_back(15.0);
	in_plane_rotations.push_back(30.0);
	in_plane_rotations.push_back(45.0);

//	/****************
//	 * 
//	 * http://stackoverflow.com/questions/12007775/to-calculate-world-coordinates-from-screen-coordinates-with-opencv  
//	 * FOR 3D - 2D mapping using intrinsics (available in Fanelli's dataset)
//	 * 
//	 * MUST ALSO DO FOR IN-PLANE ROATATIONS ----------------------------------------------------- DO NEXT
//	 * THIS CORRESPONDS TO AGAIN FINDING ROTATION FROM THE CANONICAL POSITION
//	 * (0,0,r) TO THE NEW CAMERA POSITION (x,y,z)
//	 * HOWEVER, WE MUST ALSO ACCOUNT FOR THE IN-PLANE ROTATION
//	 * SO, IF WE THINK ABOUT IT THE OBJECT IS *FIRST* ROTATED JUST
//	 * AROUND THE Z-AXIS (IN-PLANE) FOR WHICH WE CAN DEFINE THE ROTATION MATRIX
//	 * THEN THE OBJECT IS ROTATED BY THE *NEGATIVE* OF THE ROTATION MATRIX OF THE CAMERA
//	 * SO WE CAN MULTIPLY THESE TOGETHER IN THIS ORDER TO GET FINAL ROTATION OF OBJECT.
//	 * 
//	 * REMEMBER: WE MUST NEGATE THE CAMERA ROTATION THAT IS GIVEN AS IT CURRENTLY IS POSE OF
//	 * THE CAMERA, WHICH IS OPPOSITE TO THAT OF OBJECT
//	 ******************/
	RenderViews rv;

//	rv.set_z_near(0.0001);

	rv.setTesselationLevel(2);
//	rv.setTesselationLevel(3);

//Actually want lower hemisphere as y coors are <0 (in camera -ve Y means upwards, so lower hemisphere is the upper one in the camera space)
//	rv.setUseUpperHemisphereOnly(false);
	rv.setUseUpperHemisphereOnly(true);

	rv.setUseVertices(true);
	rv.set_save_images_and_poses(true);

	std::vector<std::string> out_names;
	std::vector<std::string> mesh_names;
//	mesh_names.push_back("ape_mesh.ply");
//	out_names.push_back("output/ape/ape_");

	mesh_names.push_back("benchvise_mesh.ply");
	out_names.push_back("output/benchvise/benchvise_");

	for (int i = 0; i < (int) mesh_names.size(); i++) {
		cout << " doing " << mesh_names.at(i) << endl;

		for (std::vector<float>::iterator it = scales.begin(); it != scales.end(); it++) {
			cout << " Rendereing for scale " << *it << endl;

			std::stringstream prefix;
			prefix << out_names.at(i) << *it;
			cout << "writing to " << prefix.str() << endl;
			rv.set_image_prefix(prefix.str());
			rv.setRadiusSphere(*it);

			for (std::vector<double>::iterator in_plane_rotation = in_plane_rotations.begin(); in_plane_rotation != in_plane_rotations.end(); in_plane_rotation++) {
				cout << "rendering for yaw rotation " << *in_plane_rotation << endl;
				rv.generate_around_icosahedron_hemisphere_euler_angles(mesh_names.at(i), *in_plane_rotation);
//				rv.generate_around_icosahedron_hemisphere_camera_matrix(mesh_names.at(i), *in_plane_rotation);
//				rv.generate_around_icosahedron_hemisphere(mesh_names.at(i), *in_plane_rotation);
//				rv.generate_all_views(*in_plane_rotation);
			}
		}
	}

}

