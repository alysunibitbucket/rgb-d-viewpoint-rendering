/*
 * render_views_tesselated_sphere.h
 *
 *  Created on: Dec 23, 2011
 *      Author: aitor
 */

#ifndef RENDER_VIEWS_H_
#define RENDER_VIEWS_H_

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkMatrix4x4.h>
#include <pcl/common/common.h>
#include <boost/function.hpp>
#include "utils.hpp"
#include <pcl/point_types.h>

#include <vtkCellData.h>
#include <vtkWorldPointPicker.h>
#include <vtkPropPicker.h>
#include <vtkPlatonicSolidSource.h>
#include <vtkLoopSubdivisionFilter.h>
#include <vtkTriangle.h>
#include <vtkTransform.h>
#if VTK_MAJOR_VERSION==6 || (VTK_MAJOR_VERSION==5 && VTK_MINOR_VERSION>4)
#include <vtkHardwareSelector.h>
#include <vtkSelectionNode.h>
#else 
#include <vtkVisibleCellSelector.h>
#endif
#include <vtkSelection.h>
#include <vtkCellArray.h>
#include <vtkTransformFilter.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointPicker.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransformPolyDataFilter.h>
#include <opencv2/opencv.hpp>
#include <pcl/range_image/range_image.h>
#include "utils.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include <vtkDoubleArray.h>

#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkWorldPointPicker.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>
#include <vtkPLYReader.h>

/** \brief @b Class to render synthetic views of a 3D mesh using a tesselated sphere
 * NOTE: This class should replace renderViewTesselatedSphere from pcl::visualization.
 * Some extensions are planned in the near future to this class like removal of duplicated views for
 * symmetrical objects, generation of RGB synthetic clouds when RGB available on mesh, etc.
 * \author Aitor Aldoma
 * \ingroup apps
 */
class PCL_EXPORTS RenderViews {
private:

	int cols_;
	int rows_;
	int tesselation_level_;
	float view_angle_;
	bool use_vertices_;
	float radius_sphere_;
	vtkSmartPointer<vtkPolyData> polydata_;
	bool use_upper_hemisphere_only_;
	bool save_images_and_poses;
	std::string image_prefix;
	double z_near;
	double z_far;
public:
//	RenderViews(int cols = 640, int rows = 480, double z_near= 200, double z_far = 100000, int tesselation_level = 1, float view_angle = 43, bool use_vertices = false,
//			double radius_sphere = 1.0f, bool use_upper_hemisphere_only = true,
//			bool save_images = false, std::string image_prefix = std::string("tmp")) 
	RenderViews(int cols = 640, int rows = 480, double z_near= 200, double z_far = 100000, int tesselation_level = 1, float view_angle = 45.4120, bool use_vertices = false,
				double radius_sphere = 1.0f, bool use_upper_hemisphere_only = true,
				bool save_images = false, std::string image_prefix = std::string("tmp")) 


	: cols_(cols), rows_(rows), tesselation_level_(tesselation_level), view_angle_(view_angle), use_vertices_(use_vertices), 
	  radius_sphere_(radius_sphere), use_upper_hemisphere_only_(use_upper_hemisphere_only),
	  save_images_and_poses(save_images), image_prefix(image_prefix), z_near(z_near), z_far(z_far)
{
		/* Viewing angles given from microsoft for the Kinect: http://msdn.microsoft.com/en-us/library/jj131033.aspx
		 * 43 degrees vertical
		 * 57 degrees horizontal
		*/
		//end kinect setup
		
		/*********************************
		 * Transform object CoM to the origin
		 **********************************/
}

	void set_z_near(double n){
		z_near = n;
	}
	
	void set_z_far(double f){
		z_far = f;
	}
	
	vtkSmartPointer<vtkMatrix4x4> generate_specific_view(double x, double y, double z, double yaw, double pitch, double roll, 
			std::string& rgb_out, std::string& depth_out,std::string& pose_out, bool center_object_in_output_image = false);
	
	vtkSmartPointer<vtkMatrix4x4> generate_specific_view(double x, double y, double z, double yaw, double pitch, double roll, 
			vtkSmartPointer<vtkRenderWindow>& render_win,
			vtkSmartPointer<vtkCamera>& cam,
			vtkSmartPointer<vtkPolyDataMapper>& mapper,
			vtkSmartPointer<vtkWindowToImageFilter>& windowToImageFilter,
			std::string& rgb_out, std::string& depth_out, bool center_object_in_output_image);
	
	void set_save_images_and_poses(bool b){
		save_images_and_poses = b;
	}
	
	bool should_save_images_and_poses(){
		return save_images_and_poses;
	}
	
	void set_image_prefix(std::string p){
		image_prefix = p;
	}
	
	std::string get_image_prefix(){
		return image_prefix;
	}
	
	void generate_all_views(double additional_yaw = 0.0);
	
	
	void generate_around_icosahedron_hemisphere(std::string& mesh_path, double additional_yaw = 0.0);
	
	void setUseUpperHemisphereOnly(bool b){
		use_upper_hemisphere_only_ = b;
	}
	
	bool shouldUseUpperHemispherOnly(){
		return use_upper_hemisphere_only_;
	}
	

	/* \brief Wether to use the vertices or triangle centers of the tesselated sphere
	 * \param use true indicates to use vertices, false triangle centers
	 */

	void setUseVertices(bool use) {
		use_vertices_ = use;
	}

	/* \brief Radius of the sphere where the virtual camera will be placed
	 * \param use true indicates to use vertices, false triangle centers
	 */
	void setRadiusSphere(float radius) {
		radius_sphere_ = radius;
	}

	/* \brief How many times the icosahedron should be tesselated. Results in more or less camera positions and generated views.
	 * \param level amount of tesselation
	 */
	void setTesselationLevel(int level) {
		tesselation_level_ = level;
	}

	/* \brief Sets the view angle of the virtual camera
	 * \param angle view angle in degrees
	 */
	void setViewAngle(float angle) {
		view_angle_ = angle;
	}

	/* \brief adds the mesh to be used as a vtkPolyData
	 * \param polydata vtkPolyData object
	 */
	void setModelFromPolyData(vtkSmartPointer<vtkPolyData> &polydata) {
		polydata_ = polydata;
	}

	vtkSmartPointer<vtkPolyData> get_tesselated_sphere();
	std::vector<Eigen::Vector3f> get_cam_positions(vtkSmartPointer<vtkPolyData> sphere);
	void get_centre_of_mass(double* out);
	
	void generate_pcl_method();
	std::vector<std::pair<Eigen::Vector3f, vtkSmartPointer<vtkMatrix4x4> > > generate_around_icosahedron_hemisphere_camera_matrix(std::string& mesh_path, double additional_yaw);
	std::pair<Eigen::Vector3f, Eigen::Vector3f> get_3d_position(Eigen::Vector3f& pos, double additional_yaw);
	void generate_around_icosahedron_hemisphere_euler_angles(std::string& mesh_path, double additional_yaw);
};

#endif /* RENDER_VIEWS_TESSELATED_SPHERE_H_ */
