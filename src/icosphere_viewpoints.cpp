#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include <boost/shared_ptr.hpp>
#ifdef _WIN32
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
#endif

#include "render_views.hpp"
#include <vtk/vtkPolyDataMapper.h>
#include <vtkPLYReader.h>
#include <vtkPLYWriter.h>
#include <vtk/vtkSmartPointer.h>
#include <vtkSelection.h>
#include <vtkCellArray.h>
#include <vtkTransformFilter.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointPicker.h>
#include <vtkCleanPolyData.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkCubeSource.h>
#include <opencv2/opencv.hpp>
#include <vtkExtractEdges.h>
#include <vtkArrowSource.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkPlaneSource.h>
#include <vtkPlane.h>

vtkSmartPointer<vtkMatrix4x4> get_rotation_around_x(double degrees) {
	double theta = DEG2RAD(degrees);
	vtkSmartPointer<vtkMatrix4x4> rot_mat = vtkSmartPointer<vtkMatrix4x4>::New();
	rot_mat->SetElement(1, 1, cos(theta));
	rot_mat->SetElement(1, 2, -sin(theta));
	rot_mat->SetElement(2, 1, sin(theta));
	rot_mat->SetElement(2, 2, cos(theta));

	return rot_mat;
}

vtkSmartPointer<vtkMatrix4x4> get_rotation_around_y(double degrees) {
	double theta = DEG2RAD(degrees);
	vtkSmartPointer<vtkMatrix4x4> rot_mat = vtkSmartPointer<vtkMatrix4x4>::New();

	rot_mat->SetElement(0, 0, cos(theta));
	rot_mat->SetElement(0, 2, sin(theta));
	rot_mat->SetElement(2, 0, -sin(theta));
	rot_mat->SetElement(2, 2, cos(theta));

	return rot_mat;
}

vtkSmartPointer<vtkMatrix4x4> get_rotation_around_z(double degrees) {
	double theta = DEG2RAD(degrees);
	vtkSmartPointer<vtkMatrix4x4> rot_mat = vtkSmartPointer<vtkMatrix4x4>::New();

	rot_mat->SetElement(0, 0, cos(theta));
	rot_mat->SetElement(0, 1, -sin(theta));
	rot_mat->SetElement(1, 0, sin(theta));
	rot_mat->SetElement(1, 1, cos(theta));
	rot_mat->SetElement(2, 2, 1);

	return rot_mat;
}

void print_matrix_to_console(char* id, vtkSmartPointer<vtkMatrix4x4>& matrix) {
	std::cout << id << "=[" << std::endl;
	for (int i = 0; i < 4; i++) {
		std::cout << "[";
		for (int j = 0; j < 4; j++) {
			std::cout << matrix->GetElement(i, j) << " ";
		}
		std::cout << "];" << std::endl;
	}
	std::cout << "];" << std::endl;
}

void print_matrix_to_console(char* id, Eigen::Matrix3f& matrix) {
	std::cout << id << "=[" << std::endl;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			std::cout << matrix(i, j) << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "];" << std::endl;
}

vtkSmartPointer<vtkPolyData> get_polydata(std::string& mesh_name) {
	vtkSmartPointer<vtkPLYReader> fileReader = vtkSmartPointer<vtkPLYReader>::New();

	fileReader->SetFileName(mesh_name.c_str());
	fileReader->Update();

	vtkSmartPointer<vtkPolyData> polydata_ = fileReader->GetOutput();
	return polydata_;
}

vtkSmartPointer<vtkActor> get_ply_actor_at_origin(std::string& mesh_name) {
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	actor->SetMapper(mapper);
	actor->Modified();

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh_name);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_filter->SetTransform(translation);
	translation_filter->SetInput(polydata_);
	translation_filter->Update();

	mapper->SetInputConnection(translation_filter->GetOutputPort());
	mapper->Update();

	return actor;
}

vtkSmartPointer<vtkActor> get_icosphere_edges_actor(int index_to_highlight, float scale, std::vector<Eigen::Vector3f>& cam_positions) {

	vtkSmartPointer<vtkPlatonicSolidSource> ico = vtkSmartPointer<vtkPlatonicSolidSource>::New();
	ico->SetSolidTypeToIcosahedron();
	ico->Update();

	//tesselate cells from icosahedron
	vtkSmartPointer<vtkLoopSubdivisionFilter> subdivide = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
	subdivide->SetNumberOfSubdivisions(2);
	subdivide->SetInputConnection(ico->GetOutputPort());

	vtkSmartPointer<vtkPolyData> icosphere = subdivide->GetOutput();

	icosphere->Update();

	double com[3];
	calculate_center_of_mass(icosphere, com);

	vtkSmartPointer<vtkTransform> icosphere_transformaiton = vtkSmartPointer<vtkTransform>::New();
	//First to CoM
	icosphere_transformaiton->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> ico_transformation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	ico_transformation_filter->SetTransform(icosphere_transformaiton);
	ico_transformation_filter->SetInput(icosphere);
	ico_transformation_filter->Update();

	vtkSmartPointer<vtkExtractEdges> extractEdges = vtkSmartPointer<vtkExtractEdges>::New();
	extractEdges->SetInputConnection(ico_transformation_filter->GetOutputPort());
	extractEdges->Update();

	unsigned char red[3] = { 255, 0, 0 };
	unsigned char white[3] = { 255, 255, 255 };
	unsigned char grey[3] = { 211, 211, 211 };
	vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
	colors->SetNumberOfComponents(3);
	colors->SetName("Colors");

	int num_points = extractEdges->GetOutput()->GetNumberOfPoints();

	int index = 0;

	for (int i = 0; i < num_points; i++) {

		Eigen::Vector3f cam_pos(extractEdges->GetOutput()->GetPoint(i)[0], extractEdges->GetOutput()->GetPoint(i)[1], extractEdges->GetOutput()->GetPoint(i)[2]);

		cam_pos.normalize();
		cam_pos *= scale;

		if (cam_pos(1) >= 0) {
			cam_positions.push_back(cam_pos);
			if (index == index_to_highlight) {
				colors->InsertNextTupleValue(red);
			} else {
				colors->InsertNextTupleValue(grey);
			}
			index++;
		} else {
			colors->InsertNextTupleValue(white);
		}
	}
	extractEdges->GetOutput()->GetPointData()->SetScalars(colors);

	vtkSmartPointer<vtkTransform> icosphere_scale = vtkSmartPointer<vtkTransform>::New();
	icosphere_scale->Scale(scale, scale, scale);

	vtkSmartPointer<vtkTransformFilter> ico_scale_filter = vtkSmartPointer<vtkTransformFilter>::New();
	ico_scale_filter->SetTransform(icosphere_scale);
	ico_scale_filter->SetInputConnection(extractEdges->GetOutputPort());
	ico_scale_filter->Update();

	// Create a mapper
	vtkSmartPointer<vtkPolyDataMapper> sphere_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	sphere_mapper->SetInputConnection(ico_scale_filter->GetOutputPort());
	sphere_mapper->Update();

	// Create an actor
	vtkSmartPointer<vtkActor> sphere_actor = vtkSmartPointer<vtkActor>::New();
	sphere_actor->SetMapper(sphere_mapper);
	sphere_actor->Modified();

	return sphere_actor;
}

vtkSmartPointer<vtkMatrix4x4> get_camera_matrix(Eigen::Vector3f eye, Eigen::Vector3f up, Eigen::Vector3f look_at) {
	/*
	 * Get Points and work out vectors 
	 */

	//For left handed coordinate system Eigen::Vector3f z_axis = look_at - cam_point;
	//For Right Handed Coordinate system: (which vkt/openGL uses)
	Eigen::Vector3f z_axis = eye - look_at;
	z_axis.normalize();

	//i.e. they are parallel
	if (up.dot(z_axis) == 1) {
		up = Eigen::Vector3f(1, 0, 0);
		//TODO: should we make this (0 0 1) instead as if camera is facing down the y then the 
	}
	Eigen::Vector3f x_axis = up.cross(z_axis);
	x_axis.normalize();

	Eigen::Vector3f y_axis = z_axis.cross(x_axis);
	//note below we should write y_axis.normalize but actually y_axis is unit vector as it is the cross product of two orthogonal vectors as x_axis is orthogonal to z_axis and both are unit
	up.normalize();

	vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();

	// Create the direction cosine matrix
	matrix->Identity();
	for (unsigned int i = 0; i < 3; i++) {
		matrix->SetElement(i, 0, (double) x_axis(i));
		matrix->SetElement(i, 1, (double) y_axis(i));
		matrix->SetElement(i, 2, (double) z_axis(i));
	}

	double x_trans = -(x_axis.dot(eye));
	double y_trans = -(y_axis.dot(eye));
	double z_trans = -(z_axis.dot(eye));

	matrix->SetElement(3, 0, x_trans);
	matrix->SetElement(3, 1, y_trans);
	matrix->SetElement(3, 2, z_trans);

	return matrix;
}

vtkSmartPointer<vtkActor> get_actor_of_poly_under_pose(vtkSmartPointer<vtkPolyData>& poly, vtkSmartPointer<vtkMatrix4x4>& matrix) {
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkTransform> matrix_transform = vtkSmartPointer<vtkTransform>::New();
	matrix_transform->Concatenate(matrix);

	vtkSmartPointer<vtkTransformFilter> matrix_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	matrix_transform_filter->SetTransform(matrix_transform);
	matrix_transform_filter->SetInput(poly);
	matrix_transform_filter->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(matrix_transform_filter->GetOutputPort());
	mapper->Update();

	actor->SetMapper(mapper);
	actor->Modified();

	return actor;
}



vtkSmartPointer<vtkActor> get_actor_of_poly_under_pose_our_dataset(std::string& mesh, vtkSmartPointer<vtkMatrix4x4>& matrix, vtkSmartPointer<vtkTransform>& previous_transform) {
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh);
	
	vtkSmartPointer<vtkTransformFilter> previous_transformation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	previous_transformation_filter->SetInput(polydata_);
	previous_transformation_filter->SetTransform(previous_transform);
	previous_transformation_filter->Update();	
	
	double com[3];
	vtkSmartPointer<vtkPolyData> updated_poly = previous_transformation_filter->GetPolyDataOutput();
	calculate_center_of_mass(updated_poly, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(updated_poly);
	translation_to_origin_filter->Update();

	vtkSmartPointer<vtkTransform> matrix_transform = vtkSmartPointer<vtkTransform>::New();
	matrix_transform->Concatenate(matrix);

	vtkSmartPointer<vtkTransformFilter> matrix_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	matrix_transform_filter->SetTransform(matrix_transform);
	matrix_transform_filter->SetInputConnection(translation_to_origin_filter->GetOutputPort());
	matrix_transform_filter->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(matrix_transform_filter->GetOutputPort());
	mapper->Update();

	actor->SetMapper(mapper);
	actor->Modified();

//	vtkSmartPointer<vtkPolyData> polydata_tmp = matrix_transform_filter->GetPolyDataOutput();
//	calculate_center_of_mass(polydata_tmp, com);
//	std::cout << "com of mesh is now at (" <<com[0] << ", "<< com[1] << ", " << com[2] << ")" << std::endl;
//	
//	double* out = matrix->MultiplyDoublePoint(com); 
//	std::cout <<" after transformation is (" << out[0] << ", " << out[1] << ", "<< out[2] << ")" << std::endl;

	return actor;
}



vtkSmartPointer<vtkActor> get_actor_of_poly_under_pose(std::string& mesh, vtkSmartPointer<vtkMatrix4x4>& matrix) {
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	vtkSmartPointer<vtkTransform> matrix_transform = vtkSmartPointer<vtkTransform>::New();
	matrix_transform->Concatenate(matrix);

	vtkSmartPointer<vtkTransformFilter> matrix_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	matrix_transform_filter->SetTransform(matrix_transform);
	matrix_transform_filter->SetInputConnection(translation_to_origin_filter->GetOutputPort());
	matrix_transform_filter->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(matrix_transform_filter->GetOutputPort());
	mapper->Update();

	actor->SetMapper(mapper);
	actor->Modified();

//	vtkSmartPointer<vtkPolyData> polydata_tmp = matrix_transform_filter->GetPolyDataOutput();
//	calculate_center_of_mass(polydata_tmp, com);
//	std::cout << "com of mesh is now at (" <<com[0] << ", "<< com[1] << ", " << com[2] << ")" << std::endl;
//	
//	double* out = matrix->MultiplyDoublePoint(com); 
//	std::cout <<" after transformation is (" << out[0] << ", " << out[1] << ", "<< out[2] << ")" << std::endl;

	return actor;
}

vtkSmartPointer<vtkCamera> get_camera_positioned_according_to_pose(vtkSmartPointer<vtkMatrix4x4>& matrix, Eigen::Vector3f& cam_view_up, double z_near, double z_far, double view_angle) {
	vtkSmartPointer<vtkCamera> cam = vtkSmartPointer<vtkCamera>::New();
	cam->SetPosition(0, 0, 0);

	//translation element
	cam->SetFocalPoint(matrix->GetElement(0, 3), matrix->GetElement(1, 3), matrix->GetElement(2, 3));
	//y axis of rotation
//	cam->SetViewUp(matrix->GetElement(0, 1), matrix->GetElement(1, 1), matrix->GetElement(2, 1));
	cam->SetViewUp((double) cam_view_up(0), (double) cam_view_up(1), (double) cam_view_up(2));

	cam->SetViewAngle(view_angle);
	cam->SetClippingRange(z_near, z_far);
	cam->Modified();

	return cam;
}

void save_images(vtkSmartPointer<vtkRenderer>& renderer, vtkSmartPointer<vtkRenderWindow>& render_win, vtkSmartPointer<vtkMatrix4x4>& object_pose_transformation_matrix, std::string& rgb_out,
		std::string& depth_out, std::string& pose_out, double z_near, double z_far, double view_angle, int cols_, int rows_) {
	/*
	 * Save images
	 */
	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImageFilter->SetInput(render_win);
	windowToImageFilter->SetMagnification(1);
	windowToImageFilter->SetInputBufferTypeToRGB();

	vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
	writer->SetFileName(rgb_out.c_str());
	writer->SetInputConnection(windowToImageFilter->GetOutputPort());
	writer->Write();

	cv::Mat depth_img = cv::Mat(rows_, cols_, CV_16UC1, cv::Scalar(0));

	float * depth = new float[cols_ * rows_];
	render_win->GetZbufferData(0, 0, cols_ - 1, rows_ - 1, &(depth[0]));

	for (int y = 0; y < depth_img.rows; y++) {
		for (int x = 0; x < depth_img.cols; x++) {
			float z_buff = depth[y * cols_ + x];
			//because we flip the image around x

			if (z_buff != 1.0) {
				//formula from here: https://en.wikipedia.org/wiki/Z-buffering
				double depth = -1 * ((z_far * z_near) / (z_buff * (z_far - z_near) - z_far));
				depth_img.at < int16_t > (y, x) = (int16_t) depth;

			} else {
				depth_img.at < int16_t > (y, x) = 0;
			}
		}
	}
	//must flip image as it's producing one from camera perspective i.e. upside down
	cv::flip(depth_img, depth_img, 0);
	cv::imwrite(depth_out, depth_img);

	ofstream fout;
	fout.open(pose_out.c_str());

	double yaw = RAD2DEG(get_yaw(*object_pose_transformation_matrix));
	double pitch = RAD2DEG(get_pitch(*object_pose_transformation_matrix));
	double roll = RAD2DEG(get_roll(*object_pose_transformation_matrix));

	fout << yaw << ", " << pitch << ", " << roll << endl;

	float fx = 572.4114;
	float fy = 573.5704;
	float cx = 325.2611;
	float cy = 242.0490;
	//Intrinsic matrix
	Eigen::Matrix3f I = Eigen::Matrix3f::Identity();
	I(0, 0) = fx;
	I(0, 2) = cx;
	I(1, 1) = fy;
	I(1, 2) = cy;

	Eigen::Vector4f translation(object_pose_transformation_matrix->GetElement(0, 3), object_pose_transformation_matrix->GetElement(1, 3), object_pose_transformation_matrix->GetElement(2, 3),
			object_pose_transformation_matrix->GetElement(3, 3));
//	translation(2) = std::fabs((float) translation(2));

	renderer->SetWorldPoint((double) translation(0), (double) translation(1), (double) translation(2), (double) translation(3));
	renderer->WorldToDisplay();
	double* disp = renderer->GetDisplayPoint();

//	Eigen::Vector3f image_plane_pos = I * translation;
//	image_plane_pos /= image_plane_pos(2);
//
//	fout << image_plane_pos(0) << ", " << image_plane_pos(1) << ", " << fabs((float)translation(2)) << std::endl;

	fout << disp[0] << ", " << disp[1] << ", " << fabs((float) translation(2)) << std::endl;

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			fout << object_pose_transformation_matrix->GetElement(i, j) << " ";
		}
		fout << ";" << std::endl;
	}
	fout << std::endl;

	fout.flush();
	fout.close();
}

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>

// Define interaction style
class MouseInteractorStyle: public vtkInteractorStyleTrackballCamera {
public:
	static MouseInteractorStyle* New();vtkTypeMacro(MouseInteractorStyle, vtkInteractorStyleTrackballCamera)
	;

	virtual void OnLeftButtonDown() {

		std::cout << "Picking pixel: " << this->Interactor->GetEventPosition()[0] << " " << this->Interactor->GetEventPosition()[1] << std::endl;
		this->Interactor->GetPicker()->Pick(this->Interactor->GetEventPosition()[0], this->Interactor->GetEventPosition()[1], 0,  // always zero.
				this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
		double picked[3];
		this->Interactor->GetPicker()->GetPickPosition(picked);
		std::cout << "Picked value: " << picked[0] << " " << picked[1] << " " << picked[2] << std::endl;
		// Forward events
		vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
	}

};
vtkStandardNewMacro(MouseInteractorStyle);
void render_image_with_point_picker(std::string& mesh, vtkSmartPointer<vtkMatrix4x4>& object_pose_transformation_matrix, Eigen::Vector3f& cam_view_up, std::string& rgb_out, std::string& depth_out,
		std::string& pose_out, vtkSmartPointer<vtkRenderWindow>& render_win, vtkSmartPointer<vtkRenderer>& renderer) {

	int cols_ = 640;
	int rows_ = 480;
	double z_near = 200;
	double z_far = 10000000;
	double view_angle = 45.4120;

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);

	vtkSmartPointer<vtkActor> poly_at_pose_actor = get_actor_of_poly_under_pose(mesh, object_pose_transformation_matrix);
	renderer->AddActor(poly_at_pose_actor);

	renderer->Modified();

	vtkSmartPointer<vtkCamera> cam = get_camera_positioned_according_to_pose(object_pose_transformation_matrix, cam_view_up, z_near, z_far, view_angle);
	renderer->SetActiveCamera(cam);
	renderer->Modified();

	render_win->Render();

	//get points
	vtkSmartPointer<vtkWorldPointPicker> wpp = vtkSmartPointer<vtkWorldPointPicker>::New();
	int status = wpp->Pick(325, 242, 950, renderer);
	std::cout << "status is " << status << std::endl;
//	if(status != 0){
	double pos[3] = { 325, 242, 950 };
	wpp->GetPickPosition(pos);
	double* res = wpp->GetPickPosition();
	std::cout << "res is (" << res[0] << ", " << res[1] << ", " << res[2] << ")" << std::endl;
	double* cen = renderer->GetCenter();
	std::cout << " cen is (" << cen[0] << ", " << cen[1] << ") " << std::endl;

	Eigen::Vector3f translation(object_pose_transformation_matrix->GetElement(0, 3), object_pose_transformation_matrix->GetElement(1, 3), object_pose_transformation_matrix->GetElement(2, 3));
//		translation(2) = std::fabs((float) translation(2));
	std::cout << " translation is (" << translation(0) << ", " << translation(1) << ", " << translation(2) << ", " << object_pose_transformation_matrix->GetElement(3, 3) << ")" << std::endl;
	renderer->SetWorldPoint((double) translation(0), (double) translation(1), (double) translation(2) + 500, 1.0);
	renderer->WorldToDisplay();
	double* disp = renderer->GetDisplayPoint();

	std::cout << "disp is (" << disp[0] << ", " << disp[1] << ", " << disp[2] << ")" << std::endl;

	renderer->SetWorldPoint((double) translation(0) + 5, (double) translation(1) + 10, (double) translation(2), 1.0);
	renderer->WorldToDisplay();
	double* disp2 = renderer->GetDisplayPoint();
	std::cout << "disp is (" << disp2[0] << ", " << disp2[1] << ", " << disp2[2] << ")" << std::endl;
//	}
	exit(-1);
	save_images(renderer, render_win, object_pose_transformation_matrix, rgb_out, depth_out, pose_out, z_near, z_far, view_angle, cols_, rows_);

	renderer->RemoveActor(poly_at_pose_actor);
}




void render_image_our_dataset(std::string& mesh, vtkSmartPointer<vtkMatrix4x4>& object_pose_transformation_matrix, Eigen::Vector3f& cam_view_up, std::string& rgb_out, std::string& depth_out,
		std::string& pose_out, vtkSmartPointer<vtkRenderWindow>& render_win, vtkSmartPointer<vtkRenderer>& renderer, vtkSmartPointer<vtkTransform>& previous_transform) {

	
//vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	//TODO: Pass in the render window, 
	// Remove the actor on every iteration?

	int cols_ = 640;
	int rows_ = 480;
	double z_near = 200;
	double z_far = 10000000;
	double view_angle = 45.4120;

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);

//	rwi->SetRenderWindow(render_win);

	vtkSmartPointer<vtkActor> poly_at_pose_actor = get_actor_of_poly_under_pose_our_dataset(mesh, object_pose_transformation_matrix, previous_transform);
	renderer->AddActor(poly_at_pose_actor);

	renderer->Modified();

	vtkSmartPointer<vtkCamera> cam = get_camera_positioned_according_to_pose(object_pose_transformation_matrix, cam_view_up, z_near, z_far, view_angle);
	renderer->SetActiveCamera(cam);
	renderer->Modified();

	render_win->Render();

//	rwi->Start();

	save_images(renderer, render_win, object_pose_transformation_matrix, rgb_out, depth_out, pose_out, z_near, z_far, view_angle, cols_, rows_);

	renderer->RemoveActor(poly_at_pose_actor);
}






void render_image(std::string& mesh, vtkSmartPointer<vtkMatrix4x4>& object_pose_transformation_matrix, Eigen::Vector3f& cam_view_up, std::string& rgb_out, std::string& depth_out,
		std::string& pose_out, vtkSmartPointer<vtkRenderWindow>& render_win, vtkSmartPointer<vtkRenderer>& renderer) {

	
//vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	//TODO: Pass in the render window, 
	// Remove the actor on every iteration?

	int cols_ = 640;
	int rows_ = 480;
	double z_near = 200;
	double z_far = 10000000;
	double view_angle = 45.4120;

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);

//	rwi->SetRenderWindow(render_win);

	vtkSmartPointer<vtkActor> poly_at_pose_actor = get_actor_of_poly_under_pose(mesh, object_pose_transformation_matrix);
	renderer->AddActor(poly_at_pose_actor);

	renderer->Modified();

	vtkSmartPointer<vtkCamera> cam = get_camera_positioned_according_to_pose(object_pose_transformation_matrix, cam_view_up, z_near, z_far, view_angle);
	renderer->SetActiveCamera(cam);
	renderer->Modified();

	render_win->Render();

//	rwi->Start();

	save_images(renderer, render_win, object_pose_transformation_matrix, rgb_out, depth_out, pose_out, z_near, z_far, view_angle, cols_, rows_);

	renderer->RemoveActor(poly_at_pose_actor);
}

void generate_all_views_our_dataset(std::string& mesh, std::vector<double>& in_plane_rotations, std::vector<double>& scales, std::string& output_prefix, 
		vtkSmartPointer<vtkTransform>& previos_transformation) {
	//Add Icosphere
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	render_win->Start();

	for (std::vector<double>::iterator s = scales.begin(); s != scales.end(); s++) {
		int index_to_extract = 0;
		std::vector<Eigen::Vector3f> cam_positions;
		vtkSmartPointer<vtkActor> icosphere_actor = get_icosphere_edges_actor(index_to_extract, *s, cam_positions);
		std::cout << "******** generated " << cam_positions.size() << " camera positions *************" << std::endl;
		int position_count = 0;
		for (std::vector<Eigen::Vector3f>::iterator position = cam_positions.begin(); position != cam_positions.end(); position++) {

			Eigen::Vector3f eye((*position)(0), (*position)(1), (*position)(2));
			Eigen::Vector3f look_at(0, 0, 0);
			Eigen::Vector3f up(0, 1, 0);

			vtkSmartPointer<vtkMatrix4x4> camera_matrix = get_camera_matrix(eye, up, look_at);

			vtkSmartPointer<vtkMatrix4x4> inv_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
			//inverted matrix is same as transpose for rotation matrix
			vtkMatrix4x4::Transpose(camera_matrix, inv_matrix);

			Eigen::Vector3f cam_view_up(inv_matrix->GetElement(0, 1), inv_matrix->GetElement(1, 1), inv_matrix->GetElement(2, 1));

			vtkSmartPointer<vtkMatrix4x4> object_rotation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
			//Take only the rotation part
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					object_rotation_matrix->SetElement(i, j, inv_matrix->GetElement(i, j));
				}
			}

			for (std::vector<double>::iterator ipr = in_plane_rotations.begin(); ipr != in_plane_rotations.end(); ipr++) {

				vtkSmartPointer<vtkMatrix4x4> in_plane_rotation_matrix = get_rotation_around_z(*ipr);

				vtkSmartPointer<vtkMatrix4x4> combined_object_rotation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
				vtkMatrix4x4::Multiply4x4(in_plane_rotation_matrix, object_rotation_matrix, combined_object_rotation_matrix);

				vtkSmartPointer<vtkMatrix4x4> initial_rotation_around_x = get_rotation_around_x(90);

				vtkSmartPointer<vtkMatrix4x4> final_transformation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
				vtkMatrix4x4::Multiply4x4(combined_object_rotation_matrix, initial_rotation_around_x, final_transformation_matrix);

				//Add back in the translation
				for (int i = 0; i < 4; i++) {
					final_transformation_matrix->SetElement(i, 3, inv_matrix->GetElement(i, 3));
				}

				std::stringstream rgb_out;
				rgb_out << output_prefix << "scale_" << *s << "_ipr_" << *ipr << "_" << position_count << "_rgb.png";

				std::stringstream depth_out;
				depth_out << output_prefix << "scale_" << *s << "_ipr_" << *ipr << "_" << position_count << "_depth.png";

				std::stringstream pose_out;
				pose_out << output_prefix << "scale_" << *s << "_ipr_" << *ipr << "_" << position_count << "_pose.txt";

				std::string rgb_out_file = rgb_out.str();
				std::string depth_out_file = depth_out.str();
				std::string pose_out_file = pose_out.str();

				
				render_image_our_dataset(mesh, final_transformation_matrix, cam_view_up, rgb_out_file, depth_out_file, pose_out_file, render_win, renderer, previos_transformation);

			}
			std::cout << " done position " << position_count << std::endl;
			position_count++;
		}
	}
}



void generate_all_views(std::string& mesh, std::vector<double>& in_plane_rotations, std::vector<double>& scales, std::string& output_prefix) {
	//Add Icosphere
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
	render_win->Start();

	for (std::vector<double>::iterator s = scales.begin(); s != scales.end(); s++) {
		int index_to_extract = 0;
		std::vector<Eigen::Vector3f> cam_positions;
		vtkSmartPointer<vtkActor> icosphere_actor = get_icosphere_edges_actor(index_to_extract, *s, cam_positions);
		std::cout << "******** generated " << cam_positions.size() << " camera positions *************" << std::endl;
		int position_count = 0;
		for (std::vector<Eigen::Vector3f>::iterator position = cam_positions.begin(); position != cam_positions.end(); position++) {

			Eigen::Vector3f eye((*position)(0), (*position)(1), (*position)(2));
			Eigen::Vector3f look_at(0, 0, 0);
			Eigen::Vector3f up(0, 1, 0);

			vtkSmartPointer<vtkMatrix4x4> camera_matrix = get_camera_matrix(eye, up, look_at);

			vtkSmartPointer<vtkMatrix4x4> inv_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
			//inverted matrix is same as transpose for rotation matrix
			vtkMatrix4x4::Transpose(camera_matrix, inv_matrix);

			Eigen::Vector3f cam_view_up(inv_matrix->GetElement(0, 1), inv_matrix->GetElement(1, 1), inv_matrix->GetElement(2, 1));

			vtkSmartPointer<vtkMatrix4x4> object_rotation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
			//Take only the rotation part
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					object_rotation_matrix->SetElement(i, j, inv_matrix->GetElement(i, j));
				}
			}

			for (std::vector<double>::iterator ipr = in_plane_rotations.begin(); ipr != in_plane_rotations.end(); ipr++) {

				vtkSmartPointer<vtkMatrix4x4> in_plane_rotation_matrix = get_rotation_around_z(*ipr);

				vtkSmartPointer<vtkMatrix4x4> combined_object_rotation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
				vtkMatrix4x4::Multiply4x4(in_plane_rotation_matrix, object_rotation_matrix, combined_object_rotation_matrix);

				vtkSmartPointer<vtkMatrix4x4> initial_rotation_around_x = get_rotation_around_x(90);

				vtkSmartPointer<vtkMatrix4x4> final_transformation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
				vtkMatrix4x4::Multiply4x4(combined_object_rotation_matrix, initial_rotation_around_x, final_transformation_matrix);

				//Add back in the translation
				for (int i = 0; i < 4; i++) {
					final_transformation_matrix->SetElement(i, 3, inv_matrix->GetElement(i, 3));
				}

				std::stringstream rgb_out;
				rgb_out << output_prefix << "scale_" << *s << "_ipr_" << *ipr << "_" << position_count << "_rgb.png";

				std::stringstream depth_out;
				depth_out << output_prefix << "scale_" << *s << "_ipr_" << *ipr << "_" << position_count << "_depth.png";

				std::stringstream pose_out;
				pose_out << output_prefix << "scale_" << *s << "_ipr_" << *ipr << "_" << position_count << "_pose.txt";

				std::string rgb_out_file = rgb_out.str();
				std::string depth_out_file = depth_out.str();
				std::string pose_out_file = pose_out.str();

				render_image(mesh, final_transformation_matrix, cam_view_up, rgb_out_file, depth_out_file, pose_out_file, render_win, renderer);

			}
			std::cout << " done position " << position_count << std::endl;
			position_count++;
		}
	}
}

Eigen::Matrix3Xf get_rotation_matrix(double yaw, double pitch, double roll) {
	vtkSmartPointer<vtkMatrix4x4> R_yaw = get_rotation_around_z(yaw);
	vtkSmartPointer<vtkMatrix4x4> R_pitch = get_rotation_around_y(pitch);
	vtkSmartPointer<vtkMatrix4x4> R_roll = get_rotation_around_x(roll);

	vtkSmartPointer<vtkMatrix4x4> R_tmp = vtkSmartPointer<vtkMatrix4x4>::New();

	vtkMatrix4x4::Multiply4x4(R_pitch, R_roll, R_tmp);

	vtkSmartPointer<vtkMatrix4x4> R = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkMatrix4x4::Multiply4x4(R_yaw, R_tmp, R);

	Eigen::Matrix3Xf out = Eigen::Matrix3f::Identity();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			out(i, j) = R->GetElement(i, j);
		}
	}

	return out;
}

double calc_accuracy_no_translation(double estimated_yaw, double estimated_pitch, double estimated_roll, double estimated_x, double estimated_y, double estimated_z, double real_yaw, double real_pitch,
		double real_roll, double real_x, double real_y, double real_z, std::string mesh) {

	/*
	 * The reason I need to flip the axis in the estimated rotation matrix is because
	 * This is given in a right handed coordinate system
	 * the groud truth is in a left handed one, therefore the right handed rotation and translation 
	 * display correctly when rendered with their rotation matrix and translation
	 * 
	 * 
	 * However, in the rendering training images we have y pointint up (0 1 0) and z pointing into the screen
	 * in the ground truth it is the other way y is down (0 -1 0) and z out of the screen
	 * 
	 * Therefore we must flip the y and z axis. The z is already negative so we make it positive, 
	 * the Y axis is given by the pixel coordinate which is already in the new coordinate system
	 * as in pixels y goes down. so we should keep Y the same, but for our rotation we still need 
	 * to flip y as well as z. 
	 */

	Eigen::Matrix3Xf estimated_r = get_rotation_matrix(estimated_yaw, estimated_pitch, estimated_roll);

	for (int i = 0; i < 3; i++) {
		estimated_r(1, i) = -estimated_r(1, i);
		estimated_r(2, i) = -estimated_r(2, i);
	}
	Eigen::Matrix3Xf real_r = get_rotation_matrix(real_yaw, real_pitch, real_roll);

	Eigen::Vector3f estimated_t(estimated_x, estimated_y, estimated_z);
	Eigen::Vector3f real_t(real_x, real_y, real_z);

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	int num_of_points = translation_to_origin_filter->GetOutput()->GetNumberOfPoints();
	double total = 0;
	double points = num_of_points;
	for (int i = 0; i < num_of_points; i++) {
		Eigen::Vector3f point(translation_to_origin_filter->GetOutput()->GetPoint(i)[0], translation_to_origin_filter->GetOutput()->GetPoint(i)[1],
				translation_to_origin_filter->GetOutput()->GetPoint(i)[2]);

		Eigen::Vector3f est_transformation = (estimated_r * point);	// + estimated_t;

		Eigen::Vector3f real_transformation = (real_r * point);	// + real_t;

		est_transformation -= real_transformation;

		total += est_transformation.norm();

	}

	return total / points;
}

double calc_accuracy_from_object_file(double estimated_yaw, double estimated_pitch, double estimated_roll, double estimated_x, double estimated_y, double estimated_z, double real_yaw,
		double real_pitch, double real_roll, double real_x, double real_y, double real_z, std::vector<point3d>& points_in_mm) {
	/*
	 * The reason I need to flip the axis in the estimated rotation matrix is because
	 * This is given in a right handed coordinate system
	 * the groud truth is in a left handed one, therefore the right handed rotation and translation 
	 * display correctly when rendered with their rotation matrix and translation
	 * 
	 * 
	 * However, in the rendering training images we have y pointint up (0 1 0) and z pointing into the screen
	 * in the ground truth it is the other way y is down (0 -1 0) and z out of the screen
	 * 
	 * Therefore we must flip the y and z axis. The z is already negative so we make it positive, 
	 * the Y axis is given by the pixel coordinate which is already in the new coordinate system
	 * as in pixels y goes down. so we should keep Y the same, but for our rotation we still need 
	 * to flip y as well as z. 
	 */

	Eigen::Matrix3Xf estimated_r = get_rotation_matrix(estimated_yaw, estimated_pitch, estimated_roll);

	for (int i = 0; i < 3; i++) {
		estimated_r(1, i) = -estimated_r(1, i);
		estimated_r(2, i) = -estimated_r(2, i);
	}
	Eigen::Matrix3Xf real_r = get_rotation_matrix(real_yaw, real_pitch, real_roll);

	Eigen::Vector3f estimated_t(estimated_x, estimated_y, estimated_z);
	Eigen::Vector3f real_t(real_x, real_y, real_z);

	int num_of_points = points_in_mm.size();

	//std::cout <<" number of points are " <<  num_of_points << std::endl;

	double total = 0;
	double points = num_of_points;
	for (int i = 0; i < num_of_points; i++) {
		point3d p = points_in_mm.at(i);
		Eigen::Vector3f point(p.x, p.y, p.z);

		Eigen::Vector3f est_transformation = (estimated_r * point) + estimated_t;

		Eigen::Vector3f real_transformation = (real_r * point) + real_t;

		est_transformation -= real_transformation;

		total += est_transformation.norm();

	}

	return total / points;
}

std::vector<Eigen::Vector3f> get_tranformed_points(double real_yaw, double real_pitch,
		double real_roll, double real_x, double real_y, double real_z, vtkSmartPointer<vtkPolyData>& polydata_){
	
	Eigen::Matrix3Xf real_r = get_rotation_matrix(real_yaw, real_pitch, real_roll);
	Eigen::Vector3f real_t(real_x, real_y, real_z);
	
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	int num_of_points = translation_to_origin_filter->GetOutput()->GetNumberOfPoints();

	std::vector<Eigen::Vector3f> transformed_points;
	for (int i = 0; i < num_of_points; i++) {
		Eigen::Vector3f point(translation_to_origin_filter->GetOutput()->GetPoint(i)[0], translation_to_origin_filter->GetOutput()->GetPoint(i)[1],
				translation_to_origin_filter->GetOutput()->GetPoint(i)[2]);
		
		Eigen::Vector3f real_transformation = (real_r * point) + real_t;
		transformed_points.push_back(real_transformation);
	}
	
	return transformed_points;

}
double calc_accuracy_symmetric_obj_with_poly(double estimated_yaw,double estimated_pitch,double estimated_roll,double estimated_x, double estimated_y,double estimated_z,
		std::vector<Eigen::Vector3f>& gt_transformed_points, vtkSmartPointer<vtkPolyData>& polydata){
	/*
			 * The reason I need to flip the axis in the estimated rotation matrix is because
			 * This is given in a right handed coordinate system
			 * the groud truth is in a left handed one, therefore the right handed rotation and translation 
			 * display correctly when rendered with their rotation matrix and translation
			 * 
			 * 
			 * However, in the rendering training images we have y pointint up (0 1 0) and z pointing into the screen
			 * in the ground truth it is the other way y is down (0 -1 0) and z out of the screen
			 * 
			 * Therefore we must flip the y and z axis. The z is already negative so we make it positive, 
			 * the Y axis is given by the pixel coordinate which is already in the new coordinate system
			 * as in pixels y goes down. so we should keep Y the same, but for our rotation we still need 
			 * to flip y as well as z. 
			 */

			Eigen::Matrix3Xf estimated_r = get_rotation_matrix(estimated_yaw, estimated_pitch, estimated_roll);

			for (int i = 0; i < 3; i++) {
				estimated_r(1, i) = -estimated_r(1, i);
				estimated_r(2, i) = -estimated_r(2, i);
			}

			Eigen::Vector3f estimated_t(estimated_x, estimated_y, estimated_z);

			double com[3];
			calculate_center_of_mass(polydata, com);

			vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
			translation_to_origin->Translate(-com[0], -com[1], -com[2]);

			vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
			translation_to_origin_filter->SetTransform(translation_to_origin);
			translation_to_origin_filter->SetInput(polydata);
			translation_to_origin_filter->Update();

			int num_of_points = translation_to_origin_filter->GetOutput()->GetNumberOfPoints();

			//std::cout <<" number of points are " <<  num_of_points << std::endl;

			double total = 0;
			double points = num_of_points;
			std::cout << "number of points is "<< num_of_points << std::endl;
			for (int i = 0; i < num_of_points; i++) {
				Eigen::Vector3f point(translation_to_origin_filter->GetOutput()->GetPoint(i)[0], translation_to_origin_filter->GetOutput()->GetPoint(i)[1],
						translation_to_origin_filter->GetOutput()->GetPoint(i)[2]);

				Eigen::Vector3f est_transformation = (estimated_r * point) + estimated_t;
				
				double min_dist = DBL_MAX;
				for(std::vector<Eigen::Vector3f>::iterator x2 = gt_transformed_points.begin(); x2 != gt_transformed_points.end(); x2++){
					Eigen::Vector3f diff = (*x2 - est_transformation);
					double val = diff.norm();
					min_dist = std::min(min_dist, val);
				}
				
				total += min_dist;
			}

			return total / points;
}
double calc_accuracy_symmetric_obj_with_poly(double estimated_yaw, double estimated_pitch, double estimated_roll, double estimated_x, double estimated_y, double estimated_z, double real_yaw, double real_pitch,
		double real_roll, double real_x, double real_y, double real_z, vtkSmartPointer<vtkPolyData>& polydata_){
	
	/*
		 * The reason I need to flip the axis in the estimated rotation matrix is because
		 * This is given in a right handed coordinate system
		 * the groud truth is in a left handed one, therefore the right handed rotation and translation 
		 * display correctly when rendered with their rotation matrix and translation
		 * 
		 * 
		 * However, in the rendering training images we have y pointint up (0 1 0) and z pointing into the screen
		 * in the ground truth it is the other way y is down (0 -1 0) and z out of the screen
		 * 
		 * Therefore we must flip the y and z axis. The z is already negative so we make it positive, 
		 * the Y axis is given by the pixel coordinate which is already in the new coordinate system
		 * as in pixels y goes down. so we should keep Y the same, but for our rotation we still need 
		 * to flip y as well as z. 
		 */

		Eigen::Matrix3Xf estimated_r = get_rotation_matrix(estimated_yaw, estimated_pitch, estimated_roll);

		for (int i = 0; i < 3; i++) {
			estimated_r(1, i) = -estimated_r(1, i);
			estimated_r(2, i) = -estimated_r(2, i);
		}
		Eigen::Matrix3Xf real_r = get_rotation_matrix(real_yaw, real_pitch, real_roll);

		Eigen::Vector3f estimated_t(estimated_x, estimated_y, estimated_z);
		Eigen::Vector3f real_t(real_x, real_y, real_z);

		double com[3];
		calculate_center_of_mass(polydata_, com);

		vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
		translation_to_origin->Translate(-com[0], -com[1], -com[2]);

		vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
		translation_to_origin_filter->SetTransform(translation_to_origin);
		translation_to_origin_filter->SetInput(polydata_);
		translation_to_origin_filter->Update();

		int num_of_points = translation_to_origin_filter->GetOutput()->GetNumberOfPoints();

		//std::cout <<" number of points are " <<  num_of_points << std::endl;

		double total = 0;
		double points = num_of_points;
		std::cout << "number of points is "<< num_of_points << std::endl;
		for (int i = 0; i < num_of_points; i++) {
			Eigen::Vector3f point(translation_to_origin_filter->GetOutput()->GetPoint(i)[0], translation_to_origin_filter->GetOutput()->GetPoint(i)[1],
					translation_to_origin_filter->GetOutput()->GetPoint(i)[2]);

			Eigen::Vector3f est_transformation = (estimated_r * point) + estimated_t;
			double min_dist = DBL_MAX;	
			for (int j = 0; j < num_of_points; j++) {
				Eigen::Vector3f x2(translation_to_origin_filter->GetOutput()->GetPoint(j)[0], translation_to_origin_filter->GetOutput()->GetPoint(j)[1],
									translation_to_origin_filter->GetOutput()->GetPoint(j)[2]);
				
				Eigen::Vector3f real_transformation = (real_r * x2) + real_t;
				real_transformation -= est_transformation;
				double val = real_transformation.norm();
				min_dist = std::min(min_dist, val);
			}
			total += min_dist;
		}

		return total / points;
	
}
double calc_accuracy_with_poly(double estimated_yaw, double estimated_pitch, double estimated_roll, double estimated_x, double estimated_y, double estimated_z, double real_yaw, double real_pitch,
		double real_roll, double real_x, double real_y, double real_z, vtkSmartPointer<vtkPolyData>& polydata_) {

	/*
	 * The reason I need to flip the axis in the estimated rotation matrix is because
	 * This is given in a right handed coordinate system
	 * the groud truth is in a left handed one, therefore the right handed rotation and translation 
	 * display correctly when rendered with their rotation matrix and translation
	 * 
	 * 
	 * However, in the rendering training images we have y pointint up (0 1 0) and z pointing into the screen
	 * in the ground truth it is the other way y is down (0 -1 0) and z out of the screen
	 * 
	 * Therefore we must flip the y and z axis. The z is already negative so we make it positive, 
	 * the Y axis is given by the pixel coordinate which is already in the new coordinate system
	 * as in pixels y goes down. so we should keep Y the same, but for our rotation we still need 
	 * to flip y as well as z. 
	 */

	Eigen::Matrix3Xf estimated_r = get_rotation_matrix(estimated_yaw, estimated_pitch, estimated_roll);

	for (int i = 0; i < 3; i++) {
		estimated_r(1, i) = -estimated_r(1, i);
		estimated_r(2, i) = -estimated_r(2, i);
	}
	Eigen::Matrix3Xf real_r = get_rotation_matrix(real_yaw, real_pitch, real_roll);

	Eigen::Vector3f estimated_t(estimated_x, estimated_y, estimated_z);
	Eigen::Vector3f real_t(real_x, real_y, real_z);

	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	int num_of_points = translation_to_origin_filter->GetOutput()->GetNumberOfPoints();

	//std::cout <<" number of points are " <<  num_of_points << std::endl;

	double total = 0;
	double points = num_of_points;
	for (int i = 0; i < num_of_points; i++) {
		Eigen::Vector3f point(translation_to_origin_filter->GetOutput()->GetPoint(i)[0], translation_to_origin_filter->GetOutput()->GetPoint(i)[1],
				translation_to_origin_filter->GetOutput()->GetPoint(i)[2]);

		Eigen::Vector3f est_transformation = (estimated_r * point) + estimated_t;

		Eigen::Vector3f real_transformation = (real_r * point) + real_t;

		est_transformation -= real_transformation;

		total += est_transformation.norm();

	}

	return total / points;
}

double calc_accuracy(double estimated_yaw, double estimated_pitch, double estimated_roll, double estimated_x, double estimated_y, double estimated_z, double real_yaw, double real_pitch,
		double real_roll, double real_x, double real_y, double real_z, std::string mesh) {

	/*
	 * The reason I need to flip the axis in the estimated rotation matrix is because
	 * This is given in a right handed coordinate system
	 * the groud truth is in a left handed one, therefore the right handed rotation and translation 
	 * display correctly when rendered with their rotation matrix and translation
	 * 
	 * 
	 * However, in the rendering training images we have y pointint up (0 1 0) and z pointing into the screen
	 * in the ground truth it is the other way y is down (0 -1 0) and z out of the screen
	 * 
	 * Therefore we must flip the y and z axis. The z is already negative so we make it positive, 
	 * the Y axis is given by the pixel coordinate which is already in the new coordinate system
	 * as in pixels y goes down. so we should keep Y the same, but for our rotation we still need 
	 * to flip y as well as z. 
	 */

	Eigen::Matrix3Xf estimated_r = get_rotation_matrix(estimated_yaw, estimated_pitch, estimated_roll);

	for (int i = 0; i < 3; i++) {
		estimated_r(1, i) = -estimated_r(1, i);
		estimated_r(2, i) = -estimated_r(2, i);
	}
	Eigen::Matrix3Xf real_r = get_rotation_matrix(real_yaw, real_pitch, real_roll);

	Eigen::Vector3f estimated_t(estimated_x, estimated_y, estimated_z);
	Eigen::Vector3f real_t(real_x, real_y, real_z);

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	int num_of_points = translation_to_origin_filter->GetOutput()->GetNumberOfPoints();

	//std::cout <<" number of points are " <<  num_of_points << std::endl;

	double total = 0;
	double points = num_of_points;
	for (int i = 0; i < num_of_points; i++) {
		Eigen::Vector3f point(translation_to_origin_filter->GetOutput()->GetPoint(i)[0], translation_to_origin_filter->GetOutput()->GetPoint(i)[1],
				translation_to_origin_filter->GetOutput()->GetPoint(i)[2]);

		Eigen::Vector3f est_transformation = (estimated_r * point) + estimated_t;

		Eigen::Vector3f real_transformation = (real_r * point) + real_t;

		est_transformation -= real_transformation;

		total += est_transformation.norm();

	}

	return total / points;
}

//
//Eigen::Vector3f calc_offset_vector_by_com(std::string& mesh_name){
//	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh_name);
//	double com[3];
//	calculate_center_of_mass(polydata_, com);
//	
//	Eigen::Vector3f offset(com[0], com[1], com[2]);
//	return offset;
//}

Eigen::Vector3f calc_offset_vector_by_com_After_initial_adjustment(std::string& mesh_name) {
	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh_name);
	double com[3];
	double bounds[6];
	calculate_center_of_mass(polydata_, com);
	std::cout << " initial com is (" << com[0] << ", " << com[1] << ", " << com[2] << std::endl;
	polydata_->GetBounds(bounds);
	std::cout << "initial bounds are " << std::endl;
	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl << std::endl;
	;

	float x = bounds[0] + (bounds[1] - bounds[0]) / 2.0;
	float y = bounds[2] + (bounds[3] - bounds[2]) / 2.0;
	float z = bounds[4] + (bounds[5] - bounds[4]) / 2.0;

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
//	translation_to_origin->Translate(-x, -y, -bounds[5]);	//same as z_min
	translation_to_origin->Translate(0, 0, -bounds[5]);	//same as z_min

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	vtkSmartPointer<vtkPolyData> translated_poly = translation_to_origin_filter->GetPolyDataOutput();
	calculate_center_of_mass(translated_poly, com);
	translated_poly->GetBounds(bounds);
	std::cout << " bounds are now " << std::endl;
	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl << std::endl;
	;

	Eigen::Vector3f offset(com[0], com[1], com[2]);
	return offset;
}

Eigen::Vector3f calc_offset_vector_by_com(vtkSmartPointer<vtkPolyData>& polydata) {
	double com[3];
	double bounds[6];
	calculate_center_of_mass(polydata, com);
	std::cout << " initial com is (" << com[0] << ", " << com[1] << ", " << com[2] << std::endl;
	polydata->GetBounds(bounds);
	std::cout << "initial bounds are " << std::endl;
	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl << std::endl;
	;

	Eigen::Vector3f offset(com[0], com[1], com[2]);
	return offset;
}


Eigen::Vector3f calc_offset_vector_by_com(std::string& mesh_name) {
	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh_name);
	double com[3];
	double bounds[6];
	calculate_center_of_mass(polydata_, com);
	std::cout << " initial com is (" << com[0] << ", " << com[1] << ", " << com[2] << std::endl;
	polydata_->GetBounds(bounds);
	std::cout << "initial bounds are " << std::endl;
	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl << std::endl;
	;

	Eigen::Vector3f offset(com[0], com[1], com[2]);
	return offset;
}

Eigen::Vector3f calc_offset_vector(std::string& mesh_name) {
	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh_name);
	double com[3];
	double bounds[6];
	calculate_center_of_mass(polydata_, com);
	std::cout << " initial com is (" << com[0] << ", " << com[1] << ", " << com[2] << std::endl;
	polydata_->GetBounds(bounds);
	std::cout << "initial bounds are " << std::endl;
	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl << std::endl;
	;

	//center of bounding box;
	float x = bounds[0] + (bounds[1] - bounds[0]) / 2.0;
	float y = bounds[2] + (bounds[3] - bounds[2]) / 2.0;
	float z = bounds[4] + (bounds[5] - bounds[4]) / 2.0;

	//we want to translate such that the center of the bounding box is at the origin and then shift up in z by half bb z val;

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-x, -y, -(z + (bounds[5] - bounds[4]) / 2.0));	//same as z_min

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	vtkSmartPointer<vtkPolyData> translated_poly = translation_to_origin_filter->GetPolyDataOutput();
	calculate_center_of_mass(translated_poly, com);
	translated_poly->GetBounds(bounds);
	std::cout << "xmin: " << bounds[0] << " " << "xmax: " << bounds[1] << std::endl << "ymin: " << bounds[2] << " " << "ymax: " << bounds[3] << std::endl << "zmin: " << bounds[4] << " " << "zmax: "
			<< bounds[5] << std::endl;

	std::cout << "center of mass is (" << com[0] << ", " << com[1] << ", " << com[2] << ")" << std::endl;

	Eigen::Vector3f offset(com[0], com[1], com[2]);
	return offset;
}

Eigen::Vector3f get_offset_vector_for_mesh_to_board(std::string& mesh_name) {

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh_name);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(translation_to_origin_filter->GetOutputPort());
	mapper->Update();

	vtkSmartPointer<vtkPolyData> translated_poly = translation_to_origin_filter->GetPolyDataOutput();
	calculate_center_of_mass(translated_poly, com);
	std::cout << " com is now (" << com[0] << ", " << com[1] << ", " << com[2] << ") " << std::endl;

	double bounds[6];
	translated_poly->GetBounds(bounds);
//	  << "xmin: " << bounds[0] << " " 
//	  << "xmax: " << bounds[1] << std::endl
//	  << "ymin: " << bounds[2] << " " 
//	  << "ymax: " << bounds[3] << std::endl
//	  << "zmin: " << bounds[4] << " " 
//	  << "zmax: " << bounds[5] << std::endl;

	/*
	 * As Hinterstoisser's ground truth axis are z into the screen is negative and vtk is the oppsite, we take z-max and we want
	 * to translate it by -z_max to bring the model in line with the board
	 */

	Eigen::Vector3f offset(0, 0, -bounds[5]);
	return offset;
}

Eigen::Vector3f get_adjusted_ground_truth_translation_vector(Eigen::Vector3f& original_translation, Eigen::Matrix3f& object_rotation_mat, Eigen::Vector3f& mesh_offset) {
	/*
	 * Explanation of the following:
	 * The ground truth translation is for the center of the marker board not the center of mass of the object
	 * This translation is given in units relative to the camera (i.e. in the camera coordinate system)
	 * 
	 * To bring the center of the marker board in line with the center of mass of the mesh we must 
	 * translate it by mesh_offset. Note though that this mesh offset it in wolrd coordinates (not camera)
	 * 
	 * To get it in camera coordinates, we first get the camera rotation matrix (as the inverse/transpose of the object one)
	 * Then we can extract the relationship of the camera axis to world coordinates.
	 * 
	 * Then to get the tranlation of the eye position (camera) we do 
	 * (-dot(x_axis,eye), -dot(y_axis, eye), -dot(z_axis,eye)) (as defined in render_image method)
	 * 
	 * so as we want to effectively offset this eye position by the mesh offset, the new_eye = eye - mesh_offset
	 * 
	 * so the new translation is (-dot(x_axis, new_eye) ...);
	 * new_eye = (e_x - o_x, e_y - o_y, e_z - o_z)
	 * -dot(x_axis, new_eye) = -( x_axis(0)*(e_x - o_x) + x_axis(1)*(e_y - o_y) + x_axis(2)*(e_z - o_z))
	 *  = -(x_axis(0)*e_x - x_axis(0)*o_x + x_axis(1)*e_y - x_axis(1)*o_y + x_axis(2)*e_z - x_axis(2)*o_z)
	 *  = -(dot(x_axis, eye) -dot(x_axis, offset))
	 *  = -dot(x_axis, eye) + dot(x_axis, offset)
	 *  ... and the same for the other two axis
	 * 
	 *  = original_translation + [ dot(x_axis, offset), dot(y_axis, offset), dot(z_axis, offset)];  
	 * 
	 */
	Eigen::Matrix3f camera_rotation_matrix = object_rotation_mat.transpose();

	Eigen::Vector3f x_axis(camera_rotation_matrix(0, 0), camera_rotation_matrix(1, 0), camera_rotation_matrix(2, 0));
	Eigen::Vector3f y_axis(camera_rotation_matrix(0, 1), camera_rotation_matrix(1, 1), camera_rotation_matrix(2, 1));
	Eigen::Vector3f z_axis(camera_rotation_matrix(0, 2), camera_rotation_matrix(1, 2), camera_rotation_matrix(2, 2));

	Eigen::Vector3f offset_in_camera_coordinate_system(x_axis.dot(mesh_offset), y_axis.dot(mesh_offset), z_axis.dot(mesh_offset));
//	std::cout<< "offset in cam coords is (" << offset_in_camera_coordinate_system(0) << ", "<< offset_in_camera_coordinate_system(1) << ", " << 
//			offset_in_camera_coordinate_system(2) << ")" << std::endl;

	Eigen::Vector3f ground_truth_adjusted_translation = original_translation + offset_in_camera_coordinate_system;
//	std::cout <<" result is (" << ground_truth_adjusted_translation(0) << ", "<< ground_truth_adjusted_translation(1) << ", " << ground_truth_adjusted_translation(2) << ") "<< std::endl;

	return ground_truth_adjusted_translation;
}

void get_calculated_com() {
	std::string mesh_name = "benchvise_mesh.ply";
	Eigen::Matrix3f R = get_rotation_matrix(97.7243, 78.2035, 7.8484);
//	Eigen::Matrix3f R = get_rotation_matrix( 0, 0, 0);
	Eigen::Vector3f tra(11.7692, 175.759, 1058.34);

	/*COM becomes where tra is as tra is calculated corresponding to the
	 * camera coordinate system so Tra is world coordinates which 
	 * were for example world coords (x,y,z)*-dot (x_Axis,y_axis_,z_axis) = tra (still world coordinates)
	 * Now to find image plane 
	 * 
	 * To find image plane coordinates we can do it two ways
	 * 1)
	 * 
	 * using world coordinate of center of mass = (0,0,0) then do the followind
	 * s[u v 1] = I*[R|T] *[0 0 0 1] where R is the inverse camera matrix (i.e the object rotation) 
	 * and T is tra (eye position in camera axis)
	 * 
	 * or the second way is I*T 
	 * 
	 * note however [R|T]*[0 0 0 1] == T so they are the same and T is the com position 
	 * and I*T is the position on the image plane
	 */

	vtkSmartPointer<vtkMatrix4x4> transformation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			transformation_matrix->SetElement(i, j, (double) R(i, j));
		}
	}

	for (int i = 0; i < 3; i++) {
		transformation_matrix->SetElement(i, 3, (double) tra(i));
	}

	Eigen::Vector3f _cam_view_up(0, -1, 0);

	std::string rgb_out = "rgb_24_tmp.png";
	std::string depth_out = "depth_24_tmp.png";
	std::string pose_out = "pose_24_tmp.png";

	vtkSmartPointer<vtkRenderer> _renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> _render_win = vtkSmartPointer<vtkRenderWindow>::New();

	_renderer->SetBackground(1.0, 1.0, 1.0);
	_renderer->Modified();
	_render_win->AddRenderer(_renderer);
	_render_win->SetSize(640, 480);
	_render_win->Start();

	render_image(mesh_name, transformation_matrix, _cam_view_up, rgb_out, depth_out, pose_out, _render_win, _renderer);
	//	
	cv::Mat r = cv::imread(rgb_out, CV_LOAD_IMAGE_ANYCOLOR);
	cv::Mat d = cv::imread(depth_out, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

	int minx;
	int maxx, miny, maxy;
	float com_x = 0, com_y = 0, com_z = 0;
	float number_of_points = 0;
	for (int y = 0; y < d.rows; y++) {
		for (int x = 0; x < d.cols; x++) {
			if (d.at < int16_t > (y, x) > 0) {
				minx = std::min(minx, x);
				maxx = std::max(maxx, x);
				miny = std::min(miny, y);
				maxy = std::max(maxy, y);
				com_x += x;
				com_y += y;
				com_z += d.at < int16_t > (y, x);
				number_of_points++;
			}
		}
	}

	com_x /= number_of_points;
	com_y /= number_of_points;
	com_z /= number_of_points;

	std::cout << " com is (" << com_x << ", " << com_y << ", " << com_z << ")" << std::endl;
	cv::imshow("r", r);
	//	cv::imshow("real", test_rgb);
	cv::waitKey(0);

}

#include <boost/tokenizer.hpp>
std::map<int, float> split_float_line(std::string& line) {
	typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
	boost::char_separator<char> sep(" ");
	Tokenizer info(line, sep);   // tokenize the line of data

	std::map<int, float> output;
	int cols = 0;
	for (Tokenizer::iterator it = info.begin(); it != info.end(); ++it) {
		output[cols] = strtod(it.current_token().c_str(), 0);
		cols++;
	}

	return output;
}


std::map<int, float> split_float_line_comma(std::string& line) {
	typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
	boost::char_separator<char> sep(",");
	Tokenizer info(line, sep);   // tokenize the line of data

	std::map<int, float> output;
	int cols = 0;
	for (Tokenizer::iterator it = info.begin(); it != info.end(); ++it) {
		output[cols] = strtod(it.current_token().c_str(), 0);
		cols++;
	}

	return output;
}


#include <fstream>
struct pose {
	float yaw;
	float pitch;
	float roll;
	pose(float y = 0, float p = 0, float r = 0)
			: yaw(y), pitch(p), roll(r) {
	}
	;
};

pose get_pose(int id, std::string& data_dir) {

	std::stringstream pose_filepath;
	pose_filepath << data_dir << "rot" << id << ".rot";

	std::ifstream pose_file(pose_filepath.str().c_str());
	std::string line;
	//do this first call to ignore the first line
	getline(pose_file, line);
	Eigen::Matrix3f R = Eigen::Matrix3f::Identity();
	int row = 0;
	while (getline(pose_file, line)) {
		std::map<int, float> output = split_float_line(line);
		R(row, 0) = output[0];
		R(row, 1) = output[1];
		R(row, 2) = output[2];
		row++;
	}

	double yaw = RAD2DEG(get_yaw(R));
	double pitch = RAD2DEG(get_pitch(R));
	double roll = RAD2DEG(get_roll(R));

	pose p;
	p.yaw = yaw;
	p.pitch = pitch;
	p.roll = roll;

	return p;
}

Eigen::Vector3f get_ground_truth_translation(int id, std::string& data_dir) {
	std::stringstream tra_filepath;
	tra_filepath << data_dir << "tra" << id << ".tra";

	std::ifstream tra_file(tra_filepath.str().c_str());
	std::string line;
	//ignore first line
	getline(tra_file, line);
	Eigen::Vector3f tra(0, 0, 0);
	int col = 0;
	while (getline(tra_file, line)) {
		//Change to mm
		float v = atof(line.c_str()) * 10.0f;
		tra(col) = v;
		col++;
	}
	return tra;
}

struct transformation{
	pose r;
	Eigen::Vector3f t;
};

#include <boost/algorithm/string.hpp>

std::vector<transformation> get_adjusted_ground_truth_transformations(int id, std::string& data_dir, Eigen::Vector3f& mesh_offset_from_board){
	using namespace boost::algorithm;
	std::stringstream filepath;
	filepath << data_dir << "img_" << id << ".txt";
	
	std::ifstream file(filepath.str().c_str());
	std::string line;
	//ignore first line
	getline(file, line);
	int instances = atoi(line.c_str());
	
//	std::cout <<" got file " << filepath.str() << " with " << instances << " instances " << std::endl;
	
	std::vector<transformation> ret;
	
	for(int i = 0; i < instances; i++){
		Eigen::Matrix3f R = Eigen::Matrix3f::Identity();
		getline(file, line);
		std::map<int, float> row1 = split_float_line_comma(line);
		R(0,0) = row1[0];
		R(0,1) = row1[1];
		R(0,2) = row1[2];
		
		getline(file, line);
		std::map<int, float> row2 = split_float_line_comma(line);
		
		R(1,0) = row2[0];
		R(1,1) = row2[1];
		R(1,2) = row2[2];
		
		getline(file, line);
		std::map<int, float> row3 = split_float_line_comma(line);
		
		R(2,0) = row3[0];
		R(2,1) = row3[1];
		R(2,2) = row3[2];

		Eigen::Vector3f T(row1[3], row2[3], row3[3]);
		
//		std::cout <<" R is " << std::endl;
//		for(int x = 0; x < 3; x++){
//			for(int y = 0; y < 3; y++){
//				std::cout << R(x,y) << " ";
//			}
//			std::cout << std::endl;
//		}
//		std::cout <<" T is (" << T(0) << ", "<< T(1) << ", "<< T(2) << std::endl;
		
		
		double yaw = RAD2DEG(get_yaw(R));
		double pitch = RAD2DEG(get_pitch(R));
		double roll = RAD2DEG(get_roll(R));

		pose p;
		p.yaw = yaw;
		p.pitch = pitch;
		p.roll = roll;
		
		transformation trans;
		trans.r = p;
		Eigen::Vector3f adj_T = get_adjusted_ground_truth_translation_vector(T, R, mesh_offset_from_board);
		
		trans.t = adj_T;
		
		ret.push_back(trans);
		//ignore row 4
		getline(file, line);
	}

	return ret;
}

Eigen::Vector3f get_adjusted_translation(int id, std::string& data_dir, Eigen::Vector3f& mesh_offset_from_board, pose& p) {
	Eigen::Matrix3f R = get_rotation_matrix(p.yaw, p.pitch, p.roll);

//	std::cout <<" R is " << std::endl;
//	print_matrix_to_console("R", R);

	//This returns it in mm
	Eigen::Vector3f tra = get_ground_truth_translation(id, data_dir);
//	std::cout <<" loaded tra is ("<< tra(0) << ", "<< tra(1) << ", "<< tra(2) << ") "<< std::endl;

	Eigen::Vector3f adj_tra = get_adjusted_ground_truth_translation_vector(tra, R, mesh_offset_from_board);

	return adj_tra;
}

std::vector<point3d> get_points_in_mm(std::string& object_file) {
	std::cout << " file is " << object_file << std::endl;
	std::vector<point3d> points;

	std::ifstream obj_file(object_file.c_str());
	std::string line;
	std::getline(obj_file, line);
	while (std::getline(obj_file, line)) {
//		std::cout <<" line is "<< line << std::endl;
		std::map<int, float> output = split_float_line(line);
		point3d p(output[0] * 10, output[1] * 10, output[2] * 10);
		points.push_back(p);
	}
	return points;
}

double get_accuracy_from_object_file(std::string& result_file, std::string& data_dir, Eigen::Vector3f& mesh_offset_from_board, std::string& object_file, double threshold) {

	std::vector<point3d> points_in_mm = get_points_in_mm(object_file);

	float fx = 572.4114;
	float fy = 573.5704;
	float cx = 325.2611;
	float cy = 242.0490;

	std::ifstream res_file(result_file.c_str());
	std::string line;

	double total_score = 0;
	double total_images = 0;
	while (std::getline(res_file, line)) {
		std::map<int, float> output = split_float_line(line);
		int id = output[0];
		float x = output[1];
		float y = output[2];
		float z = output[3];
		float yaw = output[4];
		float pitch = output[5];
		float roll = output[6];

		float estimated_x_world_coord = (x - cx) * z / fx;
		float estimated_y_world_coord = (y - cy) * z / fy;

		pose ground_truth_pose = get_pose(id, data_dir);
		Eigen::Vector3f ground_truth_adj_tra = get_adjusted_translation(id, data_dir, mesh_offset_from_board, ground_truth_pose);

		double score = calc_accuracy_from_object_file(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
				(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), points_in_mm);

//			double score = calc_accuracy(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
//						(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), mesh_filename);

		//		double score_no_trans = calc_accuracy_no_translation(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
		//				(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), mesh_filename);
		////		std::cout <<" score is " << score <<" score with no trans is "<< score_no_trans << std::endl;

		if (score <= threshold) {
			total_score++;
			if (id == 79) {
				std::cout << " correct - file id is " << id << " score is " << score << std::endl;
			}
		} else {
			if (id == 79) {
				std::cout << "  file id is " << id << " score is " << score << std::endl;
			}
		}
		total_images++;
	}
	std::cout << " total images is " << total_images << std::endl;
	return total_score / total_images;
}


//double get_drost_accuracy(std::string& result_file, Eigen::Vector3f& mesh_offset_from_board, std::string& mesh_filename, double threshold){
//	float fx = 572.4114;
//	float fy = 573.5704;
//	float cx = 325.2611;
//	float cy = 242.0490;
//
//	std::ifstream res_file(result_file.c_str());
//	std::string line;
//
//	double total_score = 0;
//	double total_images = 0;
//	while (std::getline(res_file, line)) {
//		if ((int) total_images % 10 == 0) {
//			std::cout << " total_images is " << total_images << " current_accuracy is " << total_score / total_images << std::endl;
//		}
//
//		std::map<int, float> output = split_float_line(line);
//		int id = output[0];
//		float x = output[1];
//		float y = output[2];
//		float z = output[3];
//		float yaw = output[4];
//		float pitch = output[5];
//		float roll = output[6];
//
//		float estimated_x_world_coord = (x - cx) * z / fx;
//		float estimated_y_world_coord = (y - cy) * z / fy;
//
//		pose ground_truth_pose = get_pose(id, data_dir);
//		Eigen::Vector3f ground_truth_adj_tra = get_adjusted_translation(id, data_dir, mesh_offset_from_board, ground_truth_pose);
//
//		double score = calc_accuracy(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
//				(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), mesh_filename);
//
//
//		if (score <= threshold) {
//			total_score++;
//		} 
//
//		total_images++;
//	}
//	std::cout << " total images is " << total_images << std::endl;
//	return total_score / total_images;
//}

double get_accuracy(std::string& result_file, std::string& data_dir, Eigen::Vector3f& mesh_offset_from_board, std::string& mesh_filename, double threshold) {
	/*
	 * lines in file are of format
	 * id x y z yaw pitch roll (x y are pixels z is mm)
	 */
	float fx = 572.4114;
	float fy = 573.5704;
	float cx = 325.2611;
	float cy = 242.0490;

	std::ifstream res_file(result_file.c_str());
	std::string line;

	double total_score = 0;
	double total_images = 0;
	while (std::getline(res_file, line)) {
		if ((int) total_images % 10 == 0) {
			std::cout << " total_images is " << total_images << " current_accuracy is " << total_score / total_images << std::endl;
		}

		std::map<int, float> output = split_float_line(line);
		int id = output[0];
		float x = output[1];
		float y = output[2];
		float z = output[3];
		float yaw = output[4];
		float pitch = output[5];
		float roll = output[6];

//		std::cout << "result is [" << id << " (" <<x << ", " << y << ", "<< z << ") pose: (" << yaw << ", "<< pitch << ", " << roll << ") " << std::endl; 

//		if(id == 1128){

		float estimated_x_world_coord = (x - cx) * z / fx;
		float estimated_y_world_coord = (y - cy) * z / fy;

//		std::cout << "(x,y) pixel is (" << x << ", " << y << ") and z is " << z << std::endl;
//		std::cout <<" real world estimated coords are (" << estimated_x_world_coord << ", "<< estimated_y_world_coord << ", " << z << " )" << std::endl;

		pose ground_truth_pose = get_pose(id, data_dir);
		Eigen::Vector3f ground_truth_adj_tra = get_adjusted_translation(id, data_dir, mesh_offset_from_board, ground_truth_pose);

		double score = calc_accuracy(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
				(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), mesh_filename);

		//double score = calc_accuracy_no_translation(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
		//	(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), mesh_filename);

//		std::cout <<" score is " << score << std::endl;

		if (score <= threshold) {
			total_score++;

			if (id == 79) {
				std::cout << " correct - file id is " << id << " score is " << score << std::endl;
			} else {
//					std::cout << "CORRECT********** result is [" << id << " (" <<x << ", " << y << ", "<< z << ") pose: (" << yaw << ", "<< pitch << ", " << roll << ") " << std::endl;
//					std::cout << "  file id is " << id << " score is " << score << std::endl;
			}
		} else {
//				if(id == 79){
					std::cout << "result is [" << id << " (" <<x << ", " << y << ", "<< z << ") pose: (" << yaw << ", "<< pitch << ", " << roll << ") " << std::endl;
					std::cout << "ground truth is (" <<(double) ground_truth_adj_tra(0) << ", "<< (double) ground_truth_adj_tra(1) << ", "<<  (double) ground_truth_adj_tra(2) << ") pose: ("
							<< ground_truth_pose.yaw << ", "<<  ground_truth_pose.pitch << ", "<<  ground_truth_pose.roll << ") "<< std::endl;
//					
					std::cout << "  file id is " << id << " score is " << score << std::endl;
					std::cout << std::endl;
//				}
		}
//			std::cout <<" estimated yaw, pitch, roll is (" << yaw << ", "<< pitch << ", "<< roll << ") estimated translation is (" << estimated_x_world_coord << ", "<< estimated_y_world_coord << ", "<< z << ")" << std::endl;
//			std::cout << "real yaw, pitch, roll is ( " << ground_truth_pose.yaw << ", " << ground_truth_pose.pitch << ", "<<  ground_truth_pose.roll << ") " << 
//					" real translation is (" << (double) ground_truth_adj_tra(0) << ", "<< (double) ground_truth_adj_tra(1) << ", "<< (double) ground_truth_adj_tra(2) << ") "<< std::endl;  
//			cv::Mat r = cv::imread("/home/aly/workspace/linemod_opencv_src/all_data/lamp/data/color1128.jpg", CV_LOAD_IMAGE_ANYCOLOR);
//			cv::imshow("r", r);
//			cv::waitKey(0);
//			exit(-1);
//		}
//		

//			std::cout << "score (with translation) is "<< score << std::endl;
//			double score_without_trans = calc_accuracy_no_translation(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, 
//			ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll, 
//			(double)ground_truth_adj_tra(0),  (double)ground_truth_adj_tra(1), (double)ground_truth_adj_tra(2), mesh_filename);
//			
//			std::cout << " score without translation is " << score_without_trans << std::endl;
//
//			if(score_without_trans <= threshold){
//				cout << "score without trans is " << score_without_trans << std::endl;
//				std::cout << "real (x,y,z) is (" <<(double)ground_truth_adj_tra(0) << ", "<<  (double)ground_truth_adj_tra(1) << ", "<<  (double)ground_truth_adj_tra(2) << ") " << 
//						" real (yaw, pitch, roll)  is (" << ground_truth_pose.yaw << ", " <<  ground_truth_pose.pitch << ", " <<  ground_truth_pose.roll << ") " << std::endl;  
//				std::cout << "id is "<< id << " estimated (x,y,z) is (" << x << "," << y << ", "<< z << ") estimated (yaw, pitch , roll) is (" << yaw << ", " << pitch << ", "<< roll << ")" << std::endl;
//				std::cout <<" estimated world (x,y,z) is (" << estimated_x_world_coord << ", "<< estimated_y_world_coord << ", " << z << " )" << std::endl;
//				
//				double our_score = calc_accuracy(-43.5136, -8.82181, 164.236, 92.2001, 98.3676, 913.578, 
//								ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll, 
//								(double)ground_truth_adj_tra(0),  (double)ground_truth_adj_tra(1), (double)ground_truth_adj_tra(2), mesh_filename);
//				
//				std::cout << "our score is "<< our_score << std::endl;
//				exit(-1);
//			}			
//		}

		total_images++;
	}
	std::cout << " total images is " << total_images << std::endl;
	return total_score / total_images;
}

void draw_estimation_and_ground_truth(double yaw, double pitch, double roll, double x, double y, double z, std::string mesh, cv::Mat& rgb, std::string& rgb_out, std::string& depth_out,
		std::string& pose_out, vtkSmartPointer<vtkRenderWindow>& render_win, vtkSmartPointer<vtkRenderer>& renderer, bool estimated = false) {

	/*
	 * Note real world coords calculated from the pixels will not look the same when projected back
	 * that is because vtk renderer foes not use the same internal params we use to convert from 
	 * pixel to world. There is a constant offset
	 */

	int cols_ = 640;
	int rows_ = 480;
	double z_near = 200;
	double z_far = 10000000;
	double view_angle = 45.4120;

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);

	Eigen::Matrix3Xf rot = get_rotation_matrix(yaw, pitch, roll);
	if (estimated) {
		for (int i = 0; i < 3; i++) {
			rot(1, i) = -rot(1, i);
			rot(2, i) = -rot(2, i);
		}
	}

	Eigen::Vector3f trans(x, y, z);
	if (!estimated) {
		Eigen::Vector3f offset = calc_offset_vector_by_com(mesh);
		Eigen::Matrix3f rot_copy;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				rot_copy(i, j) = rot(i, j);
			}
		}

		trans = get_adjusted_ground_truth_translation_vector(trans, rot_copy, offset);
		std::cout << " adj trans is (" << trans(0) << ", " << trans(1) << ", " << trans(2) << ") " << std::endl;
	}

	vtkSmartPointer<vtkMatrix4x4> object_pose_transformation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			object_pose_transformation_matrix->SetElement(i, j, (double) rot(i, j));
		}
	}
	for (int i = 0; i < 3; i++) {
		object_pose_transformation_matrix->SetElement(i, 3, (double) trans(i));
	}

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	vtkSmartPointer<vtkActor> est_poly_at_pose_actor = get_actor_of_poly_under_pose(mesh, object_pose_transformation_matrix);
	renderer->AddActor(est_poly_at_pose_actor);
	renderer->Modified();

	//	Eigen::Vector3f cam_view_up(_final_transformation_matrix->GetElement(0, 1), _final_transformation_matrix->GetElement(1, 1), _final_transformation_matrix->GetElement(2, 1));
	Eigen::Vector3f cam_view_up(0, -1, 0);

	vtkSmartPointer<vtkCamera> cam = get_camera_positioned_according_to_pose(object_pose_transformation_matrix, cam_view_up, z_near, z_far, view_angle);
	renderer->SetActiveCamera(cam);
	renderer->Modified();
	render_win->Render();
//	rwi->Start();

	save_images(renderer, render_win, object_pose_transformation_matrix, rgb_out, depth_out, pose_out, z_near, z_far, view_angle, cols_, rows_);

	cv::Mat m = cv::imread(rgb_out, CV_LOAD_IMAGE_ANYCOLOR);
	cv::Mat d = cv::imread(depth_out, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

	for (int y = 0; y < d.rows; y++) {
		for (int x = 0; x < d.cols; x++) {
			if (d.at < int16_t > (y, x) > 0) {
				rgb.at < cv::Vec3b > (y, x)[0] = m.at < cv::Vec3b > (y, x)[0];
				rgb.at < cv::Vec3b > (y, x)[1] = m.at < cv::Vec3b > (y, x)[1];
				rgb.at < cv::Vec3b > (y, x)[2] = m.at < cv::Vec3b > (y, x)[2];
			}
		}
	}
//	cv::imwrite(rgb_out, rgb);
	cv::imshow("r", rgb);
	cv::waitKey(0);

}

void generate_views_with_supporting_plane(std::string& mesh, double yaw, double pitch, double roll, double x, double y, double z, bool estimated) {
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> render_win = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderWindowInteractor> rwi = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->Modified();
	render_win->AddRenderer(renderer);
	render_win->SetSize(640, 480);
//	render_win->Start();
	rwi->SetRenderWindow(render_win);

	int cols_ = 640;
	int rows_ = 480;
	double z_near = 200;
	double z_far = 10000000;
	double view_angle = 45.4120;

	Eigen::Matrix3Xf rot = get_rotation_matrix(yaw, pitch, roll);
	if (estimated) {
		for (int i = 0; i < 3; i++) {
			rot(1, i) = -rot(1, i);
			rot(2, i) = -rot(2, i);
		}
	}

	Eigen::Vector3f trans(x, y, z);
	if (!estimated) {
		Eigen::Vector3f offset = calc_offset_vector_by_com(mesh);
		Eigen::Matrix3f rot_copy;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				rot_copy(i, j) = rot(i, j);
			}
		}

		trans = get_adjusted_ground_truth_translation_vector(trans, rot_copy, offset);
		std::cout << " adj trans is (" << trans(0) << ", " << trans(1) << ", " << trans(2) << ") " << std::endl;
	}

	vtkSmartPointer<vtkMatrix4x4> object_pose_transformation_matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			object_pose_transformation_matrix->SetElement(i, j, (double) rot(i, j));
		}
	}
	for (int i = 0; i < 3; i++) {
		object_pose_transformation_matrix->SetElement(i, 3, (double) trans(i));
	}

	vtkSmartPointer<vtkPolyData> polydata_ = get_polydata(mesh);
	double com[3];
	calculate_center_of_mass(polydata_, com);

	vtkSmartPointer<vtkTransform> translation_to_origin = vtkSmartPointer<vtkTransform>::New();
	translation_to_origin->Translate(-com[0], -com[1], -com[2]);

	vtkSmartPointer<vtkTransformFilter> translation_to_origin_filter = vtkSmartPointer<vtkTransformFilter>::New();
	translation_to_origin_filter->SetTransform(translation_to_origin);
	translation_to_origin_filter->SetInput(polydata_);
	translation_to_origin_filter->Update();

	double bounds[6];
	translation_to_origin_filter->GetPolyDataOutput()->GetBounds(bounds);
	double y_min = bounds[2];

	vtkSmartPointer<vtkPlaneSource> planeSource = vtkSmartPointer<vtkPlaneSource>::New();
//	 planeSource->SetCenter(object_pose_transformation_matrix->GetElement(0, 3), object_pose_transformation_matrix->GetElement(1, 3), object_pose_transformation_matrix->GetElement(2, 3));
//	planeSource->SetCenter(1,0,-100);
//	 planeSource->SetNormal(object_pose_transformation_matrix->GetElement(0, 1), object_pose_transformation_matrix->GetElement(1, 1), object_pose_transformation_matrix->GetElement(2, 1));
//	planeSource->SetNormal(1,0,1);
	planeSource->SetPoint1(object_pose_transformation_matrix->GetElement(0, 0) * 1000, object_pose_transformation_matrix->GetElement(0, 1) * 1000,
			object_pose_transformation_matrix->GetElement(0, 2) * 1000);
	planeSource->SetPoint2(object_pose_transformation_matrix->GetElement(1, 0) * 1000, object_pose_transformation_matrix->GetElement(1, 1) * 1000,
			object_pose_transformation_matrix->GetElement(1, 2) * 1000);
//	planeSource->SetResolution(1000,1000);

//	planeSource->SetPoint3(object_pose_transformation_matrix->GetElement(0,2), object_pose_transformation_matrix->GetElement(1, 2), object_pose_transformation_matrix->GetElement(2, 2));

	vtkSmartPointer<vtkPolyData> plane = planeSource->GetOutput();
	// Create a mapper and actor
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
#if VTK_MAJOR_VERSION <= 5
	mapper->SetInput(plane);
#else
	mapper->SetInputData(plane);
#endif

	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);

	vtkSmartPointer<vtkActor> poly_at_pose_actor = get_actor_of_poly_under_pose(mesh, object_pose_transformation_matrix);
//	renderer->AddActor(poly_at_pose_actor);

	Eigen::Vector3f cam_view_up(0, -1, 0);

	vtkSmartPointer<vtkCamera> cam = get_camera_positioned_according_to_pose(object_pose_transformation_matrix, cam_view_up, z_near, z_far, view_angle);
	renderer->SetActiveCamera(cam);
	renderer->Modified();

	renderer->AddActor(actor);
	renderer->SetBackground(.1, .2, .3); // Background color dark blue
	renderer->Modified();

	render_win->Render();
	rwi->Start();

	std::string rgb_out = "tmp_rgb.png";
	std::string depth_out = "tmp_depth.png";
	std::string pose_out = "tmp_pose.txt";

	save_images(renderer, render_win, object_pose_transformation_matrix, rgb_out, depth_out, pose_out, z_near, z_far, view_angle, cols_, rows_);
	cv::Mat rgb = cv::imread(rgb_out, CV_LOAD_IMAGE_ANYCOLOR);
	cv::Mat depth = cv::imread(depth_out, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
	cv::imshow("rgb", rgb);
	cv::Mat vis_d = get_visualizable_depth_img(depth);
	cv::imshow("depth", vis_d);
	cv::waitKey(0);

}




void produce_precision_recall_curve_drost(std::string& output_data, std::string& mesh_name, std::string& data_dir, std::string& out_file, bool partially_done){

	std::ifstream res_file(output_data.c_str());
	std::string line;

	std::ofstream out;
	
	if(partially_done){
		out.open(out_file.c_str(), ios::app);
	}
	else{
		out.open(out_file.c_str());
	}

	std::vector<int> ids_already_done;
	if (partially_done) {
		std::ifstream res_file(out_file.c_str());
		std::string line;
		while (std::getline(res_file, line)) {
			std::map<int, float> output = split_float_line(line);
			int id = output[0];
			if(find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()){
				ids_already_done.push_back(id);
			}
		}
	}
	
	
	float fx = 572.4114;
	float fy = 573.5704;
	float cx = 325.2611;
	float cy = 242.0490;

	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(mesh_name);

	vtkSmartPointer<vtkPolyData> polydata = get_polydata(mesh_name);
	std::cout << " doing file " << std::endl;
	int current_id = -1;
	int count = ids_already_done.size();
	
	while (std::getline(res_file, line)) {
		std::map<int, float> output = split_float_line(line);
		int id = output[0];
		
		Eigen::Matrix3Xf drost_rot_mat = Eigen::Matrix3f::Identity();
				
		
		drost_rot_mat(0,0) = output[1];
		drost_rot_mat(0,1)  = output[2];
		drost_rot_mat(0,2)  = output[3];
		float x_world = output[4];
		
		//Note negating second and third rows (flipping) so that the axis match our training axis
		drost_rot_mat(1,0)  = -output[5];
		drost_rot_mat(1,1)  = -output[6];
		drost_rot_mat(1,2)  = -output[7];
		float y_world = output[8];
		
		drost_rot_mat(2,0) = -output[9];
		drost_rot_mat(2,1) = -output[10];
		drost_rot_mat(2,2) = -output[11];
		float z = output[12];

		float yaw = RAD2DEG(get_yaw(drost_rot_mat));
		float pitch = RAD2DEG(get_pitch(drost_rot_mat));
		float roll = RAD2DEG(get_roll(drost_rot_mat));
		
		
		float x = (x_world/z)*fx + cx;
		float y = (y_world/z)*fy + cy;
		
		float weight = output[13];
		
		if (!partially_done || std::find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()) {
			if (id != current_id) {
				count++;
				std::cout << " doing id " << id << " number " << count << std::endl;
				current_id = id;
			}

			float estimated_x_world_coord = (x - cx) * z / fx;
			float estimated_y_world_coord = (y - cy) * z / fy;

			pose ground_truth_pose = get_pose(id, data_dir);
			Eigen::Vector3f ground_truth_adj_tra = get_adjusted_translation(id, data_dir, mesh_offset_from_board, ground_truth_pose);

			double score = calc_accuracy_with_poly(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
					(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), polydata);

			
			std::cout << "result is [" << id << " (" << x << ", " << y << ", " << z << ") pose: (" << yaw << ", " << pitch << ", " << roll << ") " << std::endl;
			std::cout << "ground truth is (" << (double) ground_truth_adj_tra(0) << ", " << (double) ground_truth_adj_tra(1) << ", " << (double) ground_truth_adj_tra(2) << ") pose: ("
					<< ground_truth_pose.yaw << ", " << ground_truth_pose.pitch << ", " << ground_truth_pose.roll << ") " << std::endl;
//					
			std::cout << "  file id is " << id << " score is " << score << std::endl;
			std::cout << std::endl;
			
			//calculate if correct at threshold then output to new_file
			out << id << ", " << weight << ", " << score << std::endl;

		}
	}
}


void produce_precision_recall_curve_our_dataset_symmetric_objects(std::string& output_data, 
		std::string& mesh_name, std::string& data_dir, std::string& out_file, 
		bool partially_done, vtkSmartPointer<vtkTransform>& pre_transform){
	
	std::ifstream res_file(output_data.c_str());
	std::string line;

	std::ofstream out;

	if (partially_done) {
		out.open(out_file.c_str(), ios::app);
	} else {
		out.open(out_file.c_str());
	}

	std::vector<int> ids_already_done;
	if (partially_done) {
		std::ifstream res_file(out_file.c_str());
		std::string line;
		while (std::getline(res_file, line)) {
			std::map<int, float> output = split_float_line(line);
			int id = output[0];
			if (find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()) {
				ids_already_done.push_back(id);
			}
		}
	}

	float fx = 571.9737;
	float fy = 571.0073;
	float cx = 319.5000;
	float cy = 239.5000;
	/*
	 *GET offset and transformed poly for out dataset 
	 * 
	 */
	vtkSmartPointer<vtkPolyData> obj = get_polydata(mesh_name);

	vtkSmartPointer<vtkTransformFilter> rotation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	rotation_filter->SetInput(obj);
	rotation_filter->SetTransform(pre_transform);
	rotation_filter->Update();

	double bounds[6];
	vtkSmartPointer<vtkPolyData> poly = rotation_filter->GetPolyDataOutput();
	poly->GetBounds(bounds);

	float x_trans = -(bounds[0] + ((bounds[1] - bounds[0]) / 2.0));
	float y_trans = -(bounds[2] + ((bounds[3] - bounds[2]) / 2.0));
	float z_trans = -bounds[5]; //max z

	vtkSmartPointer<vtkTransform> bring_to_origin_transform = vtkSmartPointer<vtkTransform>::New();
	bring_to_origin_transform->Translate(x_trans, y_trans, z_trans);

	vtkSmartPointer<vtkTransformFilter> origin_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	origin_transform_filter->SetInput(poly);
	origin_transform_filter->SetTransform(bring_to_origin_transform);
	origin_transform_filter->Update();

	vtkSmartPointer<vtkPolyData> polydata = origin_transform_filter->GetPolyDataOutput();

	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(polydata);
	/*
	 * End
	 * 
	 */ 

	std::cout << " doing file " << output_data  << std::endl;
	int current_id = -1;
	int count = ids_already_done.size();

	
	std::map<int, std::vector<std::vector<float> > > img_id_to_gt_transformation_values;
	
	std::vector<std::vector<Eigen::Vector3f> > real_transformed_points_for_current_id;
	
	while (std::getline(res_file, line)) {
		std::cout <<" got line "<< line << std::endl;
		std::map<int, float> output = split_float_line(line);
		int id = output[0];
		float x = output[1];
		float y = output[2];
		float z = output[3];
		float yaw = output[4];
		float pitch = output[5];
		float roll = output[6];
		float weight = output[7];
		if (!partially_done || std::find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()) {

			if (id != current_id) {
				count++;
				std::cout << " doing id " << id << " number " << count << std::endl;
				current_id = id;
				
				//clear and re populate the real transformed points
				real_transformed_points_for_current_id.clear();
				std::vector<transformation> adj_gt_transforamtions = get_adjusted_ground_truth_transformations(id, data_dir, mesh_offset_from_board);
				for(std::vector<transformation>::iterator t = adj_gt_transforamtions.begin(); t != adj_gt_transforamtions.end(); t++){
					pose ground_truth_pose = t->r;
					Eigen::Vector3f ground_truth_adj_tra = t->t;
					std::vector<Eigen::Vector3f> transformed_points = get_tranformed_points(ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll, 
							ground_truth_adj_tra(0), ground_truth_adj_tra(1), ground_truth_adj_tra(2),
							polydata);
					real_transformed_points_for_current_id.push_back(transformed_points);
				}
			}

			float estimated_x_world_coord = (x - cx) * z / fx;
			float estimated_y_world_coord = (y - cy) * z / fy;


			double min_score = DBL_MAX;
			for(std::vector<std::vector<Eigen::Vector3f> >::iterator gt = real_transformed_points_for_current_id.begin(); gt != real_transformed_points_for_current_id.end(); gt++){
				double score = calc_accuracy_symmetric_obj_with_poly(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, *gt, polydata);
				min_score = std::min(score, min_score);
			}
			std::cout << "min score is " << min_score << std::endl;
			//calculate if correct at threshold then output to new_file
			out << id << ", " << weight << ", " << min_score << std::endl;
		}
	}
}


void produce_precision_recall_curve_our_dataset(std::string& output_data, std::string& mesh_name, std::string& data_dir, std::string& out_file, bool partially_done, bool symmetric,
		vtkSmartPointer<vtkTransform>& pre_transform){
	std::ifstream res_file(output_data.c_str());
	std::string line;

	std::ofstream out;

	if (partially_done) {
		out.open(out_file.c_str(), ios::app);
	} else {
		out.open(out_file.c_str());
	}

	std::vector<int> ids_already_done;
	if (partially_done) {
		std::ifstream res_file(out_file.c_str());
		std::string line;
		while (std::getline(res_file, line)) {
			std::map<int, float> output = split_float_line(line);
			int id = output[0];
			if (find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()) {
				ids_already_done.push_back(id);
			}
		}
	}

	float fx = 571.9737;
	float fy = 571.0073;
	float cx = 319.5000;
	float cy = 239.5000;
		
	//GET offset and transformed poly for out dataset
	vtkSmartPointer<vtkPolyData> obj = get_polydata(mesh_name);

	vtkSmartPointer<vtkTransformFilter> rotation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	rotation_filter->SetInput(obj);
	rotation_filter->SetTransform(pre_transform);
	rotation_filter->Update();

	double bounds[6];
	vtkSmartPointer<vtkPolyData> poly = rotation_filter->GetPolyDataOutput();
	poly->GetBounds(bounds);

	float x_trans = -(bounds[0] + ((bounds[1] - bounds[0]) / 2.0));
	float y_trans = -(bounds[2] + ((bounds[3] - bounds[2]) / 2.0));
	float z_trans = -bounds[5]; //max z

	vtkSmartPointer<vtkTransform> bring_to_origin_transform = vtkSmartPointer<vtkTransform>::New();
	bring_to_origin_transform->Translate(x_trans, y_trans, z_trans);

	vtkSmartPointer<vtkTransformFilter> origin_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	origin_transform_filter->SetInput(poly);
	origin_transform_filter->SetTransform(bring_to_origin_transform);
	origin_transform_filter->Update();

	vtkSmartPointer<vtkPolyData> polydata = origin_transform_filter->GetPolyDataOutput();

	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(polydata);
	//END 

	std::cout << " doing file " << output_data  << std::endl;
	int current_id = -1;
	int count = ids_already_done.size();

	while (std::getline(res_file, line)) {
		std::cout <<" got line "<< line << std::endl;
		std::map<int, float> output = split_float_line(line);
		int id = output[0];
		float x = output[1];
		float y = output[2];
		float z = output[3];
		float yaw = output[4];
		float pitch = output[5];
		float roll = output[6];
		float weight = output[7];
		if (!partially_done || std::find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()) {

			if (id != current_id) {
				count++;
				std::cout << " doing id " << id << " number " << count << std::endl;

				current_id = id;
			}

			float estimated_x_world_coord = (x - cx) * z / fx;
			float estimated_y_world_coord = (y - cy) * z / fy;

			std::vector<transformation> adj_gt_transforamtions = get_adjusted_ground_truth_transformations(id, data_dir, mesh_offset_from_board);

			double min_score = DBL_MAX;
			for(std::vector<transformation>::iterator t = adj_gt_transforamtions.begin(); t != adj_gt_transforamtions.end(); t++){
				//For each instance
				pose ground_truth_pose = t->r;
				Eigen::Vector3f ground_truth_adj_tra = t->t;
				double score = 0;
				if(symmetric){
					score = calc_accuracy_symmetric_obj_with_poly(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
									(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), polydata);
				}
				else{
					score = calc_accuracy_with_poly(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
									(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), polydata);
				}
				min_score = std::min(score, min_score);

			}
			std::cout << "min score is " << min_score << std::endl;
			//calculate if correct at threshold then output to new_file
			out << id << ", " << weight << ", " << min_score << std::endl;
		}
	}
}


void produce_precision_recall_curve_date(std::string& output_data, std::string& mesh_name, std::string& data_dir, std::string& out_file, bool partially_done, vtkSmartPointer<vtkTransform>& previous_transform) {

	std::ifstream res_file(output_data.c_str());
	std::string line;

	std::ofstream out;
	
	if(partially_done){
		out.open(out_file.c_str(), ios::app);
	}
	else{
		out.open(out_file.c_str());
	}

	std::vector<int> ids_already_done;
	if (partially_done) {
		std::ifstream res_file(out_file.c_str());
		std::string line;
		while (std::getline(res_file, line)) {
			std::map<int, float> output = split_float_line(line);
			int id = output[0];
			if(find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()){
				ids_already_done.push_back(id);
			}
		}
	}
	
	
	float fx = 572.4114;
	float fy = 573.5704;
	float cx = 325.2611;
	float cy = 242.0490;

//	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(mesh_name);
	
	vtkSmartPointer<vtkPolyData> initial_ply = get_polydata(mesh_name);
	vtkSmartPointer<vtkTransformFilter> prv_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	prv_transform_filter->SetInput(initial_ply);
	prv_transform_filter->SetTransform(previous_transform);
	prv_transform_filter->Update();
	
	vtkSmartPointer<vtkPolyData> ply = prv_transform_filter->GetPolyDataOutput();
	
	double bounds[6];
	ply->GetBounds(bounds);
	
	float x_min = bounds[0];
	float x_max = bounds[1];
	float y_min = bounds[2];
	float y_max = bounds[3];
	float z_min = bounds[4];
	float z_max = bounds[5];

	std::cout << " bounds are x(" << x_min << ", " << x_max << ") y(" << y_min << ", " << y_max << ") z " << z_min << ", " << z_max << ") " << std::endl;

	float x_trans = x_min + (x_max - x_min) / 2.0;
	float y_trans = y_min + (y_max - y_min)/2.0;
	float z_trans = z_max; 
	
	vtkSmartPointer<vtkTransform> trans_to_top_of_board = vtkSmartPointer<vtkTransform>::New();
	trans_to_top_of_board->Translate(-x_trans, -y_trans, -z_trans);
	
	vtkSmartPointer<vtkTransformFilter> top_of_board_filter = vtkSmartPointer<vtkTransformFilter>::New();
	top_of_board_filter->SetInput(ply);
	top_of_board_filter->SetTransform(trans_to_top_of_board);
	top_of_board_filter->Update();
	
	vtkSmartPointer<vtkPolyData> polydata = top_of_board_filter->GetPolyDataOutput();
	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(polydata);
	
	
	std::cout << " doing file " << std::endl;
	int current_id = -1;
	int count = ids_already_done.size();
	
	while (std::getline(res_file, line)) {
		std::map<int, float> output = split_float_line(line);
		int id = output[0];
		float x = output[1];
		float y = output[2];
		float z = output[3];
		float yaw = output[4];
		float pitch = output[5];
		float roll = output[6];
		float weight = output[7];
		if (!partially_done || std::find(ids_already_done.begin(), ids_already_done.end(), id) == ids_already_done.end()) {

			if (id != current_id) {
				count++;
				std::cout << " doing id " << id << " number " << count << std::endl;

				current_id = id;

			}

			float estimated_x_world_coord = (x - cx) * z / fx;
			float estimated_y_world_coord = (y - cy) * z / fy;

			pose ground_truth_pose = get_pose(id, data_dir);
			Eigen::Vector3f ground_truth_adj_tra = get_adjusted_translation(id, data_dir, mesh_offset_from_board, ground_truth_pose);

			double score = calc_accuracy_with_poly(yaw, pitch, roll, estimated_x_world_coord, estimated_y_world_coord, z, ground_truth_pose.yaw, ground_truth_pose.pitch, ground_truth_pose.roll,
					(double) ground_truth_adj_tra(0), (double) ground_truth_adj_tra(1), (double) ground_truth_adj_tra(2), polydata);

			//calculate if correct at threshold then output to new_file
			out << id << ", " << weight << ", " << score << std::endl;

		}
	}
}
#include <vtkBoundingBox.h>
#include <vtkOutlineSource.h>
void generate_training_images(){

	vtkSmartPointer<vtkMatrix4x4> cam_previous_transform_mat = vtkSmartPointer<vtkMatrix4x4>::New();
	cam_previous_transform_mat->SetElement(0, 0, 0.6709);
	cam_previous_transform_mat->SetElement(0, 1, 0.5704);
	cam_previous_transform_mat->SetElement(0, 2, -0.4739);
	cam_previous_transform_mat->SetElement(0, 3, 269.8005);

	cam_previous_transform_mat->SetElement(1, 0, -0.7005);
	cam_previous_transform_mat->SetElement(1, 1, 0.6972);
	cam_previous_transform_mat->SetElement(1, 2, -0.1524);
	cam_previous_transform_mat->SetElement(1, 3, 87.1281);

	cam_previous_transform_mat->SetElement(2, 0, 0.2434);
	cam_previous_transform_mat->SetElement(2, 1, 0.4342);
	cam_previous_transform_mat->SetElement(2, 2, 0.8673);
	cam_previous_transform_mat->SetElement(2, 3, -610.4048);
	
	vtkSmartPointer<vtkTransform> cam_pre_transform = vtkSmartPointer<vtkTransform>::New();
	cam_pre_transform->SetMatrix(cam_previous_transform_mat);
	
	vtkSmartPointer<vtkMatrix4x4> eggbox_previous_transform_mat = vtkSmartPointer<vtkMatrix4x4>::New();
	eggbox_previous_transform_mat->SetElement(0, 0, -0.7968);
	eggbox_previous_transform_mat->SetElement(0, 1, 0.6016);
	eggbox_previous_transform_mat->SetElement(0, 2, -0.0557);
	eggbox_previous_transform_mat->SetElement(0, 3, 10.2387);

	eggbox_previous_transform_mat->SetElement(1, 0, 0.5198);
	eggbox_previous_transform_mat->SetElement(1, 1, 0.6356);
	eggbox_previous_transform_mat->SetElement(1, 2, -0.5708);
	eggbox_previous_transform_mat->SetElement(1, 3, 318.4659);

	eggbox_previous_transform_mat->SetElement(2, 0, -0.3080);
	eggbox_previous_transform_mat->SetElement(2, 1, -0.4838);
	eggbox_previous_transform_mat->SetElement(2, 2, -0.8192);
	eggbox_previous_transform_mat->SetElement(2, 3, 446.1539);
	
	
	vtkSmartPointer<vtkTransform> eggbox_pre_transform = vtkSmartPointer<vtkTransform>::New();
	eggbox_pre_transform->SetMatrix(eggbox_previous_transform_mat);
	
	vtkSmartPointer<vtkTransform> our_registered_prev_transform = vtkSmartPointer<vtkTransform>::New();
	our_registered_prev_transform->Scale(1000, 1000, 1000);
	
	
	std::vector<double> scales;
//	scales.push_back(1150.0);
//	scales.push_back(1050.0);
//	scales.push_back(950.0);
//	scales.push_back(850.0);
//	scales.push_back(750.0);
	scales.push_back(650.0);

	std::vector<double> in_plane_rotations;
	in_plane_rotations.push_back(-15);
	in_plane_rotations.push_back(-30);
	in_plane_rotations.push_back(-45);
	in_plane_rotations.push_back(0);
	in_plane_rotations.push_back(15);
	in_plane_rotations.push_back(30);
	in_plane_rotations.push_back(45);

	
//	std::string output_prefix = "our_coffe_cup_train/validation_subset/coffee_cup_";
//	std::string mesh_ = "coffee_cup.ply";
//
//	vtkSmartPointer<vtkMatrix4x4> z_rot = get_rotation_around_z(-3);
//	vtkSmartPointer<vtkMatrix4x4> rot_mat = get_rotation_around_x(-116);
//	vtkSmartPointer<vtkTransform> our_coffee_cup_previous_transform = vtkSmartPointer<vtkTransform>::New();
//	our_coffee_cup_previous_transform->Scale(1000, 1000, 1000);
//	our_coffee_cup_previous_transform->Concatenate(rot_mat);
//	our_coffee_cup_previous_transform->Concatenate(z_rot);
//	generate_all_views_our_dataset(mesh_, in_plane_rotations, scales, output_prefix, our_coffee_cup_previous_transform);
//	
	//
//	std::string output_prefix = "cam_train_all_scales_correct_com/cam_";
//	std::string mesh_ = "cam_mesh.ply";
//	generate_all_views_our_dataset(mesh_, in_plane_rotations, scales, output_prefix, cam_pre_transform);
	
//	std::string output_prefix = "holepuncher_train_all_scales_correct_com/holepuncher_";
//	std::string mesh_ = "holepuncher_mesh.ply";
//	std::string output_prefix = "can_train_all_scales_correct_com/can_";
//	std::string mesh_ = "can_mesh.ply";
	
//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/joystick_train_all_images/joystick_";
//	std::string mesh_ = "joystick_segmented_registered.ply";

//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/shampoo_train_all_images/shampoo_";
//	std::string mesh_ = "shampoo_segmented_registered.ply";
	


//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/juice_carton_train_all_images/juice_carton_";
//	std::string mesh_ = "JuiceCarton_segmented_registered.ply";
	
//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/milk_carton_train_all_images/milk_carton_";
//	std::string mesh_ = "MilkCarton_segmented_registered.ply";
	
	
//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/xbox_train_all_images/xbox_";
//	std::string mesh_ = "/home/aly/workspace/viewpoint_rendering/our_registered_meshes/xbox_segmented_registered.ply";
	
//		std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/stapler_train_all_images/stapler_";
//		std::string mesh_ = "/home/aly/workspace/viewpoint_rendering/our_registered_meshes/stapler_segmented_registered.ply";
	
//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/mug_train_all_images/mug_";
//	std::string mesh_ = "/home/aly/workspace/viewpoint_rendering/our_registered_meshes/mug_segmented_registered_tmp.ply";
//	
//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/our_camera_train_all_images/our_camera_";
//	std::string mesh_ = "/home/aly/workspace/viewpoint_rendering/our_registered_meshes/camera_segmented_registered.ply";

	//May 23rd experiment
//	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/tmp_camera/our_camera_";
//	std::string mesh_ = "/media/hd2/eccv14/our_dataset/meshes/camera_segmented_registered.ply";
//	
//	generate_all_views_our_dataset(mesh_, in_plane_rotations, scales, output_prefix, our_registered_prev_transform);
	
	
	//another may 23rd experiment
	std::string output_prefix = "/home/aly/workspace/viewpoint_rendering/tmp_coffee/coffee_cup_";
	std::string mesh_ = "/media/hd2/eccv14/our_dataset/meshes/coffee_cup.ply";

	vtkSmartPointer<vtkMatrix4x4> z_rot = get_rotation_around_z(-3);
	vtkSmartPointer<vtkMatrix4x4> rot_mat = get_rotation_around_x(-116);
	vtkSmartPointer<vtkTransform> our_coffee_cup_previous_transform = vtkSmartPointer<vtkTransform>::New();
	our_coffee_cup_previous_transform->Scale(1000, 1000, 1000);
	our_coffee_cup_previous_transform->Concatenate(rot_mat);
	our_coffee_cup_previous_transform->Concatenate(z_rot);
	generate_all_views_our_dataset(mesh_, in_plane_rotations, scales, output_prefix, our_coffee_cup_previous_transform);
	
	
	
//	generate_all_views(mesh_, in_plane_rotations, scales, output_prefix);
}


//	std::vector<double> cup_scales;
//	cup_scales.push_back(1050.0);
//	std::vector<double> in_plane_rotations;
//	in_plane_rotations.push_back(-15);
//	in_plane_rotations.push_back(-30);
//	in_plane_rotations.push_back(-45);
//	in_plane_rotations.push_back(0);
//	in_plane_rotations.push_back(15);
//	in_plane_rotations.push_back(30);
//	in_plane_rotations.push_back(45);
//	

//	generate_all_views_our_dataset(cup_mesh_name, in_plane_rotations, cup_scales, output_prefix, complete_transform);
//	exit(-1);

Eigen::Vector3f get_offset_vector_our_dataset(std::string& mesh_name, vtkSmartPointer<vtkTransform>& prev_transform){
	vtkSmartPointer<vtkPolyData> obj = get_polydata(mesh_name);
	
	vtkSmartPointer<vtkTransformFilter> rotation_filter = vtkSmartPointer<vtkTransformFilter>::New();
	rotation_filter->SetInput(obj);
	rotation_filter->SetTransform(prev_transform);
	rotation_filter->Update();
	
	double bounds[6];
	vtkSmartPointer<vtkPolyData> poly = rotation_filter->GetPolyDataOutput();
	poly->GetBounds(bounds);
	
	float x_trans = -(bounds[0] + ((bounds[1] - bounds[0]) / 2.0));
	float y_trans = -(bounds[2] + ((bounds[3] - bounds[2]) / 2.0));
	float z_trans = -bounds[5]; //max z

	vtkSmartPointer<vtkTransform> bring_to_origin_transform = vtkSmartPointer<vtkTransform>::New();
	bring_to_origin_transform->Translate(x_trans, y_trans, z_trans);

	vtkSmartPointer<vtkTransformFilter> origin_transform_filter = vtkSmartPointer<vtkTransformFilter>::New();
	origin_transform_filter->SetInput(poly);
	origin_transform_filter->SetTransform(bring_to_origin_transform);
	origin_transform_filter->Update();

	vtkSmartPointer<vtkPolyData> origin_transformed_poly = origin_transform_filter->GetPolyDataOutput();

	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(origin_transformed_poly);
	
	return mesh_offset_from_board;
}

int main(int argc, char **argv) {
	
	generate_training_images();
	exit(-1);
	
	
//	Eigen::Matrix3Xf rotation_mat = get_rotation_matrix( -165, 6.28912e-16, -121.717);
//	for(int i = 0; i < 3; i ++){
//		for(int j = 0; j < 3; j++){
//			std::cout << rotation_mat(i,j) << " ";
//		}
//		std::cout << std::endl;
//	}
//
//	exit(-1);
	
	
	vtkSmartPointer<vtkMatrix4x4> our_coffee_cup_z_rot = get_rotation_around_z(-3);
	vtkSmartPointer<vtkMatrix4x4> our_coffee_cup_rot_mat = get_rotation_around_x(-116);

	vtkSmartPointer<vtkTransform> our_coffee_cup_prev_transform = vtkSmartPointer<vtkTransform>::New();
	our_coffee_cup_prev_transform->Scale(1000, 1000, 1000);
	our_coffee_cup_prev_transform->Concatenate(our_coffee_cup_rot_mat);
	our_coffee_cup_prev_transform->Concatenate(our_coffee_cup_z_rot);
	
	/***
	 * 
	 * pr curves our dataset
	 * 
	 */
	
	//	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_1_third_pr.txt";
	//	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_0.25_pr.txt";
	
	
	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_2_third_pr.txt";
	std::string m_name =  "coffee_cup_simplified3.ply";
	std::string d_dir = "/home/aly/workspace/linemod_opencv_src/all_data/subset_cup/Annotation_New/";
	std::string o_file = "our_coffee_cup_2_third_patch_iter_prob_no_thresh_simplified_3_cup.txt";
//	

//	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_1_third_pr.txt";
//	std::string m_name =  "coffee_cup_simplified3.ply";
//	std::string d_dir = "/home/aly/workspace/linemod_opencv_src/all_data/subset_cup/Annotation_New/";
//	std::string o_file = "our_coffee_cup_1_third_patch_iter_prob_no_thresh_simplified_3_cup.txt";
//	
	
//	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_0.25_pr.txt";
//	std::string m_name =  "coffee_cup_simplified3.ply";
//	std::string d_dir = "/home/aly/workspace/linemod_opencv_src/all_data/subset_cup/Annotation_New/";
//	std::string o_file = "our_coffee_cup_1_third_patch_iter_prob_no_thresh_simplified_3_cup.txt";
	
//	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_1_fifth_pr.txt";
//	std::string m_name =  "coffee_cup_simplified3.ply";
//	std::string d_dir = "/home/aly/workspace/linemod_opencv_src/all_data/subset_cup/Annotation_New/";
//	std::string o_file = "our_coffee_cup_1_fifth_patch_iter_prob_no_thresh_simplified_3_cup.txt";
	
//	std::string pr_res = "/home/aly/workspace/linemod_opencv_src/our_coffee_cup_subset_0.5_pr.txt";
//	std::string m_name =  "coffee_cup_simplified3.ply";
//	std::string d_dir = "/home/aly/workspace/linemod_opencv_src/all_data/subset_cup/Annotation_New/";
//	std::string o_file = "our_coffee_cup_0.5_patch_iter_prob_no_thresh_simplified_3_cup.txt";
	
	produce_precision_recall_curve_our_dataset_symmetric_objects(pr_res, m_name, d_dir, o_file, true, our_coffee_cup_prev_transform);
//	produce_precision_recall_curve_our_dataset(pr_res, m_name, d_dir, o_file, false, symmetric, our_coffee_cup_prev_transform);
	
	exit(-1);
	
//	generate_training_images();
//	
//	exit(-1);
	
	
	vtkSmartPointer<vtkMatrix4x4> cam_previous_transform_mat = vtkSmartPointer<vtkMatrix4x4>::New();
	cam_previous_transform_mat->SetElement(0, 0, 0.6709);
	cam_previous_transform_mat->SetElement(0, 1, 0.5704);
	cam_previous_transform_mat->SetElement(0, 2, -0.4739);
	cam_previous_transform_mat->SetElement(0, 3, 269.8005);

	cam_previous_transform_mat->SetElement(1, 0, -0.7005);
	cam_previous_transform_mat->SetElement(1, 1, 0.6972);
	cam_previous_transform_mat->SetElement(1, 2, -0.1524);
	cam_previous_transform_mat->SetElement(1, 3, 87.1281);

	cam_previous_transform_mat->SetElement(2, 0, 0.2434);
	cam_previous_transform_mat->SetElement(2, 1, 0.4342);
	cam_previous_transform_mat->SetElement(2, 2, 0.8673);
	cam_previous_transform_mat->SetElement(2, 3, -610.4048);

	vtkSmartPointer<vtkTransform> cam_pre_transform = vtkSmartPointer<vtkTransform>::New();
	cam_pre_transform->SetMatrix(cam_previous_transform_mat);
	
//	std::string pr_results_out = "/home/aly/workspace/eccv14_resutls/ours/driller/raw_vals/driller_pr_40%_2_third_patch_no_discard.txt";
//	std::string pr_results_out = "/home/aly/workspace/eccv14_resutls/ours/driller/raw_vals/driller_pr_40%_2_third_patch_10_5_iter_prob_no_thresh.txt";
//	std::string pr_results_out = "/home/aly/workspace/eccv14_resutls/ours/driller/raw_vals/driller_pr_40%_2_third_patch_10_5_iter_prob_50%.txt";
//	std::string pr_results_out = "/home/aly/workspace/eccv14_resutls/ours/driller/raw_vals/driller_pr_40%_2_third_patch_10_5_iter_prob_50%.txt";
	
	std::string pr_results_out = "/home/aly/workspace/eccv14_resutls/ours/cam/raw_vals/cam_pr_40%_2_third_patch_10_5_iter_prob_no_thresh.txt";
	

	
//	std::string mesh_name = "benchvise_mesh.ply";
//	std::string mesh_name = "lamp_mesh.ply";
//	std::string mesh_name = "ape_mesh.ply";
//	std::string mesh_name = "benchvise_mesh.ply";
//	std::string mesh_name = "iron_mesh.ply";
//	std::string mesh_name = "driller_mesh.ply";
//	std::string mesh_name = "coffee_cup.ply";
//	std::string mesh_name = "coffee_cup_simplified2.ply";
	std::string mesh_name = "cam_mesh.ply";
	
	//TODO: for the eggbox and cam and can we need to first register before computing the score!!!	

//	std::string data_dir = "/home/aly/workspace/linemod_opencv_src/all_data/lamp/data/";
//	std::string data_dir = "/home/aly/workspace/linemod_opencv_src/all_data/ape/data/";
//	std::string data_dir = "/home/aly/workspace/linemod_opencv_src/all_data/driller/data/";
	std::string data_dir = "/home/aly/workspace/linemod_opencv_src/all_data/cam/data/";
//	std::string data_dir = "/home/aly/workspace/linemod_opencv_src/all_data/subset_cup/Annotation/";


	
//	std::string out_file = "ape_no_discard_2_third.txt";
//	std::string out_file = "ape_iter_prob_2_third.txt";
//	std::string out_file = "ape_iter_prob_thresh_50_2_third.txt";
//	std::string out_file = "linemod_opencv_ape.txt";
//	std::string out_file = "linemod_opencv_benchvise.txt";
//	std::string out_file = "linemod_opencv_iron.txt";
//	std::string out_file = "linemod_opencv_driller.txt";
//	std::string out_file = "driller_no_discard.txt";
//	std::string out_file = "driller_iter_prob_no_thresh.txt";
//	std::string out_file = "driller_iter_prob_50%_thresh.txt";
//	std::string out_file = "our_coffee_cup_2_third_patch_iter_prob_no_thresh.txt";
//	std::string out_file = "our_coffee_cup_1_third_patch_iter_prob_no_thresh.txt";
	
	std::string out_file = "cam_2_third_patch_iter_prob_no_thresh_Fixed_offset.txt";
	
	
//	bool symmetric = true;
	
//	std::string drost_pr_out = "/home/aly/workspace/eccv14_resutls/drost/lamp/drost_lamp_results_pr.txt";
//	std::string drost_res_out = "/home/aly/workspace/eccv14_resutls/drost/lamp/drost_processed_lamp_results_pr.txt";
	
	
//	vtkSmartPointer<vtkMatrix4x4> coffee_cup_pre_z_rot = get_rotation_around_z(-3);
//	vtkSmartPointer<vtkMatrix4x4> coffee_cup_pre_rot_mat = get_rotation_around_x(-116);
//	
//	vtkSmartPointer<vtkTransform> coffee_cup_pre_transform = vtkSmartPointer<vtkTransform>::New();
//	coffee_cup_pre_transform->Scale(1000, 1000, 1000);
//	coffee_cup_pre_transform->Concatenate(coffee_cup_pre_rot_mat);
//	coffee_cup_pre_transform->Concatenate(coffee_cup_pre_z_rot);
	
//	produce_precision_recall_curve_drost(drost_pr_out, mesh_name,data_dir, drost_res_out, false);

	produce_precision_recall_curve_date(pr_results_out, mesh_name, data_dir, out_file, true, cam_pre_transform);

	exit(-1);
	
	
	
	
	

//	Eigen::Vector3f mesh_offset_from_board = calc_offset_vector_by_com(mesh_name);
//	std::cout << " offset now is (" << mesh_offset_from_board(0) << ", " << mesh_offset_from_board(1) << ", " << mesh_offset_from_board(2) << std::endl;
	
//	double threshold = 28.6908;
//	double threshold =31.36705;
	double threshold = 38.91375;
//	lamp 0.1 = 28.5155;
	//lamp 0.11 = 31.36705;
	//lamp 0.125 = 35.644375
	//lamp 0.15 = 42.77325

//	double accuracy = get_accuracy(result_file, data_dir, mesh_offset_from_board, mesh_name, threshold);
//	std::cout <<" for file " << result_file << " value is " << std::endl;
//	std::cout <<" with alternate offset vector is " << std::endl;
//	std::cout << "accuracy is " << accuracy << " at threshold " << threshold  << std::endl;

}
